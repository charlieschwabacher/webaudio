// @flow
import grammar from './grammar';
import { entries } from '../utils';
import {
  type ASTNode,
  ANode,
  ANodeOption,
  Clip,
  Config,
  Connection,
  File,
  ModTarget,
  Modulation,
  Module,
  Node,
  Note,
  Project,
  Root,
  Section,
  Sequence,
} from './astNodes';
import {
  validateUnique,
  validateKinds,
} from './validation';

type AST<T> = { ast: T, sourceString: string };

type BlockInfo = { name: string, configs: Object, sections: Object };

type ConfigInfo<T> = { name: string, value: T };

type SectionInfo<TItem, THead = null> = {
  name: string,
  head: THead,
  items: Array<TItem>,
};

const loc = (
  { source: { startIdx, endIdx } },
): ({ _offset: number, _start: number, _length: number}) => {
  return {
    _offset: NaN,
    _start: startIdx,
    _length: endIdx - startIdx,
  };
};

const semantics = grammar.createSemantics();

semantics.addAttribute('ast', {
  Exp(project: AST<Project>, blocks: AST<Array<Module | Sequence>>): Root {
    const modules = [];
    const sequences = [];
    blocks.ast.forEach(block => {
      if (block instanceof Module) {
        modules.push(block);
      } else if (block instanceof Sequence) {
        sequences.push(block);
      } else {
        throw new Error('unrecognized block');
      }
    });

    return new Root({
      project: project.ast,
      modules,
      sequences,
      ...loc(this),
    });
  },


  block(
    name: AST<string>,
    configs: AST<Array<Config<*>>>,
    sections: AST<Array<Section<*, *>>>,
    _0: any,
  ): BlockInfo {
    return {
      name: name.ast,
      configs: configs.ast.reduce((memo, c) => {
        if (memo[c.name]) {
          throw new Error(`${name.ast} has duplicate config ${c.name}`);
        }
        memo[c.name] = c;
        return memo;
      }, {}),
      sections: sections.ast.reduce((memo, s) => {
        if (memo[s.name]) {
          throw new Error(`${name.ast} has duplicate section ${s.name}`);
        }
        memo[s.name] = s;
        return memo;
      }, {})
    }
  },
  project(
    block: AST<BlockInfo>,
  ): Project {
    const { configs, sections } = block.ast;

    validateKinds('project', 'config', configs, {}, { tempo: 'number', level: 'number' });
    validateKinds('project', 'section', sections, {}, { files: 'file', arrangement: 'clip' });

    return new Project({
      level: configs.level,
      tempo: configs.tempo, // bpm
      files: sections.files,
      arrangement: sections.arrangement,
      ...loc(this),
    });
  },
  sequence(
    block: AST<{ name: string, configs: Object, sections: Object }>,
  ): Sequence {
    const { name, configs, sections } = block.ast;

    validateKinds('sequence', 'config', configs, { target: 'string', length: 'number' }, { start: 'number' });
    validateKinds('sequence', 'section', sections, { notes: 'note' }, {}, 'modulation');

    return new Sequence({
      name,
      target: configs.target,
      start: configs.start,
      length: configs.length,
      notes: sections.notes,
      modulation: (
        entries(sections)
        .filter(([k]) => k !== 'notes')
        .map(([k, v]) => v)
      ),
      ...loc(this),
    });
  },
  module(block: AST<BlockInfo>): Module {
    const { name, configs, sections } = block.ast;

    validateKinds('module', 'config', configs, {}, { level: 'number', voices: 'number' });
    validateKinds('module', 'section', sections, { nodes: 'audioNode', connections: 'connection' }, {});

    return new Module({
      name,
      level: configs.level,
      voices: configs.voices,
      nodes: sections.nodes,
      connections: sections.connections,
      ...loc(this),
    });
  },


  config<T>(
    _0: any,
    ident: AST<string>,
    _1: any,
    exp: AST<T>,
    _2: any,
  ): ConfigInfo<T> {
    return {
      name: ident.ast,
      value: exp.ast,
    }
  },
  numberConfig(config: AST<ConfigInfo<number>>): Config<number> {
    return new Config({
      kind: 'number',
      ...config.ast,
      ...loc(this),
    });
  },
  identConfig(config: AST<ConfigInfo<string>>): Config<string> {
    return new Config({
      kind: 'string',
      ...config.ast,
      ...loc(this),
    });
  },
  configs(configs: AST<Array<Config<*>>>): Array<Config<*>> {
    return configs.ast;
  },


  section<TItem: ASTNode, THead: ?ASTNode>(
    head: AST<THead>,
    items: AST<Array<TItem>>
  ): SectionInfo<TItem, ?THead> {
    return {
      name: head.sourceString.trim(),
      head: head.ast instanceof Node ? head.ast : null,
      items: items.ast,
    };
  },
  fileSection(section: AST<SectionInfo<File>>): Section<File>{
    // validate that there no duplicate files
    validateUnique(section.ast.items, 'url', 'project files');

    return new Section({
      kind: 'file',
      ...section.ast,
      ...loc(this),
    });
  },
  clipSection(section: AST<SectionInfo<Clip>>): Section<Clip>{
    return new Section({
      kind: 'clip',
      ...section.ast,
      ...loc(this),
    });
  },
  noteSection(section: AST<SectionInfo<Note>>): Section<Note>{
    return new Section({
      kind: 'note',
      ...section.ast,
      ...loc(this),
    });
  },
  modSection(
    section: AST<SectionInfo<Modulation, ModTarget>>,
  ): Section<Modulation, ModTarget>{
    return new Section({
      kind: 'modulation',
      ...section.ast,
      ...loc(this),
    });
  },
  audioNodeSection(section: AST<SectionInfo<ANode>>): Section<ANode>{
    // validate no overlapping node names
    validateUnique(section.ast.items, 'name', 'module nodes');

    return new Section({
      kind: 'audioNode',
      ...section.ast,
      ...loc(this),
    });
  },
  connectionSection(section: AST<SectionInfo<Connection>>): Section<Connection>{
    return new Section({
      kind: 'connection',
      ...section.ast,
      ...loc(this),
    });
  },
  sections(sections: AST<Array<Section<*, *>>>): Array<Section<*,*>>{
    // validate no overlapping section names
    validateUnique(sections.ast, 'name');

    return sections.ast;
  },


  line(
    _0: any,
    exp: AST<string>,
    _1: any
  ) {
    return exp.ast;
  },
  def(
    _0: any,
    _1: any,
    _2: any,
    ident: AST<string>,
    _3: any,
  ): string {
    return ident.ast;
  },
  modTarget(
    _0: any,
    node: AST<string>,
    _1: any,
    param: AST<string>,
    _2: any,
  ): ModTarget {
    return new ModTarget({
      node: node.ast,
      param: param.ast,
      ...loc(this),
    });
  },
  clip(
    _0: any,
    sequence: AST<string>,
    _1: any,
    start: AST<number>,
    _2: any,
    length: AST<number>,
    _3: any,
  ): Clip {
    return new Clip({
      sequence: sequence.ast,
      start: start.ast,
      length: length.ast,
      ...loc(this),
    });
  },
  note(
    _0: any,
    key: AST<number>,
    _1: any,
    start: AST<number>,
    _2: any,
    length: AST<number>,
    _3: any,
  ): Note {
    return new Note({
      key: key.ast,
      start: start.ast,
      length: length.ast,
      ...loc(this),
    });
  },
  modulation(
    _0: any,
    value: AST<number>,
    _1: any,
    time: AST<number>,
    _2: any,
  ): Modulation {
    return new Modulation({
      value: value.ast,
      time: time.ast,
      ...loc(this),
    });
  },
  audioNode(
    _0: any,
    name: AST<string>,
    _1: any,
    nodeType: AST<string>,
    _2: any,
    options: AST<Array<ANodeOption>>,
    _3: any,
  ): ANode {
    return new ANode({
      name: name.ast,
      nodeType: nodeType.ast,
      options: options.ast,
      ...loc(this),
    });
  },
  connection(
    _0: any,
    from: AST<string>,
    _1: any,
    to: AST<string>,
    _2: any,
    param: AST<Array<string>>,
    _3: any,
  ): Connection {
    return new Connection({
      from: from.ast,
      to: to.ast,
      param: param.ast[0],
      ...loc(this),
    });
  },
  file(_0: any, url: AST<string>, _1: any): File {
    return new File({
      url: url.ast,
      ...loc(this),
    })
  },

  nodeOption(
    key: AST<string>,
    _0: any,
    value: AST<string>,
  ): ANodeOption {
    return new ANodeOption({
      key: key.ast,
      value: value.ast,
      ...loc(this),
    });
  },
  value(
    v: AST<string>,
  ): string {
    return v.ast;
  },
  list(
    _0: any,
    _1: any,
    numbers: AST<Array<number>>,
    _2: any,
    _3: any,
  ): Array<number> {
    return numbers.ast;
  },
  nodeType(type: AST<string>): string { return type.ast; },
  midiNote(d1: AST<number>, d2: AST<number>): number {
    return d1.ast * 12 + d2.ast;
  },
  base12Digit(d: AST<string>): number { return parseInt(d.ast, 12); },
  string(_0: any, val: AST<any>, _1: any): string {
    return val.sourceString
  },
  ident(_0: any, _1: any): string { return this.sourceString; },
  number(n: AST<number>): number { return n.ast; },
  fraction(num: AST<number>, _: any, denom: AST<number>): number {
    return num.ast / denom.ast;
  },
  decimal(_0: any, _1: any, _2: any, _3: any): number {
    return parseFloat(this.sourceString);
  },
  integer(_0: any, _1: any): number {
    return parseInt(this.sourceString, 10);
  },
  _terminal(): string { return this.sourceString; },
});

export default semantics;
