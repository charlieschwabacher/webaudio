// @flow
import { invariant } from '../../utils';
import Node, { type NodeProps } from './Node';

export type ANodeOptionProps = NodeProps & {
  key: string,
  value: mixed,
}

export default class ANodeOption extends Node {
  key: string;
  value: mixed;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ANodeOptionProps) {
    super(props);

    const { key, value } = props;
    this.key = key;
    this.value = value;
  }

  print() {
    const { key, value } = this;
    if (Array.isArray(value)) {
      return ` ${key} [${value.join(' ')}]`;
    } else if (
      typeof value === 'object' &&
      value != null &&
      value.kind === 'file'
    ) {
      invariant(typeof value.url === 'string');
      return ` ${key} @"${value.url}"`;
    } else {
      return ` ${key} ${JSON.stringify(value)}`;
    }
  }
};
