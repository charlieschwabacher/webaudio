// @flow

import type { ASTNode } from './types';
import { id } from '../../utils';

export const virtual = {
  _start: NaN,
  _offset: NaN,
  _length: 0,
  _virtual: true,
};

export type NodeProps = {
  _id?: string,
  _start: number,
  _offset: number,
  _length: number,
  _virtual?: boolean,
};

export default class Node implements ASTNode {
  _id: string;
  _parent: ASTNode;
  _start: number;
  _offset: number;
  _length: number;
  _virtual: ?boolean;

  constructor({ _id, _start, _offset, _length, _virtual }: NodeProps) {
    this._id = _id || id();
    this._start = _start;
    this._offset = _offset;
    this._length = _length;
    this._virtual = _virtual;
  }

  print() {
    throw new Error('print should be implemented by ast node subclasses');
  }

  children(): Array<ASTNode> {
    return [];
  }

  setOffsets() {
    if (this._virtual) {
      throw new Error('cannot set offsets of virtual node');
    }

    const { start } = this.textLocation();
    this.children().forEach(child => {
      if (!Number.isNaN(child._start)) {
        child._offset = child._start - start;
        child._start = NaN;
        child.setOffsets();
      }
    });
  }

  updateText() {
    if (this._virtual) {
      console.warn('tried to update text of virtual node');
      return NaN;
    }

    const length = this.print().length;
    const previousLength = this._length;

    this._length = length;

    const delta = length - previousLength;
    propagateOffsets(this, delta);

    return delta;
  }

  removeText() {
    if (this._virtual) {
      console.warn('tried to remove text of virtual node');
      return NaN;
    }

    const delta = -this._length;
    propagateOffsets(this, delta);

    return delta;
  }

  shiftText(delta: number) {
    this._offset += delta;
  }

  extendText(delta: number) {
    this._length += delta;
  }

  textLocation() {
    if (this._virtual) {
      throw new Error('cannot read text location of virtual node');
    }
    if (Number.isNaN(this._offset)) {
      throw new Error('attempted to read textLocation before offsets were set');
    }

    let currentOffset = this._offset;
    let currentNode = this;
    while (currentNode._parent) {
      currentNode = currentNode._parent;
      currentOffset += currentNode._offset;
    }

    currentOffset = Math.floor(currentOffset);

    return {
      start: currentOffset,
      end: currentOffset + this._length,
    };
  }
};

function propagateOffsets(node: ASTNode, delta: number): void {
  let current = node;
  let ancestor = node._parent;
  const shiftText = sibling => {
    if (
      sibling._offset > current._offset ||
      (sibling._offset === current._offset && sibling._id > current._id)
    ) {
      sibling.shiftText(delta);
    }
  };
  while (ancestor) {
    ancestor.extendText(delta);
    ancestor.children().forEach(shiftText);
    current = ancestor;
    ancestor = ancestor._parent;
  }
}
