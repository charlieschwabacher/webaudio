// @flow

import type { ASTNode } from './types';

export default function printNodes(...nodes: Array<ASTNode>): string {
  return nodes
    .filter(node => !node._virtual)
    .sort((a, b) => a._offset - b._offset)
    .map(node => node.print())
    .join('');
}
