// @flow
import Node, { type NodeProps } from './Node';

export type ModulationProps = NodeProps & {
  value: number,
  time: number, // beats
};

export default class Modulation extends Node {
  value: number;
  time: number;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ModulationProps) {
    super(props);

    const { value, time } = props;
    this.value = value;
    this.time = time;
  }

  print() {
    const { value, time } = this;
    return `    ${value} ${time}\n`;
  }
};
