// @flow
import printNodes from './printNodes';
import Node, { virtual, type NodeProps } from './Node';
import Project from './Project';
import type Sequence from './Sequence';
import type Module from './Module';

export type RootProps = NodeProps & {
  project?: Project,
  sequences: Array<Sequence>,
  modules: Array<Module>,
};

export default class Root extends Node {
  project: Project;
  sequences: Array<Sequence>;
  modules: Array<Module>;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: RootProps) {
    super(props);

    let { project, sequences, modules } = props;

    if (project == null) {
      project = new Project(virtual);
    }

    this.project = project;
    this.sequences = sequences;
    this.modules = modules;

    project._parent = this;
    sequences.forEach(s => { s._parent = this; });
    modules.forEach(m => { m._parent = this; });

    // Root has no parent, so it sets its own offset to 0 here
    this._offset = 0;
    this._start = NaN;
    this.setOffsets();
  }

  children() {
    const { project, sequences, modules } = this;
    return [ project, ...sequences, ...modules ];
  }

  print() {
    return printNodes(...this.children());
  }
};
