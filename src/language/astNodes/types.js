// @flow

export type TextLocation = {
  start: number,
  end: number,
};

export interface ASTNode {
  _id: string;
  _parent?: ASTNode;
  _start: number;
  _offset: number;
  _length: number;
  _virtual: ?boolean; // true if node is a default w/ no text source
  children(): Array<ASTNode>;
  print(): string;
  setOffsets(): void; // set child offsets
  updateText(): number; // update length and offsets of ancestors and siblings
  removeText(): number; // update offsets of ancestors and siblings
  shiftText(delta: number): void; // increase offset by delta
  extendText(delta: number): void; // increase length by delta
  textLocation(): TextLocation; // return absolute start and end indices
};
