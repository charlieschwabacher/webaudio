// @flow
import Node, { type NodeProps } from './Node';

export type ConnectionProps = NodeProps & {
  from: string,
  to: string,
  param?: ?string,
};

export default class Connection extends Node {
  from: string;
  to: string;
  param: ?string;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ConnectionProps) {
    super(props);

    const { from, to, param } = props;
    this.from = from;
    this.to = to;
    this.param = param;
  }

  print() {
    const { from, to, param } = this;
    return `    ${from}->${to}${param ? `.${param}` : ''}\n`
  }
};
