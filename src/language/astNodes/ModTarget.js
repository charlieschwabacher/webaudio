// @flow
import Node, { type NodeProps } from './Node';

export type ModTargetProps = NodeProps & {
  node: string,
  param: string,
}

export default class ModTarget extends Node {
  node: string;
  param: string;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ModTargetProps) {
    super(props);

    const { node, param } = props;
    this.node = node;
    this.param = param;
  }

  print() {
    const { node, param } = this;
    return `  ${node}.${param}\n`;
  }
}
