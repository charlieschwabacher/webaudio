// @flow
import printNodes from './printNodes';
import Node, { virtual, type NodeProps } from './Node';
import Config from './Config';
import type Section from './Section';
import type ANode from './ANode';
import type Connection from './Connection';

export type ModuleProps = NodeProps & {
  name: string,
  level?: Config<number>,
  voices?: Config<number>,
  nodes: Section<ANode>,
  connections: Section<Connection>,
};

export default class Module extends Node {
  name: string;
  level: Config<number>;
  voices: Config<number>;
  nodes: Section<ANode>;
  connections: Section<Connection>;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ModuleProps) {
    super(props);

    let { name, level, voices, nodes, connections } = props;

    if (level == null) {
      level = new Config({ ...virtual, name: 'level', kind: 'number', value: 1.0 });
    }
    if (voices == null) {
      voices = new Config({ ...virtual, name: 'voices', kind: 'number', value: 1 });
    }

    this.name = name;
    this.level = level;
    this.voices = voices;
    this.nodes = nodes;
    this.connections = connections;

    level._parent = this;
    voices._parent = this;
    nodes._parent = this;
    connections._parent = this;
  }

  children() {
    const { level, voices, nodes, connections } = this;
    return [ level, voices, nodes, connections ];
  }

  print() {
    const { name } = this;
    return [
      `module ${name}\n`,
      printNodes(...this.children()),
      '\n',
    ].join('');
  }
};
