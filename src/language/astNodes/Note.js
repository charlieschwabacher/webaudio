// @flow
import Node, { type NodeProps } from './Node';

export type NoteProps = NodeProps & {
  key: number, // midi note value
  start: number, // beats
  length: number, // beats
}

export default class Note extends Node {
  key: number;
  start: number;
  length: number;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: NoteProps) {
    super(props);

    const { key, start, length } = props;
    this.key = key;
    this.start = start;
    this.length = length;
  }

  print() {
    const { key, start, length } = this;
    return `    ${key.toString(12).toUpperCase()} ${start} ${length}\n`;
  }
};
