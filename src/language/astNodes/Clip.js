// @flow
import Node, { type NodeProps } from './Node';

export type ClipProps = NodeProps & {
  sequence: string, // sequence name
  start: number, // beats
  length: number, // beats
};

export default class Clip extends Node {
  sequence: string;
  start: number;
  length: number;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ClipProps) {
    super(props);

    const { sequence, start, length } = props;
    this.sequence = sequence;
    this.start = start;
    this.length = length;
  }

  print() {
    const { sequence, start, length } = this;
    return `    ${sequence} ${start} ${length}\n`;
  }
};
