// @flow
import printNodes from './printNodes';
import Node, { virtual, type NodeProps } from './Node';
import Config from './Config';
import type Section from './Section';
import type Note from './Note';
import type Modulation from './Modulation';
import type ModTarget from './ModTarget';

export type SequenceProps = NodeProps & {
  name: string,
  target: Config<string>; // module name
  length: Config<number>, // beats
  start?: Config<number>, // beats
  notes: Section<Note>,
  modulation: Array<Section<Modulation, ModTarget>>,
};

export default class Sequence extends Node {
  name: string;
  target: Config<string>;
  length: Config<number>;
  start: Config<number>;
  notes: Section<Note>;
  modulation: Array<Section<Modulation, ModTarget>>;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: SequenceProps) {
    super(props);

    let { name, target, length, start, notes, modulation } = props;

    if (start == null) {
      start = new Config({ ...virtual, name: 'start', kind: 'number', value: 0 });
    }

    this.name = name;
    this.target = target;
    this.length = length;
    this.start = start;
    this.notes = notes;
    this.modulation = modulation;

    target._parent = this;
    start._parent = this;
    length._parent = this;
    notes._parent = this;
    modulation.forEach(m => { m._parent = this; });
  }

  children() {
    const { target, length, start, notes, modulation } = this;
    return [ target, length, start, notes, ...modulation ];
  }

  print() {
    const { name } = this;
    return [
      `sequence ${name}\n`,
      printNodes(...this.children()),
      '\n',
    ].join('');
  }
};
