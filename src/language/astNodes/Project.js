// @flow
import printNodes from './printNodes';
import Node, { virtual, type NodeProps } from './Node';
import Config from './Config';
import Section from './Section';
import type File from './File';
import type Clip from './Clip';

export type ProjectProps = NodeProps & {
  tempo?: Config<number>, // bpm
  level?: Config<number>,
  files?: Section<File>,
  arrangement?: Section<Clip>,
};

export default class Project extends Node {
  tempo: Config<number>;
  level: Config<number>;
  files: Section<File>;
  arrangement: Section<Clip>;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ProjectProps) {
    super(props);

    let { tempo, level, files, arrangement } = props;

    if (tempo == null) {
      tempo = new Config({ ...virtual, name: 'tempo', kind: 'number', value: 120 });
    }
    if (level == null) {
      level = new Config({ ...virtual, name: 'level', kind: 'number', value: 1.0 });
    }
    if (files == null) {
      files = new Section({ ...virtual, name: 'files', kind: 'file', items: [] });
    }
    if (arrangement == null) {
      arrangement = new Section({ ...virtual, name: 'arrangement', kind: 'clip', items: [] });
    }

    this.tempo = tempo;
    this.level = level;
    this.files = files;
    this.arrangement = arrangement;

    tempo._parent = this;
    level._parent = this;
    files._parent = this;
    arrangement._parent = this;
  }

  children() {
    const { tempo, level, files, arrangement } = this;
    return [ tempo, level, files, arrangement ];
  }

  print() {
    return [
      'project\n',
      printNodes(...this.children()),
      '\n',
    ].join('');
  }
};
