// @flow
import Node, { type NodeProps } from './Node';

export type FileProps = NodeProps & {
  url: string,
};

export default class File extends Node {
  url: string;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: FileProps) {
    super(props);

    const { url } = props;
    this.url = url;
  }

  print() {
    const { url } = this;
    return `    "${url}"\n`;
  }
};
