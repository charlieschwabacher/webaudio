// @flow
import printNodes from './printNodes';
import { keyMapObj } from '../../utils';
import Node, { type NodeProps } from './Node';
import type { ASTNode } from './types';

export type SectionProps<TItem: ASTNode, THead: ?ASTNode = void> = NodeProps & {
  name: string,
  kind: string,
  head?: THead,
  items: Array<TItem>,
};

export default class Section<
  TItem: ASTNode,
  THead: ?ASTNode = null,
> extends Node {
  name: string;
  kind: string;
  items: Array<TItem>;
  itemsById: {[string]: TItem};
  head: ?THead;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: SectionProps<TItem, THead>) {
    super(props);

    const { name, kind, items, head } = props;
    this.name = name;
    this.kind = kind;
    this.head = head;
    this.items = items;
    this.itemsById = keyMapObj(items, ({ _id }) => _id);

    if (head) head._parent = this;
    items.forEach(i => { i._parent = this; });
  }

  children() {
    const { head, items } = this;
    return [
      ...(head ? [ head ] : []),
      ...items,
    ];
  }

  print() {
    const { name, head, items } = this;
    return [
      head ? head.print() : `  ${name}\n`,
      printNodes(...items),
    ].join('');
  }
};
