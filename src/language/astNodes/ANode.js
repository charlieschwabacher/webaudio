// @flow
import printNodes from './printNodes';
import Node, { type NodeProps } from './Node';
import type ANodeOption from './ANodeOption';

export type ANodeProps = NodeProps & {
  name: string,
  nodeType: string,
  options: Array<ANodeOption>,
};

export default class ANode extends Node {
  name: string;
  nodeType: string;
  options: Array<ANodeOption>;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ANodeProps) {
    super(props);

    const { name, nodeType, options } = props;
    this.name = name;
    this.nodeType = nodeType;
    this.options = options;

    options.forEach(o => { o._parent = this; });
  }

  children() {
    return [ ...this.options ];
  }

  print() {
    const { name, nodeType, options } = this;
    return `    ${name} ${nodeType}${printNodes(...options)}\n`;
  }
};
