// @flow
import { invariant } from '../../utils';
import Node, { type NodeProps } from './Node';

export type ConfigProps<T> = NodeProps & {
  name: string,
  kind: string,
  value: T,
};

export default class Config<T> extends Node {
  name: string;
  kind: string;
  value: T;
  _start: number;
  _offset: number;
  _length: number;

  constructor(props: ConfigProps<T>) {
    super(props);

    const { name, kind, value } = props;
    this.name = name;
    this.kind = kind;
    this.value = value;
  }

  print() {
    const { name, value } = this;
    invariant(typeof value === 'string' || typeof value === 'number');
    return `  ${name} ${value}\n`;
  }
};
