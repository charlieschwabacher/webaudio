// @flow

import { entries } from '../utils';

export function validateUnique(
  items: Array<Object>,
  property: string,
  subject?: string,
): void {
  const existing = new Set();
  items.forEach(item => {
    const value = item[property];
    if (existing.has(value)) {
      throw new Error(
        `duplicate ${property} ${value}`
        + (subject != null ? ` in ${subject}` : ''),
      );
    }
    existing.add(value);
  });
}

export function validateKinds(
  subject: string,
  thing: string,
  actual: {[string]: { kind: string }},
  required: {[string]: string},
  optional: {[string]: string},
  rest?: string,
): void {
  entries(actual).forEach(([ name, { kind }]) => {
    if (required[name] !== kind && optional[name] !== kind && rest !== kind) {
      const wanted = required[name] || optional[name] || rest;
      throw new Error(
        `unexpected ${thing} ${name} of kind ${kind} in ${subject}`
        + (wanted ? `, wanted ${wanted}` : ''),
      );
    }
  });
  Object.keys(required).forEach(name => {
    if (actual[name] == null) {
      throw new Error(`missing required ${thing} ${name} in ${subject}`);
    }
  })
}
