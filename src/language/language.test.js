// @flow
import { entries } from '../utils';
import grammar from './grammar';
import semantics from './semantics';
import {
  ANode,
  ANodeOption,
  Clip,
  Config,
  Connection,
  File,
  ModTarget,
  Modulation,
  Module,
  Note,
  Project,
  Root,
  Section,
  Sequence,
} from './astNodes';
import presetTracks from '../presetTracks';

const text =
`project
  level 0.6
  tempo 120
  files
    "/ir.wav"
  arrangement
    BDSEQ 0 16

sequence BDSEQ
  target BD
  start 0
  length 4
  notes
    30 0 0.25
    30 1 0.25
    30 2 0.25
    30 2.75 0.25
    30 3.5 0.25
  OSC.detune
    0 0
    1200 4

module BD
  level 1
  voices 1
  nodes
    OSC OscillatorNode detune 10 type "square"
    AMP GainNode gain 0
    RVB ConvolverNode buffer "/ir.wav"
    VENV EnvelopeNode steps [1 0.21 0 0]
    PENV EnvelopeNode steps [1200 0.06 0 0]
    SEQ SequenceNode
  connections
    OSC->AMP
    AMP->OUT
    AMP->RVB
    RBV->OUT
    SEQ->OSC.detune
    PENV->OSC.detune
    VENV->AMP.gain

`;

const s = (offset: number, length: number) =>
  ({ _start: NaN, _offset: offset, _length: length });

const compareAST = (actual: Object, expected: Object) => {
  const redact = (obj, remove) => {
    if (obj instanceof Object) {
      return Object.keys(obj).reduce((memo, k) => {
        if (!remove.has(k)) {
          memo[k] = redact(obj[k], remove);
        }
        return memo;
      }, {});
    }
    return obj;
  };
  expect(
    redact(actual, new Set(['_id', '_parent', 'itemsById'])),
  ).toEqual(
    redact(expected, new Set(['_id', '_parent', 'itemsById'])),
  );
};

const ast = new Root({
  ...s(0, 630),
  project: new Project({
    ...s(0, 84),
    level: new Config({
      ...s(8, 12),
      name: 'level',
      kind: 'number',
      value: 0.6,
    }),
    tempo: new Config({
      ...s(20, 12),
      name: 'tempo',
      kind: 'number',
      value: 120,
    }),
    files: new Section({
      ...s(32, 22),
      name: 'files',
      kind: 'file',
      head: null,
      items: [
        new File({ ...s(8, 14), url: '/ir.wav' }),
      ],
    }),
    arrangement: new Section({
      ...s(54, 29),
      name: 'arrangement',
      kind: 'clip',
      head: null,
      items: [
        new Clip({ ...s(14, 15), sequence: 'BDSEQ', start: 0, length: 16 }),
      ],
    }),
  }),
  sequences: [
    new Sequence({
      ...s(84, 164),
      name: 'BDSEQ',
      target: new Config({ ...s(15, 12), name: 'target', kind: 'string', value: 'BD' }),
      start: new Config({ ...s(27, 10), name: 'start', kind: 'number', value: 0 }),
      length: new Config({ ...s(37, 11), name: 'length', kind: 'number', value: 4 }),
      notes: new Section({
        ...s(48, 83),
        name: 'notes',
        kind: 'note',
        head: null,
        items: [
          new Note({ ...s(8, 14), key: 36, start: 0, length: 0.25 }),
          new Note({ ...s(22, 14), key: 36, start: 1, length: 0.25 }),
          new Note({ ...s(36, 14), key: 36, start: 2, length: 0.25 }),
          new Note({ ...s(50, 17), key: 36, start: 2.75, length: 0.25 }),
          new Note({ ...s(67, 16), key: 36, start: 3.5, length: 0.25 }),
        ],
      }),
      modulation: [
        new Section({
          ...s(131, 32),
          name: 'OSC.detune',
          kind: 'modulation',
          head: new ModTarget({ ...s(0, 13), node: 'OSC', param: 'detune' }),
          items: [
            new Modulation({ ...s(13, 8), value: 0, time: 0 }),
            new Modulation({ ...s(21, 11), value: 1200, time: 4 }),
          ],
        }),
      ],
    }),
  ],
  modules: [
    new Module({
      ...s(248, 382),
      name: 'BD',
      level: new Config({ ...s(10, 10), name: 'level', kind: 'number', value: 1 }),
      voices: new Config({ ...s(20, 11), name: 'voices', kind: 'number', value: 1 }),
      nodes: new Section({
        ...s(31, 224),
        name: 'nodes',
        kind: 'audioNode',
        head: null,
        items: [
          new ANode({
            ...s(8, 47),
            name: 'OSC',
            nodeType: 'OscillatorNode',
            options: [
              new ANodeOption({ ...s(23, 9), key: 'detune', value: 10 }),
              new ANodeOption({ ...s(33, 13), key: 'type', value: 'square' }),
            ],
          }),
          new ANode({
            ...s(55, 24),
            name: 'AMP',
            nodeType: 'GainNode',
            options: [
              new ANodeOption({ ...s(17, 6), key: 'gain', value: 0 }),
            ],
          }),
          new ANode({
            ...s(79, 39),
            name: 'RVB',
            nodeType: 'ConvolverNode',
            options: [
              new ANodeOption({ ...s(22, 16), key: 'buffer', value: '/ir.wav' }),
            ],
          }),
          new ANode({
            ...s(118, 41),
            name: 'VENV',
            nodeType: 'EnvelopeNode',
            options: [
              new ANodeOption({ ...s(22, 18), key: 'steps', value: [1, 0.21, 0, 0] }),
            ],
          }),
          new ANode({
            ...s(159, 44),
            name: 'PENV',
            nodeType: 'EnvelopeNode',
            options: [
              new ANodeOption({ ...s(22, 21), key: 'steps', value: [1200, 0.06, 0, 0] }),
            ],
          }),
          new ANode({
            ...s(203, 21),
            name: 'SEQ',
            nodeType: 'SequenceNode',
            options: [],
          }),
        ],
      }),
      connections: new Section({
        ...s(255, 126),
        name: 'connections',
        kind: 'connection',
        head: null,
        items: [
          new Connection({ ...s(14, 13), from: 'OSC', to: 'AMP' }),
          new Connection({ ...s(27, 13), from: 'AMP', to: 'OUT' }),
          new Connection({ ...s(40, 13), from: 'AMP', to: 'RVB' }),
          new Connection({ ...s(53, 13), from: 'RBV', to: 'OUT' }),
          new Connection({ ...s(66, 20), from: 'SEQ', to: 'OSC', param: 'detune' }),
          new Connection({ ...s(86, 21), from: 'PENV', to: 'OSC', param: 'detune' }),
          new Connection({ ...s(107, 19), from: 'VENV', to: 'AMP', param: 'gain' }),
        ],
      }),
    }),
  ],
});

it('parses text to ast', () => {
  const matcher = grammar.matcher().setInput(text);
  const result = semantics(matcher.match());
  compareAST(result.ast, ast);
});

it('prints ast to text', () => {
  expect(ast.print()).toEqual(text);
});

it('parses preset tracks', () => {
  entries(presetTracks).forEach(([ name, source ]) => {
    const matcher = grammar.matcher().setInput(source);
    const result = semantics(matcher.match());
    expect(result.ast).toBeDefined();
  });
});
