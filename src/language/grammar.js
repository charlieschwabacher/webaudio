// @flow
import ohm from 'ohm-js';

export default ohm.grammar(`
Language {


Exp
  = project (sequence | module)*


block<head>
  = head configs sections e*

project
  = block<line<"project">>

sequence
  = block<def<"sequence">>

module
  = block<def<"module">>


config<exp>
  = s? ident s exp e

numberConfig
  = config<number>

identConfig
  = config<ident>

configs
  = (
    numberConfig
    | identConfig
  )*


section<head, exp>
  = head exp*

fileSection
  = section<line<"files">, file>

clipSection
  = section<line<"arrangement">, clip>

noteSection
  = section<line<"notes">, note>

modSection
  = section<modTarget, modulation>

audioNodeSection
  = section<line<"nodes">, audioNode>

connectionSection
  = section<line<"connections">, connection>

sections
  = (
    fileSection
    | clipSection
    | noteSection
    | modSection
    | audioNodeSection
    | connectionSection
  )*


line<exp>
  = s? exp e

def<exp>
  = s? exp s ident e

modTarget
  = s? ident "." ident e

clip
  = s? ident s number s number e

note
  = s? midiNote s number s number e

modulation
  = s? number s number e

audioNode
  = s? ident s nodeType (s nodeOption)* e

connection
  = s? ident "->" ident ("." ident)? e

file
  = s? string e


nodeOption
  = ident s value

value (a literal value)
  = list
  | number
  | string

list (a list of numbers)
  = "[" s? (number s?)* "]"

nodeType
  = "AudioBufferSourceNode"
  | "BiquadFilterNode"
  | "ConvolverNode"
  | "DelayNode"
  | "DynamicsCompressorNode"
  | "EnvelopeNode"
  | "GainNode"
  | "OscillatorNode"
  | "SamplerNode"
  | "SequenceNode"
  | "WaveShaperNode"
  | "WhiteNoiseNode"

midiNote
  = base12Digit base12Digit

base12Digit
  = digit | "A" | "B"

string (a string)
  = "\\"" (~"\\"" any)* "\\""

ident (an identifier)
  = letter alnum*

number (a number)
  = fraction
  | decimal
  | integer

fraction
  = integer "/" integer

decimal (a decimal number)
  = "-"? digit+ "." digit+

integer (an integer)
  = "-"? digit+

s
  = (" " | "\\t")+

e
  = s? "\\n"

}
`);
