// @flow
import React, { Component } from 'react';
import {
  Arrange,
  AudioGraph,
  Editor,
  FFTMeter,
  Files,
  Knob,
  Launcher,
  PianoRoll,
  PlayButton,
} from './components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  initEditor,
  destroyEditor,
  replaceTextRange,
  resetWithText,
  updateNotes,
  removeNotes,
  flattenNotes,
  updateLoop,
  setModuleLevel,
  setMasterLevel,
  play,
  stop,
  createSequence,
  stageSequence,
  stopModule,
  toggleSolo,
  toggleMute,
} from './store/actions';
import presetTracks from './presetTracks';
import './App.css'
import type CodeMirror from 'codemirror';
import type { Project } from './audio';
import type {
  Root as RootAST,
  Note as NoteAST,
} from './language';
import type { Session } from './store';

const PANES = {
  'editor': '💻',
  'launcher': '🎚️',
  'piano roll': '🎹',
  'audio graph': '🔗',
  'files': '💾',
  'arrange': '🎼',
};

type Pane = $Keys<typeof PANES>;

class App extends Component<{
  project: Project,
  text: string,
  ast: RootAST,
  parseError: ?string,
  session: Session,
  initEditor: (editor: CodeMirror) => void,
  destroyEditor: (editor: CodeMirror) => void,
  replaceTextRange: (from: number, to: number, text: string) => void,
  resetWithText: (text: string) => void,
  removeNotes: (sequence: string, ids: Array<string>) => void,
  updateNotes: (sequence: string, notes: {[id: string]: NoteAST}) => void,
  flattenNotes: (sequence: string) => void,
  updateLoop: (sequence: string, start: number, length: number) => void,
  keyDown: (key: string) => void,
  keyUp: (key: string) => void,
  setModuleLevel: (module: string, level: number) => void,
  setMasterLevel: (level: number) => void,
  play: () => void,
  stop: () => void,
  createSequence: (id: string, target: string) => void,
  stageSequence: (module: string, sequence: string) => void,
  stopModule: (module: string) => void,
  toggleSolo: (module: ?string) => void,
  toggleMute: (module: string, mute: boolean) => void,
}, {
  selectedSequence: string,
  activePanes: Set<Pane>,
}> {

  constructor(props) {
    super(props);
    this.state = {
      selectedSequence: props.ast.sequences[0].name,
      activePanes: new Set(['editor', 'launcher', 'piano roll']),
    };
  }

  componentWillUpdate(nextProps) {
    // if the selected sequence is no longer present, select a new one
    if (
      nextProps.ast !== this.props.ast &&
      !nextProps.ast.sequences.some(
        s => s.name === this.state.selectedSequence
      )
    ) {
      this.selectSequence(nextProps.ast.sequences[0].name);
    }
  }

  togglePane = (pane: Pane) => {
    const activePanes = this.state.activePanes;
    if (activePanes.has(pane)) {
      if (activePanes.size > 1) {
        activePanes.delete(pane);
      }
    } else {
      activePanes.add(pane);
    }
    this.forceUpdate();
  }

  selectSequence = (name: string) => {
    this.setState({ selectedSequence: name });
  }

  render() {
    const { selectedSequence, activePanes } = this.state;
    const {
      project,
      ast,
      parseError,
      session,
      initEditor,
      destroyEditor,
      replaceTextRange,
      resetWithText,
      removeNotes,
      updateNotes,
      flattenNotes,
      updateLoop,
      setModuleLevel,
      setMasterLevel,
      play,
      stop,
      createSequence,
      stageSequence,
      stopModule,
      toggleSolo,
      toggleMute,
    } = this.props;

    const sequence = ast.sequences.find(s => s.name === selectedSequence);

    return (
      <div
        style={{
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          height: '100vh',
        }}
      >
        <div
          style={{
            display: 'flex',
            height: 30,
            justifyContent: 'flex-end',
            background: '#aaa',
            position: 'relative',
            padding: 5,
          }}
        >
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              display: 'flex',
            }}
          >
            <FFTMeter
              analyser={project.analyser}
              color='#999'
            />
          </div>
          <div style={{ position: 'relative', marginRight: 10 }}>
            <Knob
              value={ast.project.level.value}
              onChange={setMasterLevel}
              size={20}
            />
          </div>
          <div style={{ position: 'relative' }}>
            <PlayButton
              playing={session.playing}
              onClick={session.playing ? stop : play}
              size={20}
              color={session.playing ? '#ff4e49' : '#18f3ba'}
            />
          </div>
        </div>
        <div style={{ flex: 1, minHeight: 0, display: 'flex' }}>
          {
            activePanes.has('editor') &&
            <Editor
              replaceTextRange={replaceTextRange}
              initEditor={initEditor}
              destroyEditor={destroyEditor}
              parseError={parseError}
            />
          }
          {
            activePanes.has('launcher') &&
            <Launcher
              project={project}
              modules={ast.modules}
              sequences={ast.sequences}
              session={session}
              selectedSequence={selectedSequence}
              setModuleLevel={setModuleLevel}
              selectSequence={this.selectSequence}
              createSequence={createSequence}
              stageSequence={stageSequence}
              stopModule={stopModule}
              toggleSolo={toggleSolo}
              toggleMute={toggleMute}
            />
          }
          {
            activePanes.has('piano roll') && sequence &&
            <PianoRoll
              key={`pr${activePanes.size}`}
              project={project}
              sequence={sequence}
              removeNotes={removeNotes}
              updateNotes={updateNotes}
              flattenNotes={flattenNotes}
              updateLoop={updateLoop}
            />
          }
          {
            activePanes.has('audio graph') &&
            <AudioGraph
              key={`ag${activePanes.size}`}
              ast={ast}
            />
          }
          {
            activePanes.has('files') &&
            <Files
              library={project.library}
              ast={ast.project.files}
            />
          }
          {
            activePanes.has('arrange') &&
            <Arrange
              ast={ast.project.arrangement.items}
            />
          }
        </div>
        <div style={{ userSelect: 'none', display: 'flex', flexDirection: 'row', backgroundColor: '#ccc' }}>
          {
            Object.keys(PANES).map(pane =>
              <a
                style={{
                  display: 'block',
                  padding: '0 10px',
                  background: activePanes.has(pane) ? '#aaa' : '',
                }}
                key={pane}
                onClick={() => this.togglePane(pane)}
              >
                {PANES[pane]}
              </a>
            )
          }
          <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end', padding: '0 5px' }}>
            <select
              onChange={e => {
                resetWithText(presetTracks[e.target.value]);
              }}
            >
              {
                Object.keys(presetTracks).map(trackName =>
                  <option key={trackName} value={trackName}>{trackName}</option>
                )
              }
            </select>
            <select
              onChange={e =>
                this.selectSequence(e.target.value)
              }
            >
              {
                ast.sequences.map(({ name }) =>
                  <option key={name} value={name}>{name}</option>
                )
              }
            </select>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.project,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    initEditor,
    destroyEditor,
    replaceTextRange,
    resetWithText,
    removeNotes,
    updateNotes,
    flattenNotes,
    updateLoop,
    setModuleLevel,
    setMasterLevel,
    play,
    stop,
    createSequence,
    stageSequence,
    stopModule,
    toggleMute,
    toggleSolo,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
