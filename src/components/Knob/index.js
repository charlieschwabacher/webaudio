// @flow
// a mouse interactive rotary knob - expects to receive two props, value, the
// current position of the knob as a number between 0 and 1, and onChange, a
// callback which will get passed the new values on changes.
//
// Also can receive an optional prop 'disabled' which if true will prevent
// interaction.

import React, { Component } from 'react';
import { invariant } from '../../utils';
import './Knob.css';

export default class Knob extends Component<{
  value: number,
  onChange: number => void,
  range: number,
  size: number,
  disabled: boolean,
  precision: number,
}, {
  active: boolean,
}> {
  dragStartPosition: ?{ x: number, y: number }
  initialValue: ?number

  static defaultProps = {
    value: 0.5,
    range: 100,
    disabled: false,
    size: 30,
    precision: 4
  };

  state = {
    active: false,
  };

  onMouseDown = (e: MouseEvent) => {
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = { x: e.clientX, y: e.clientY };
    this.initialValue = this.props.value;
    this.setState({ active: true });
  };

  onMouseMove = (e: MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
    const { dragStartPosition, initialValue } = this;
    const { disabled, onChange, range, precision } = this.props;

    invariant(dragStartPosition);
    invariant(initialValue);

    const y = dragStartPosition.y - e.clientY;

    if (disabled) return;

    let value;
    if (y > 0) {
      const upRange = Math.min(range, dragStartPosition.y - window.scrollY);
      value = Math.min(1, initialValue + (1 - initialValue) * y / upRange);
    } else {
      const downRange = Math.min(
        range,
        window.innerHeight + window.scrollY - dragStartPosition.y,
      );
      value = Math.max(0, initialValue * (downRange + y) / downRange);
    }

    const rounded = Math.round(value * Math.pow(10, precision)) /
      Math.pow(10, precision);

    onChange(rounded);
  };

  onMouseUp = (e: MouseEvent) => {
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = null;
    this.initialValue = null;
    this.setState({ active: false });
  };

  preventDefault = (e: MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  render() {
    const { disabled, value, size } = this.props;
    const { active } = this.state;
    const radius = size / 2;

    let className = 'knob'
    if (active) className += ' knob--active';
    if (disabled) className += ' knob--disabled';

    // include draggable and onDragStart to allow use of the knob component
    // inside a parent element using native html drag/drop
    return (
      <svg className={className}
        draggable={true}
        onDragStart={this.preventDefault}
        width={size}
        height={size}
        transform={`rotate(${(value - 0.5) * 300})`}
      >
        <circle
          cx={radius}
          cy={radius}
          r={radius}
          onMouseDown={this.onMouseDown}
        />
        <rect
          x={radius - 2}
          y={3}
          width={4}
          height={Math.min(radius - 3, 10)}
          rx={2}
          ry={2}
        />
      </svg>
    );
  }
}
