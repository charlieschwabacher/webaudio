// @flow
import React, { Component } from 'react';
import type { Clip as ClipAST } from '../../language';

export default class Arrange extends Component<{
  ast: Array<ClipAST>,
}> {
  render() {
    const { ast } = this.props;

    return (
      <div>
        Arrange...
        {ast.map(clip => <p>{clip.sequence} {clip.start} {clip.length}</p>)}
      </div>
    );
  }
}
