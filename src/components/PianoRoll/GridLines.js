// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import getRows from './getRows';

export default class GridLines extends Component<{
  width: number,
  height: number,
  yScale: number,
  xScale: number,
  yScroll: number,
  xScroll: number,
  quantization: number,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      xScale,
      yScale,
      xScroll,
      yScroll,
      quantization,
    } = this.props;

    const squareHeight = height / yScale;
    const numCols = xScale * quantization;
    const minCol = xScroll * quantization;
    const squareWidth = width / numCols;

    const rows = getRows(yScale, yScroll);
    const cols = getRows(numCols, minCol);

    return (
      <g>
        {[
          // horizontal lines (separating e and f)
          ...rows.map(({ val, pos }, i) =>
            i !== 0 && val % 12 === 5 &&
            <line
              key={'h' + i}
              x1={0}
              y1={height - pos * squareHeight}
              x2={width}
              y2={height - pos * squareHeight}
              className='piano-roll__grid-line'
            />
          ),
          // vertical lines
          ...cols.map(({ val, pos }, i) =>
            i !== 0 && val % quantization !== 0 &&
            <line
              key={'v' + i}
              x1={pos * squareWidth}
              y1={0}
              x2={pos * squareWidth}
              y2={height}
              className='piano-roll__grid-line'
            />
          ),
          // strong horizontal lines (seperating b and c)
          ...rows.map(({ val, pos }, i) =>
            i !== 0 && val % 12 === 0 &&
            <line
              key={'h' + i}
              x1={0}
              y1={height - pos * squareHeight}
              x2={width}
              y2={height - pos * squareHeight}
              className='piano-roll__grid-line piano-roll__grid-line--strong'
            />
          ),
          // strong vertical lines
          ...cols.map(({ val, pos }, i) =>
            i !== 0 && val % quantization === 0 &&
            <line
              key={'v' + i}
              x1={pos * squareWidth}
              y1={0}
              x2={pos * squareWidth}
              y2={height}
              className='piano-roll__grid-line piano-roll__grid-line--strong'
            />
          ),
        ]}
      </g>
    );
  }
}
