// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

export default class Selection extends Component<{
  selectionOrigin: ?{key: number, start: number},
  selectionPosition: ?{key: number, start: number},
  width: number,
  height: number,
  yScale: number,
  xScale: number,
  yScroll: number,
  xScroll: number,
  quantization: number,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      selectionOrigin,
      selectionPosition,
      xScroll,
      yScroll,
      xScale,
      yScale,
      quantization,
    } = this.props;

    let el;
    if (selectionOrigin != null && selectionPosition != null) {
      const squareHeight = height / yScale;
      const squareWidth = width / xScale / quantization;
      const fromKey = Math.max(selectionOrigin.key, selectionPosition.key);
      const keyWidth = Math.abs(selectionOrigin.key - selectionPosition.key);
      const fromBeat =  Math.min(selectionOrigin.start, selectionPosition.start);
      const beatWidth = Math.abs(selectionOrigin.start - selectionPosition.start);
      const x = (fromBeat - xScroll) * quantization * squareWidth;
      const y = (yScale + yScroll - fromKey - 1) * squareHeight;
      const w = (beatWidth * quantization + 1) * squareWidth;
      const h = (keyWidth + 1) * squareHeight;
      el = (
        <rect
          className='piano-roll__selection'
          x={x}
          y={y}
          width={w}
          height={h}
        />
      );
    }

    return (
      <g>{el}</g>
    );
  }
}
