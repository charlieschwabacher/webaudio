// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import DragZone from './DragZone';
import Keys from './Keys';
import Notes from './Notes';
import GridShading from './GridShading';
import Selection from './Selection';
import GridLines from './GridLines';
import PlaybackMarker from './PlaybackMarker';
import LoopControl from './LoopControl';
import LoopShading from './LoopShading';
import Transport from './Transport';
import noteBounds from './noteBounds';
import { id, invariant, keyMapObj, union, activeKeys } from '../../utils';
import {
  Sequence as SequenceAST,
  Note as NoteAST,
} from '../../language';
import type { Project } from '../../audio';
import './PianoRoll.css';

type Position = { key: number, start: number };
type Coordinate = { x: number, y: number };

const QUANTIZATION_OPTIONS = [1,2,3,4,6,8,12,16,24,32];

export default class PianoRoll extends Component<{
  keyWidth: number,
  lineWidth: number,
  transportHeight: number,
  loopControlHeight: number,
  resizeHandleWidth: number,
  dragScrollInterval: number,
  minXScale: number,
  minYScale: number,
  maxYScale: number,
  project: Project,
  sequence: SequenceAST,
  removeNotes: (sequence: string, ids: Array<string>) => void,
  updateNotes: (sequence: string, notes: {[string]: NoteAST}) => void,
  flattenNotes: (sequence: string) => void,
  updateLoop: (sequence: string, start: number, length: number) => void,
}, {
  height: number,
  width: number,
  selectedNotes: Set<string>,
  quantization: number,
  xScale: number,
  yScale: number,
  xScroll: number,
  yScroll: number,
  selectionOrigin: ?Position,
  selectionPosition: ?Position,
  resizeDirection: ?('left' | 'right'),
  resizeTarget: ?NoteAST,
  translateTarget: ?NoteAST,
}> {
  scrollDeltaX: number
  scrollDeltaY: number
  container: ?Element
  grid: ?Element
  originalValue: ?Array<NoteAST>
  dragStartPosition: ?Position
  currentDragPosition: ?Position
  dragStartCoordinate: ?Coordinate
  latestMouseEvent: ?MouseEvent
  dragScrollInterval: ?IntervalID

  static defaultProps = {
    // width of lines
    lineWidth: 1,

    // width of key markers
    keyWidth: 45,

    // height of transport and loop controls
    transportHeight: 25,
    loopControlHeight: 10,

    // min and max scales of viewport
    minXScale: 2,
    // max x scale is calcualted based on the max note value and loop length
    minYScale: 24,
    maxYScale: 128,

    // maximum width from the left/right edge of a note where a drag will resize
    // rather than translate the note
    resizeHandleWidth: 10,

    // drag scroll interval
    dragScrollInterval: 150,
  }

  constructor(props: Object) {
    super(props);

    this.state = {
      // dimensions of the dom element
      height: 0,
      width: 0,

      // an array of note ids representing the currently selected notes
      // TODO: now using array index
      selectedNotes: new Set(),

      // moved notes will be quantized to 1/@state.quanitization beats
      quantization: 4,

      // x scale and scroll values are in beats
      // y scale and scroll values are in semitones
      // xScale: 9,
      // yScale: 24,
      // xScroll: 0,
      // yScroll: 48,
      ...this.autoScale(props.sequence),

      // mouse interaction state
      selectionOrigin: null,
      selectionPosition: null,
      resizeTarget: null,
      resizeDirection: null,
      translateTarget: null,
    };
  }

  shouldComponentUpdate(nextProps: Object, nextState: Object): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  componentWillReceiveProps(nextProps: Object) {
    if (
      nextProps.sequence != null &&
      nextProps.sequence._id !== this.props.sequence._id
    ) {
      this.setState(this.autoScale(nextProps.sequence));
    }
  }

  autoScale(
    sequence: SequenceAST
  ): {
    xScroll: number,
    xScale: number,
    yScroll: number,
    yScale: number,
  } {
    const { minXScale, minYScale } = this.props;
    const start = sequence.start.value;
    const length = sequence.length.value;

    const {
      minKey,
      maxKey,
      minStart,
      maxEnd,
    } = noteBounds(sequence.notes.items);

    const yScale = Math.max(minYScale, maxKey - minKey + 3);
    const yScroll = Math.ceil((minKey + maxKey - yScale) / 2);
    const xScroll = Math.min(start, minStart);
    const xScale = Math.max(minXScale, Math.max(start + length, maxEnd) - xScroll);

    return {
      yScroll,
      xScroll,
      xScale,
      yScale,
    };
  }

  // replace native scrolling behavior
  onWheel = (e: WheelEvent) => {
    e.preventDefault();
    e.stopPropagation();

    const { maxYScale } = this.props;
    const {
      width,
      height,
      xScale,
      yScale,
      xScroll,
      yScroll,
      quantization,
    } = this.state;
    const xQuantum = width / xScale / quantization;
    const yQuantum = height / yScale;
    const maxXScale = this.maxXScale();

    // update scroll deltas
    this.scrollDeltaX += e.deltaX;
    this.scrollDeltaY -= e.deltaY;

    let nextXScroll = xScroll;
    let nextYScroll = yScroll;

    // get updated scroll state
    if (Math.abs(this.scrollDeltaX) > xQuantum) {
      const quanta = (this.scrollDeltaX > 0 ? Math.floor : Math.ceil)(this.scrollDeltaX / xQuantum);
      this.scrollDeltaX -= quanta * xQuantum;
      nextXScroll = Math.max(0, Math.min(maxXScale - xScale, nextXScroll + quanta / quantization));
    }
    if (Math.abs(this.scrollDeltaY) > yQuantum) {
      const quanta = (this.scrollDeltaY > 0 ? Math.floor : Math.ceil)(this.scrollDeltaY / yQuantum);
      this.scrollDeltaY -= quanta * yQuantum;
      nextYScroll = Math.max(0, Math.min(maxYScale - yScale, nextYScroll + quanta));
    }

    // snap to quantum
    if (e.deltaX !== 0 && (nextXScroll * quantization) % 1 !== 0) {
      nextXScroll = (
        e.deltaX > 0
        ? Math.ceil(nextXScroll * quantization) / quantization
        : Math.floor(nextXScroll * quantization) / quantization
      );
    }
    if (e.deltaY !== 0 && nextYScroll % 1 !== 0) {
      nextYScroll = (
        e.deltaY > 0
        ? Math.floor(nextYScroll)
        : Math.ceil(nextYScroll)
      );
    }

    // set state
    if (nextXScroll !== xScroll || nextYScroll !== yScroll) {
      this.setState({
        xScroll: nextXScroll,
        yScroll: nextYScroll,
      });
    }
  }

  // track width and height of the component whenever they change
  updateDimensions = () => {
    const { container } = this;
    invariant(container);

    const { width, height } = container.getBoundingClientRect();
    this.setState({ width, height });
  }

  onKeyDown = (e: KeyboardEvent) => {
    const { sequence, removeNotes } = this.props;
    const { selectedNotes, quantization } = this.state;
    const noteIds = Array.from(selectedNotes);
    const notes = noteIds.map(id => sequence.notes.itemsById[id]);
    const shift = activeKeys.has('Shift');

    switch(e.key) {
      case 'ArrowLeft':
        return this.translateNotes(notes, 0, (shift ? -4 : -1) / quantization, true);
      case 'ArrowRight':
        return this.translateNotes(notes, 0, (shift ? 4 : 1) / quantization, true);
      case 'ArrowUp':
        return this.translateNotes(notes, shift ? 12 : 1, 0, true);
      case 'ArrowDown':
        return this.translateNotes(notes, shift ? -12 : -1, 0, true);
      case 'Backspace':
        return removeNotes(sequence.name, noteIds);
      case 'Escape':
        return this.selectNone();
      case 'a':
        return activeKeys.has('Meta') && this.selectAll();
      default:
        return;
    }
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
    window.addEventListener('keydown', this.onKeyDown);

    this.scrollDeltaX = 0;
    this.scrollDeltaY = 0;
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
    window.removeEventListener('keydown', this.onKeyDown);

    if (this.dragScrollInterval) {
      clearInterval(this.dragScrollInterval);
    }
  }

  getRelativePosition(x: number, y: number): { key: number, start: number } {
    const { grid } = this;
    invariant(grid);

    const { top, left } = grid.getBoundingClientRect()
    const {
      width,
      height,
      xScale,
      yScale,
      xScroll,
      yScroll,
      quantization,
    } = this.state;
    const { keyWidth } = this.props;
    const gridWidth = width - keyWidth;

    // adjust for possible unqantized scrolling
    const yAdjustment = yScroll % 1 * (height / yScale);
    const flooredScroll = Math.floor(xScroll * quantization) / quantization;
    const xAdjustment = (xScroll - flooredScroll) * width / xScale;

    const key = Math.floor((height - y + top + yAdjustment) / height * yScale) + Math.floor(yScroll);
    const start = Math.floor((x - left + xAdjustment) / gridWidth * xScale * quantization) / quantization + flooredScroll;

    return { key, start };
  }

  // return an array of note ids, representing notes in the squence intersecting
  // a rectangle defined by the arguments from {key, start}, and to {key, start}
  notesSelectedBy(from: Position, to: Position): Set<string> {
    const minKey = Math.min(from.key, to.key);
    const maxKey = Math.max(from.key, to.key);
    const minEnd = Math.min(from.start, to.start);
    const maxStart = Math.max(from.start, to.start);

    const notes = new Set();
    this.props.sequence.notes.items.forEach(note => {
      if (
        note.key >= minKey &&
        note.key <= maxKey &&
        note.start + note.length > minEnd &&
        note.start <= maxStart
      ) {
        notes.add(note._id);
      }
    });

    return notes;
  }

  selectNone() {
    this.setState({ selectedNotes: new Set() });
  }

  selectAll() {
    const { sequence: { notes } } = this.props;
    this.setState({ selectedNotes: new Set(notes.items.map(n => n._id)) });
  }

  beginDrag(
    e: MouseEvent,
    onDrag: (e: MouseEvent) => void,
    onDragEnd: ?() => void,
  ) {
    const endDrag = () => {
      window.removeEventListener('mousemove', onDrag);
      window.removeEventListener('mouseup', endDrag);
      this.dragStartPosition = null;
      this.currentDragPosition = null;
      this.dragStartCoordinate = null;
      onDragEnd != null && onDragEnd();
    }

    window.addEventListener('mousemove', onDrag);
    window.addEventListener('mouseup', endDrag);

    const position = this.getRelativePosition(e.clientX, e.clientY);
    this.dragStartPosition = position;
    this.currentDragPosition = position;
    this.dragStartCoordinate = { x: e.clientX, y: e.clientY };
  }

  updateDragScrolling = () => {
    const { xScroll, yScroll, quantization } = this.state;
    const { latestMouseEvent, grid } = this;
    invariant(grid);

    // mouse position must be set or we cannot continue
    if (latestMouseEvent == null) {
      return;
    };

    const x = latestMouseEvent.clientX;
    const y = latestMouseEvent.clientY;
    const { bottom, top, left, right } = grid.getBoundingClientRect();

    // handle scroll
    const stateChanges = {};

    if (y <= top) {
      stateChanges.yScroll = Math.min(127, yScroll + 1);
    } else if (y >= bottom) {
      stateChanges.yScroll = Math.max(0, yScroll - 1);
    }

    if (x <= left) {
      stateChanges.xScroll = Math.max(0, xScroll - 1 / quantization);
    } else if (x >= right - 1) {
      stateChanges.xScroll = xScroll + 1 / quantization;
    }

    this.setState(stateChanges);
    if (Object.keys(stateChanges).length > 0) {
      this.onDragNote(latestMouseEvent);
    }
  }

  onMouseDownGrid = (e: MouseEvent) => {
    this.beginDrag(e, this.onDragGrid, this.onDragEndGrid);
    this.setState({
      selectionOrigin: this.getRelativePosition(e.clientX, e.clientY),
      selectedNotes: (
        activeKeys.has('Shift')
          ? this.state.selectedNotes
          : new Set()
      ),
    });
  }

  onDragGrid = (e: MouseEvent) => {
    const position = this.getRelativePosition(e.clientX, e.clientY);
    const dragStartPosition = this.dragStartPosition;
    invariant(dragStartPosition);

    const selected = this.notesSelectedBy(dragStartPosition, position);
    this.setState({
      selectionPosition: position,
      selectedNotes: (
        activeKeys.has('Shift')
          ? union(this.state.selectedNotes, selected)
          : selected
      ),
    });
  }

  onDragEndGrid = () => {
    this.setState({ selectionOrigin: null, selectionPosition: null });
  }

  onDoubleClickGrid = (e: MouseEvent) => {
    e.stopPropagation();

    // add a note
    const sequence = this.props.sequence.name;
    const { key, start } = this.getRelativePosition(e.clientX, e.clientY);
    const length = 1 / this.state.quantization;
    const note = { _id: id(), key, start, length };
    this.props.updateNotes(sequence, { [note._id]: note });
  }

  // A mouse down event on a note will select the note, and can be the beginning
  // of a translation or resize of the selected notes.
  onMouseDownNote = (e: Object) => {
    e.stopPropagation();
    this.beginDrag(e, this.onDragNote, this.onDragEndNote);

    const id = e.target.dataset.id;
    const note = this.props.sequence.notes.itemsById[id];
    const position = e.target.getBoundingClientRect();

    const stateChanges = {};

    // handle note selection
    if (activeKeys.has('Shift')) {
      if (this.state.selectedNotes.has(id)) {
        stateChanges.selectedNotes = new Set(this.state.selectedNotes);
        stateChanges.selectedNotes.delete(id);
      } else {
        stateChanges.selectedNotes = new Set(this.state.selectedNotes);
        stateChanges.selectedNotes.add(id);
      }
    } else {
      if (this.state.selectedNotes.has(id)) {
        stateChanges.selectedNotes = this.state.selectedNotes;
      } else {
        stateChanges.selectedNotes = new Set([id]);
      }
    }

    // cache original values of selected notes
    const originalValue = [];
    const notes = this.props.sequence.notes.itemsById;
    Object.keys(notes).forEach((id) => {
      if (stateChanges.selectedNotes.has(id)) {
        originalValue.push(notes[id]);
      }
    });

    const handleSize = Math.max(0, Math.min(this.props.resizeHandleWidth, (position.width - this.props.resizeHandleWidth) / 2));

    // handle resize
    if (position.left > e.clientX - handleSize) {
      stateChanges.resizeTarget = note;
      stateChanges.resizeDirection = 'left';
      // @dragActionCursor = Pointer.set 'w-resize', 2, @dragActionCursor
    } else if (position.right < e.clientX + handleSize) {
      stateChanges.resizeTarget = note;
      stateChanges.resizeDirection = 'right';
      // @dragActionCursor = Pointer.set 'e-resize', 2, @dragActionCursor
    // handle translate
    } else {
      stateChanges.translateTarget = note;
      // @dragActionCursor = Pointer.set 'move', 2, @dragActionCursor
    }

    // apply state changes
    this.originalValue = originalValue;
    this.setState(stateChanges);

    // begin polling for drag scroll
    this.dragScrollInterval = setInterval(
      this.updateDragScrolling,
      this.props.dragScrollInterval
    );
  }

  translateNotes(
    notes: Array<NoteAST>,
    keyDelta: number,
    startDelta: number,
    scroll?: boolean,
  ): void {
    const { sequence, updateNotes } = this.props;
    const { yScroll, yScale, xScroll, xScale } = this.state;
    const { maxKey, minKey, minStart, maxEnd } = noteBounds(notes);

    // constrain x & y
    keyDelta = Math.min(keyDelta + maxKey, 127) - maxKey;
    keyDelta = Math.max(keyDelta + minKey, 0) - minKey;
    startDelta = Math.max(startDelta + minStart, xScroll) - minStart;

    // update scroll
    if (scroll) {
      const stateChanges = {};

      if (keyDelta > 0 && maxKey + keyDelta >= yScroll + yScale) {
        stateChanges.yScroll = maxKey + keyDelta - yScale + 1;
      } else if (keyDelta < 0 && minKey + keyDelta < yScroll) {
        stateChanges.yScroll = minKey + keyDelta;
      }

      if (startDelta > 0 && maxEnd + startDelta >= xScroll + xScale) {
        stateChanges.xScroll = maxEnd + startDelta - xScale;
      } else if (startDelta < 0 && minStart + startDelta < xScroll) {
        stateChanges.xScroll = minStart + startDelta;
      }

      this.setState(stateChanges);
    }

    // apply changes
    const currentNotes = sequence.notes.itemsById;
    const updated = notes.map(note => {
      return new NoteAST({
        // copy up to date text range information from latest version
        ...currentNotes[note._id],
        key: note.key + keyDelta,
        start: note.start + startDelta,
      });
    });
    updateNotes(sequence.name, keyMapObj(updated, n => n._id));
  }

  resizeNotesLeft(
    notes: Array<NoteAST>,
    delta: number,
    minLength: number,
  ): void {
    const { sequence, updateNotes } = this.props;
    const { minStart } = noteBounds(notes);

    // constrain x
    delta = Math.max(delta + minStart, 0) - minStart;

    const currentNotes = sequence.notes.itemsById;
    const updated = notes.map(note => {
      let start = note.start + delta;
      start = Math.min(start, note.start + note.length - minLength);
      return new NoteAST({
        ...currentNotes[note._id],
        start,
        length: note.start + note.length - start,
      });
    });
    updateNotes(sequence.name, keyMapObj(updated, n => n._id));
  }

  resizeNotesRight(
    notes: Array<NoteAST>,
    delta: number,
    minLength: number,
  ): void {
    const { sequence, updateNotes } = this.props;

    const currentNotes = sequence.notes.itemsById;
    const updated = notes.map(note => new NoteAST({
      ...currentNotes[note._id],
      length: Math.max(minLength, note.length + delta),
    }));
    updateNotes(sequence.name, keyMapObj(updated, n => n._id));
  }

  onDragNote = (e: MouseEvent) => {
    // keep latest mouse event for drag scrolling interval
    this.latestMouseEvent = e;

    const position = this.getRelativePosition(e.clientX, e.clientY);
    const {
      translateTarget,
      resizeTarget,
      xScroll,
      xScale,
      yScroll,
      yScale,
      quantization,
    } = this.state;
    const { originalValue, dragStartPosition, currentDragPosition } = this;
    invariant(originalValue != null);
    invariant(dragStartPosition != null);
    invariant(currentDragPosition != null);

    // return early if there is no change in position
    if (
      position.key === currentDragPosition.key &&
      position.start === currentDragPosition.start
    ) {
      return;
    }

    // update position
    this.currentDragPosition = position;

    // handle translation of notes
    if (translateTarget != null) {
      // do not traslate notes past the extent of the screen in response to
      // mouse events - instead the drag scrolling interval will handle this
      const keyDelta = Math.max(yScroll, Math.min(yScroll + yScale - 1, position.key)) - dragStartPosition.key;

      const dragStartRelNote = dragStartPosition.start - translateTarget.start;
      const noteOverhang = translateTarget.length - dragStartRelNote;
      const startDelta = Math.max(xScroll, Math.min(xScroll + xScale - noteOverhang, position.start)) - dragStartPosition.start;

      // make sure we snap the dragged note to the grid, not move by the quantization amount
      const snappedDelta = Math.round((translateTarget.start + startDelta) * quantization) / quantization - translateTarget.start;

      this.translateNotes(originalValue, keyDelta, snappedDelta);
    // handle resize of notes
    } else if (resizeTarget != null) {
      const minLength = 1 / this.state.quantization;
      const maxDelta = resizeTarget.length - minLength;
      const delta = Math.max(xScroll, Math.min(xScroll + xScale, position.start)) - dragStartPosition.start;
      // console.log('resize', dragStartPosition.start, position.start, delta, xScroll, xScale);

      if (this.state.resizeDirection === 'right') {
        let snapAmount = (resizeTarget.start + resizeTarget.length - dragStartPosition.start) % (1 / quantization);
        snapAmount = snapAmount < 1e-10 ? 0 : 1 / quantization - snapAmount;
        this.resizeNotesRight(originalValue, Math.max(-maxDelta, delta + snapAmount), minLength);
      } else if (this.state.resizeDirection === 'left') {
        const snapAmount = -(resizeTarget.start - dragStartPosition.start) % (1 / quantization);
        this.resizeNotesLeft(originalValue, Math.min(maxDelta, delta + snapAmount), minLength);
      }
    }
  }

  onDragEndNote = () => {
    const { originalValue } = this;
    const { sequence, updateNotes, flattenNotes } = this.props;

    // if the alt key is held, copy notes
    if (originalValue != null && activeKeys.has('Alt')) {
      updateNotes(
        sequence.name,
        originalValue.reduce((memo, { key, start, length }) => {
          const _id = id();
          memo[_id] = { _id, key, start, length };
          return memo;
        }, {})
      );
    }

    flattenNotes(sequence.name);

    this.originalValue = null;
    this.latestMouseEvent = null;
    this.setState({
      translateTarget: null,
      resizeTarget: null,
      resizeDirection: null,
    });

    if (this.dragScrollInterval) {
      clearInterval(this.dragScrollInterval);
      this.dragScrollInterval = null;
    }

    // Pointer.clear(this.dragActionCursor)
  }

  // TODO: change cursor
  onMouseMoveNote = (e: MouseEvent) => {
    // const { resizeHandleWidth } = this.state;
    // const position = e.target.getBoundingClientRect();
    // const handleSize = Math.max(0, Math.min(resizeHandleWidth, (position.width - resizeHandleWidth) / 2));
    //
    // if (position.left > e.clientX - handleSize) {
    //   this.noteHoverCursor = Pointer.set('w-resize', 1, this.noteHoverCursor)
    // } else if (position.right < e.clientX + handleSize) {
    //   this.noteHoverCursor = Pointer.set('e-resize', 1, this.noteHoverCursor)
    // } else {
    //   Pointer.clear(this.noteHoverCursor);
    // }
  }

  onDoubleClickNote = (e: Object) => {
    e.stopPropagation();

    // remove a note
    const sequence = this.props.sequence.name;
    const id = e.target.dataset.id;
    this.props.removeNotes(sequence, [id]);
  }

  onChangeQuantization = (e: Object) => {
    const quantization = e.target.value;
    this.setState({
      quantization,
      xScroll: Math.floor(this.state.xScroll * quantization) / quantization,
    });
  }

  maxXScale() {
    const {
      sequence: {
        notes,
        start: { value: start },
        length: { value: length },
      },
    } = this.props;

    return 1.5 * Math.max(
      8,
      start + length,
      notes.items.reduce((memo, note) =>
        Math.max(memo, note.start + note.length)
      , 0),
    );
  }

  onChangeXScale = (xScale: number, xScroll: number) => {
    if (xScale !== this.state.xScale) {
      this.setState({ xScale, xScroll });
    }
  }

  onChangeYScale = (yScale: number, yScroll: number) => {
    if (yScale !== this.state.yScale) {
      this.setState({ yScale, yScroll });
    }
  }

  onChangeLoop = (start: number, length: number) => {
    this.props.updateLoop(this.props.sequence.name, start, length);
  }

  render() {
    const {
      width,
      height,
      yScroll,
      yScale,
      xScroll,
      xScale,
      quantization,
      selectionOrigin,
      selectionPosition,
      selectedNotes,
      translateTarget,
      resizeTarget,
    } = this.state;
    const {
      minXScale,
      minYScale,
      maxYScale,
      keyWidth,
      transportHeight,
      loopControlHeight,
      lineWidth,
      sequence,
      project,
    } = this.props;
    const length = sequence.length.value;
    const start = sequence.start.value;

    const gridWidth = Math.max(0, width - keyWidth);
    const maxXScale = this.maxXScale();

    return (
      <div className='piano-roll'>
        <div className='piano-roll__header'>
          <DragZone
            scale={xScale}
            scroll={xScroll}
            min={minXScale}
            max={maxXScale}
            size={width}
            steps={quantization * xScale}
            direction='y'
            onChange={this.onChangeXScale}
          >
            <Transport
              width={gridWidth}
              height={transportHeight}
              xScroll={xScroll}
              xScale={xScale}
              quantization={quantization}
            />
          </DragZone>
          <LoopControl
            width={gridWidth}
            height={loopControlHeight}
            xScroll={xScroll}
            xScale={xScale}
            quantization={quantization}
            start={start}
            length={length}
            onChange={this.onChangeLoop}
          />
        </div>
        <div
          className='piano-roll__workspace'
          ref={el => this.container = el}
          onWheel={this.onWheel}
        >
          <DragZone
            scale={yScale}
            scroll={yScroll}
            min={minYScale}
            max={maxYScale}
            size={height}
            steps={maxYScale}
            direction='x'
            onChange={this.onChangeYScale}
          >
            <Keys
              width={keyWidth}
              height={height}
              yScroll={yScroll}
              yScale={yScale}
              lineWidth={lineWidth}
              onClickKey={null}
            />
          </DragZone>
          <svg
            className='piano-roll__grid'
            width={gridWidth}
            height={height}
            onMouseDown={this.onMouseDownGrid}
            onMouseUp={null}
            onClick={null}
            onDoubleClick={this.onDoubleClickGrid}
            ref={el => this.grid = el}
          >
            <GridShading
              width={gridWidth}
              height={height}
              yScale={yScale}
              yScroll={yScroll}
            />
            <LoopShading
              width={gridWidth}
              height={height}
              xScale={xScale}
              xScroll={xScroll}
              start={start}
              length={length}
            />
            <PlaybackMarker
              project={project}
              sequence={sequence}
              loopStart={sequence.start.value}
              loopSize={sequence.length.value}
              width={gridWidth}
              height={height}
              lineWidth={lineWidth}
              xScroll={xScroll}
              xScale={xScale}
              quantization={quantization}
            />
            <GridLines
              width={gridWidth}
              height={height}
              yScale={yScale}
              xScale={xScale}
              yScroll={yScroll}
              xScroll={xScroll}
              quantization={quantization}
            />
            <Selection
              selectionOrigin={selectionOrigin}
              selectionPosition={selectionPosition}
              width={gridWidth}
              height={height}
              yScale={yScale}
              xScale={xScale}
              yScroll={yScroll}
              xScroll={xScroll}
              quantization={quantization}
            />
            <Notes
              sequence={sequence}
              selectedNotes={selectedNotes}
              dragOriginalValue={this.originalValue}
              translateTarget={translateTarget}
              resizeTarget={resizeTarget}
              width={gridWidth}
              height={height}
              yScale={yScale}
              xScale={xScale}
              yScroll={yScroll}
              xScroll={xScroll}
              lineWidth={lineWidth}
              quantization={quantization}
              onMouseDown={this.onMouseDownNote}
              onMouseMove={this.onMouseMoveNote}
              onDoubleClick={this.onDoubleClickNote}
            />
          </svg>
        </div>
        <div className='piano-roll__config'>
          <select value={quantization} onChange={this.onChangeQuantization}>
            {
              QUANTIZATION_OPTIONS.map(option =>
                <option key={option} value={option}>1 / {option}</option>
              )
            }
          </select>
        </div>
      </div>
    );
  }
}
