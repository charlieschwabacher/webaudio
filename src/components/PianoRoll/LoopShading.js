// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';

export default class LoopShading extends Component<{
  width: number,
  height: number,
  xScale: number,
  xScroll: number,
  start: number,
  length: number,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      xScale,
      xScroll,
      start,
      length,
    } = this.props;

    const x1 = (start - xScroll) / xScale * width;
    const x2 = (start + length - xScroll) / xScale * width;

    return (
      <g>
        {
          start > xScroll &&
          <rect
            className='piano-roll__loop-shade'
            x={0}
            y={0}
            width={x1}
            height={height}
          />
        }
        {
          start + length < xScroll + xScale &&
          <rect
            className='piano-roll__loop-shade'
            x={x2}
            y={0}
            width={width - x2}
            height={height}
          />
        }
      </g>
    );
  }
}
