// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import KEY_PATTERN from './KEY_PATTERN';
import getRows from './getRows';

export default class Keys extends Component<{
  width: number,
  height: number,
  yScroll: number,
  yScale: number,
  maxFontSize: number,
  minFontSize: number,
  activeNotes?: Set<number>,
}> {
  static defaultProps = {
    maxFontSize: 10,
    minFontSize: 8,
  }

  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      yScale,
      yScroll,
      activeNotes,
      maxFontSize,
      minFontSize,
    } = this.props;
    const keyHeight = height / yScale;
    const fontSize = Math.min(maxFontSize, keyHeight);

    const rows = getRows(yScale, yScroll);

    return (
      <svg
        className='piano-roll__keys'
        width={width}
        height={height}
      >
        {[
          // keys
          ...rows.map(({ val, pos }, i) =>
            <rect
              key={'k' + i}
              className={
                activeNotes && activeNotes.has(val)
                ? 'piano-roll__key--active'
                : KEY_PATTERN[val % 12] === 0
                ? 'piano-roll__key--black'
                : 'piano-roll__key--white'
              }
              x={0}
              y={height - (pos + 1) * keyHeight}
              width={width}
              height={keyHeight}
            />
          ),
          // lines (only betweeb b and c, e and f)
          ...rows.map(({ val, pos }, i) =>
            i !== 0 && (val % 12 === 0 || val % 12 === 5)
            ? <line
              key={'l' + i}
              className='piano-roll__grid-line'
              x1={0}
              x2={width}
              y1={height - pos * keyHeight}
              y2={height - pos * keyHeight}
            />
            : null
          ),
          // labels
          ...(
            fontSize > minFontSize
            ? rows.map(({ val, pos }, i) =>
              val % 12 === 0
              ? <text
                key={'t' + i}
                className='piano-roll__text'
                fontSize={fontSize}
                x={8}
                y={height - (pos + 0.5) * keyHeight}
              >
                {`C${Math.floor(val / 12) - 2}`}
              </text>
              : null
            )
            : []
          ),
        ]}
      </svg>
    );
  }
}
