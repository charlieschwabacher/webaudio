// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import getRows from './getRows';

export default class Transport extends Component<{
  width: number,
  height: number,
  xScroll: number,
  xScale: number,
  quantization: number,
  fontSize: number,
  minMarkerSpacing: number,
}> {
  static defaultProps = {
    minMarkerSpacing: 60,
    fontSize: 10,
  };

  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      xScale,
      xScroll,
      fontSize,
      quantization,
    } = this.props;

    const numCols = xScale * quantization;
    const minCol = xScroll * quantization;
    const cols = getRows(numCols, minCol);

    return (
      <svg width={width} height={height} className="piano-roll__transport">
        {
          cols.map(({ val, pos }, i) =>
            // show only whole beats (allowing for float rounding error)
            Math.round(val / quantization * 10000) % 10000 === 0
            ? <text
              key={'t' + i}
              className='piano-roll__text'
              x={pos / numCols * width}
              y={height / 2}
              textAnchor={i === 0 ? 'start' : 'middle'}
              fontSize={fontSize}
            >
              {Math.floor(val / quantization)}
            </text>
            : null
          )
        }
      </svg>
    );
  }
}
