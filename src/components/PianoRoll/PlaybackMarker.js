// @flow
import React, { Component } from 'react';
import live from '../live';
import type { Project } from '../../audio';
import type { Sequence } from '../../language';

export default live(
  class PlaybackMarker extends Component<{
    project: Project,
    sequence: Sequence,
    loopStart: number,
    loopSize: number,
    width: number,
    height: number,
    xScale: number,
    xScroll: number,
    quantization: number,
  }> {
    shown: boolean

    render() {
      const {
        project,
        sequence,
        width,
        height,
        loopStart,
        loopSize,
        xScale,
        xScroll,
        quantization,
      } = this.props;

      const elapsed = project.context.currentTime - project.globalStartTime;
      const position = loopStart + (elapsed * project.ast.project.tempo.value / 60) % loopSize;
      const numCols = xScale * quantization;
      const squareWidth = width / numCols;

      let el;
      if (
        project.playing &&
        project.session.modules[sequence.target.value].playing.sequence === sequence.name &&
        position >= Math.floor(xScroll) &&
        position <= xScroll + xScale
      ) {
        const x = (Math.floor(position * quantization) - xScroll * quantization) * squareWidth;

        el = (
          <rect
            className="piano-roll__playback-marker"
            key='pb'
            x={x}
            y={0}
            width={squareWidth}
            height={height}
          />
        );
        this.shown = true;
      } else {
        this.shown = false;
      }

      return (
        <g>{el}</g>
      );
    }
  }
);
