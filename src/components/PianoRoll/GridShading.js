// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import KEY_PATTERN from './KEY_PATTERN';
import getRows from './getRows';


export default class GridShading extends Component<{
  width: number,
  height: number,
  yScale: number,
  yScroll: number,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      width,
      height,
      yScale,
      yScroll,
    } = this.props;

    const squareHeight = height / yScale;

    const rows = getRows(yScale, yScroll);

    return (
      <g>
        {
          // row shading
          rows.map(({ val, pos }, i) =>
            KEY_PATTERN[val % 12] === 0 &&
            <rect
              key={i}
              x={0}
              y={height - (pos + 1) * squareHeight}
              width={width}
              height={squareHeight}
              className='piano-roll__grid-shade'
            />
          )
        }
      </g>
    );
  }
}
