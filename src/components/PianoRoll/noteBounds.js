// @flow
import type { Note as NoteAST } from '../../language';

export default function noteBounds(
  notes: Array<NoteAST>,
): { maxKey: number, minKey: number, minStart: number, maxEnd: number } {
  return notes.reduce(
    (memo, note) => {
      if (note.key < memo.minKey) {
        memo.minKey = note.key;
      }

      if (note.key > memo.maxKey) {
        memo.maxKey = note.key;
      }

      if (note.start < memo.minStart) {
        memo.minStart = note.start;
      }

      if (note.start + note.length > memo.maxEnd) {
        memo.maxEnd = note.start + note.length;
      }

      return memo;
    },
    { maxKey: 0, minKey: 128, minStart: Infinity, maxEnd: 0 },
  );
}
