// @flow
import React, { Component } from 'react';
import shallowCompare from 'react-addons-shallow-compare';
import Note from './Note';
import { activeKeys } from '../../utils';
import type {
  Sequence as SequenceAST,
  Note as NoteAST,
} from '../../language';

export default class Notes extends Component<{
  sequence: SequenceAST,
  selectedNotes: Set<string>,
  dragOriginalValue: ?Array<NoteAST>,
  translateTarget: ?NoteAST,
  resizeTarget: ?NoteAST,
  width: number,
  height: number,
  lineWidth: number,
  yScale: number,
  xScale: number,
  yScroll: number,
  xScroll: number,
  quantization: number,
  onMouseDown?: (e: MouseEvent) => void,
  onMouseMove?: (e: MouseEvent) => void,
  onMouseOut?: (e: MouseEvent) => void,
  onClick?: (e: MouseEvent) => void,
  onDoubleClick?: (e: MouseEvent) => void,
}> {
  noteOnScreen(note: NoteAST) {
    const { start, key, length } = note;
    const { xScale, xScroll, yScale, yScroll } = this.props;
    return (
      start <= xScroll + xScale &&
      start + length >= xScroll &&
      key >= Math.floor(yScroll) &&
      key <= yScroll + yScale
    );
  }

  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      sequence,
      width,
      height,
      // lineWidth,
      xScroll,
      yScroll,
      xScale,
      yScale,
      quantization,
      selectedNotes,
      dragOriginalValue,
      translateTarget,
      resizeTarget,
      onMouseDown,
      onClick,
      onDoubleClick,
    } = this.props;

    const notes = sequence.notes;

    // we need these to be positive numbers in order to render
    if (width <= 0 || height <= 0) return <g />;

    const squareHeight = height / yScale;
    const numCols = xScale * quantization;
    const squareWidth = width / numCols;

    const els = [];

    // ghost notes
    if (
      translateTarget != null &&
      dragOriginalValue != null &&
      activeKeys.has('Alt')
    ) {
      dragOriginalValue.forEach(note => {
        if (!this.noteOnScreen(note)) return;

        const { _id, start, key, length } = note;
        const x = (start - xScroll) * squareWidth * quantization;
        const y = (yScale + yScroll - key - 1) * squareHeight;
        const w = squareWidth * length * quantization;

        els.push(
          <Note
            key={'g' + _id}
            ghost={true}
            x={x}
            y={y}
            width={w}
            height={squareHeight}
          />
        );
      });
    }

    // TODO: notes need ids, for now just using index but id will be better once
    // they are editable
    notes.items.forEach(note => {
      if (!this.noteOnScreen(note)) return;

      const { _id, start, key, length } = note;
      const x = (start - xScroll) * squareWidth * quantization;
      const y = (yScale + yScroll - key - 1) * squareHeight;
      const w = squareWidth * length * quantization;
      const h = squareHeight;
      const selected = selectedNotes != null && selectedNotes.has(_id);
      const active = (
        (translateTarget && translateTarget._id === _id) ||
        (resizeTarget && resizeTarget._id === _id)
      );

      els.push(
        <Note
          key={_id}
          id={`${_id}`}
          selected={selected}
          active={active}
          x={x}
          y={y}
          width={w}
          height={h}
          onMouseDown={onMouseDown}
          onClick={onClick}
          onDoubleClick={onDoubleClick}
        />
      );
    });

    return (
      <g>{els}</g>
    );
  }
}
