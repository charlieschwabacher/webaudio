// @flow
export default function getRows(
  scale: number,
  scroll: number,
): Array<{ val: number, pos: number }> {
  const remainder = scroll % 1;
  
  return (new Array(scale + Math.ceil(remainder))).fill().map((_, i) =>
    ({ pos: i - remainder, val: Math.floor(scroll) + i })
  );
}
