// @flow
import React from 'react';

type NoteProps = {
  x: number,
  y: number,
  width: number,
  height: number,
  id?: string,
  selected?: ?boolean,
  active?: ?boolean,
  ghost?: ?boolean,
  onMouseDown?: (e: MouseEvent) => void,
  onClick?: (e: MouseEvent) => void,
  onDoubleClick?: (e: MouseEvent) => void,
}

export default function Note(props: NoteProps) {
  const {
    id,
    x,
    y,
    width,
    height,
    selected,
    active,
    ghost,
    onMouseDown,
    onClick,
    onDoubleClick,
  } = props;

  const className = (
    'piano-roll__note'
    + (selected ? ' piano-roll__note--selected' : '' )
    + (active ? ' piano-roll__note--active' : '')
    + (ghost ? ' piano-roll__note--ghost' : '')
  );

  return (
    <g>
      <rect
        x={x}
        y={y}
        width={width}
        height={height}
        data-id={id}
        onMouseDown={onMouseDown}
        onClick={onClick}
        onDoubleClick={onDoubleClick}
        className={className}
        fill="url(#grad)"
      />
    </g>
  );
}
