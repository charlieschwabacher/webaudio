// @flow
import React, { Component } from 'react';
import { invariant } from '../../utils';

export default class LoopControl extends Component<{
  width: number,
  height: number,
  xScroll: number,
  xScale: number,
  quantization: number,
  start: number,
  length: number,
  resizeHandleWidth: number,
  onChange: (start: number, length: number) => void,
}> {
  dragType: ?('translate' | 'resize right' | 'resize left')
  dragStartPosition: ?number
  dragOriginalValue: ?{ start: number, length: number }
  bar: ?Element

  static defaultProps = {
    resizeHandleWidth: 10,
  };

  onMouseDown = (e: MouseEvent) => {
    e.preventDefault();

    const { bar } = this;
    invariant(bar);

    window.addEventListener('mousemove', this.onDrag);
    window.addEventListener('mouseup', this.onDragEnd);

    const { resizeHandleWidth, start, length } = this.props;

    this.dragStartPosition = e.clientX;
    this.dragOriginalValue = { start, length };

    // determine drag type
    const { left, width } = bar.getBoundingClientRect();
    if (e.clientX < left + resizeHandleWidth) {
      this.dragType = 'resize left';
    } else if (e.clientX > left + width - resizeHandleWidth) {
      this.dragType = 'resize right';
    } else {
      this.dragType = 'translate';
    }
  }

  onDrag = (e: MouseEvent) => {
    const { dragStartPosition, dragOriginalValue } = this;
    if (dragStartPosition == null || dragOriginalValue == null) {
      return this.onDragEnd();
    }

    const { width, xScale, quantization, onChange } = this.props;
    const { start, length } = dragOriginalValue;
    const stepWidth = width / xScale / quantization;
    const dxSteps = (e.clientX - dragStartPosition) / stepWidth;

    let dxBeats;
    switch (this.dragType) {
      case 'resize left':
        dxBeats = Math.round(dxSteps) / quantization;
        onChange(
          Math.min(start + length - 1 / quantization, Math.max(0, start + dxBeats)),
          Math.min(start + length, Math.max(1 / quantization, length - dxBeats)),
        );
        break;
      case 'resize right':
        dxBeats = Math.round(dxSteps) / quantization;
        onChange(
          start,
          Math.max(1 / quantization, length + dxBeats),
        );
        break;
      case 'translate':
        dxBeats = Math.round(dxSteps) / quantization;
        onChange(
          Math.max(0, start + dxBeats),
          length,
        );
        break;
      default:
        break;
    }
  }

  onDragEnd = () => {
    window.addEventListener('mousemove', this.onDrag);
    window.addEventListener('mouseup', this.onDragEnd);
    this.dragStartPosition = null;
    this.dragOriginalValue = null;
    this.dragType = null;
  }

  render() {
    const { width, height, xScroll, xScale, start, length } = this.props;
    const markerWidth = height * Math.sqrt(3) / 2;

    const p1 = (start - xScroll) / xScale;
    const p2 = (start + length - xScroll) / xScale;
    const x1 = Math.max(0, p1) * width;
    const x2 = Math.min(1, p2) * width;

    return (
      <svg width={width} height={height} className="piano-roll__loop-control">
        {
          x1 < width && x2 > 0 &&
          <g
            onMouseDown={this.onMouseDown}
            ref={el => this.bar = el}
          >
            <rect
              className="piano-roll__loop-control-bar"
              x={x1}
              y={0}
              width={x2 - Math.max(0, x1)}
              height={height}
            />
            {
              p1 >= 0 &&
              <polygon
                className="piano-roll__loop-control-marker"
                points={[
                  `${x1},0`,
                  `${x1 + markerWidth},${height / 2}`,
                  `${x1},${height}`,
                ].join(' ')}
              />
            }
            {
              p2 <= 1 &&
              <polygon
                className="piano-roll__loop-control-marker"
                points={[
                  `${x2},0`,
                  `${x2 - markerWidth},${height / 2}`,
                  `${x2},${height}`,
                ].join(' ')}
              />
            }
          </g>
        }
      </svg>
    );
  }
}
