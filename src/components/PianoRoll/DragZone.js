// @flow
import React, { Component, type Node } from 'react';
import { invariant } from '../../utils';

// if the start of a drag is within this number of pixels from the edge of a
// drag zone, lock the center of the zoom to the dragged edge
const LOCK_DISTANCE = 40;

export default class DragZone extends Component<{
  scale: number,
  scroll: number,
  min: number,
  max: number,
  size: number,
  steps: number,
  range: number,
  direction: 'x' | 'y',
  onChange: (value: number, offset: number) => void,
  children?: Node,
}> {
  initialScale: ?number
  initialScroll: ?number
  dragStartPosition: ?number
  zoomCenter: ?number
  el: ?Element

  static defaultProps = {
    range: 300,
  };

  onMouseDown = (e: MouseEvent) => {
    e.preventDefault();

    const { el } = this;
    invariant(el);

    window.addEventListener('mousemove', this.onDrag);
    window.addEventListener('mouseup', this.onDragEnd);

    const { direction, scale, scroll, size } = this.props;
    const { left, top, right, bottom } = el.getBoundingClientRect();
    const { clientX, clientY } = e;

    this.initialScale = scale;
    this.initialScroll = scroll;

    if (direction === 'x') {
      this.dragStartPosition = clientX;
      if (clientY < top + LOCK_DISTANCE) {
        this.zoomCenter = 1;
      } else if (clientY > bottom - LOCK_DISTANCE) {
        this.zoomCenter = 0;
      } else {
        this.zoomCenter = 1 - (clientY - top) / size
      }
    } else {
      this.dragStartPosition = clientY;
      if (clientX < left + LOCK_DISTANCE) {
        this.zoomCenter = 0;
      } else if (clientX > right - LOCK_DISTANCE) {
        this.zoomCenter = 1;
      } else {
        this.zoomCenter = (clientX - left) / size
      }
    }
  };

  onDrag = (e: MouseEvent) => {
    const { range, direction, max, min, onChange } = this.props;
    const { dragStartPosition, initialScale, initialScroll, zoomCenter } = this;
    if (
      dragStartPosition == null ||
      initialScale == null ||
      initialScroll == null ||
      zoomCenter == null
    ) {
      return this.onDragEnd();
    }

    const windowScroll = direction === 'x' ? window.scrollX : window.scrollY;
    const extent = direction === 'x' ? window.innerWidth : window.innerHeight;
    const position = direction === 'x' ? e.clientX : e.clientY;
    const upRange = Math.min(range, dragStartPosition - windowScroll);
    const downRange = Math.min(range, extent + windowScroll - dragStartPosition);
    const delta = dragStartPosition - position;
    const scaleRange = max - min;

    const normalInitialScale = (initialScale - min) / scaleRange;
    const normalScale = (
      delta < 0
      ? Math.max(0, normalInitialScale * (downRange + delta) / downRange)
      : Math.min(1, normalInitialScale + (1 - normalInitialScale) * delta / upRange)
    );
    const scale = min + Math.floor(normalScale * scaleRange);
    const scroll = Math.max(0, Math.min(max - scale, initialScroll + (initialScale - scale) * zoomCenter));

    onChange(scale, scroll);
  }

  onDragEnd = () => {
    window.removeEventListener('mousemove', this.onDrag);
    window.removeEventListener('mouseup', this.onDragEnd);
    this.initialScale = null;
    this.initialScroll = null;
    this.dragStartPosition = null;
    this.zoomCenter = null;
  }

  render() {
    return (
      <div
        ref={el => this.el = el}
        onMouseDown={this.onMouseDown}
      >
        {this.props.children}
      </div>
    );
  }
}
