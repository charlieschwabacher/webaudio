// @flow
import React, { Component } from 'react';
import withDimensions from '../withDimensions';
import live from '../live';
import { Meter } from '../../audio';

export default withDimensions(live(
  class LevelMeter extends Component<{
    meter: Meter,
    width: number,
    height: number,
    color: string,
  }> {
    render() {
      const { width, height, meter } = this.props;
      const h = meter.level;
      const color = meter.isClipping() ? '#ff4e49' : '#18f3ba';

      return (
        <svg
          width={width}
          height={height}
          viewBox="0 0 1 1"
          preserveAspectRatio="none"
        >
          <rect
            fill={color}
            x={0}
            y={1 - h}
            width={1}
            height={h}
          />
        </svg>
      );
    }
  }
));
