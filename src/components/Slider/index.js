// @flow
// a mouse interactive slider ui element - expects to receive two props, value,
// the current position of the slider as a number between 0 and 1, and onChange,
// a callback which will get passed the new values on changes.
//
// Also can receive an optional prop 'disabled' which if true will prevent
// interaction.

import React, { Component } from 'react';
import { invariant } from '../../utils';
import './Slider.css';

export default class Slider extends Component<{
  value: number,
  onChange: (value: number) => void,
  disabled?: ?bool,
  precision: number,
}, {
  active: boolean,
  width: number,
  height: number,
}> {
  dragStartPosition: ?{ x: number, y: number }
  initialValue: ?number
  container: ?HTMLDivElement

  static defaultProps = {
    precision: 4,
  }

  state = {
    active: false,
    width: 0,
    height: 0,
  };

  onMouseDown = (e: MouseEvent) => {
    e.stopPropagation();
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = { x: e.clientX, y: e.clientY };
    this.initialValue = this.props.value;
    this.setState({ active: true });
  };

  onMouseMove = (e: MouseEvent) => {
    const { dragStartPosition, initialValue } = this;
    const { disabled, onChange, precision } = this.props;
    const { height } = this.state;

    if (disabled) return;
    if (dragStartPosition == null) return;

    const y = dragStartPosition.y - e.clientY;
    const value = Math.max(0, Math.min(1, initialValue + y / height));
    const rounded = Math.round(value * Math.pow(10, precision)) /
      Math.pow(10, precision);

    onChange(rounded);
  };

  onMouseUp = (e: MouseEvent) => {
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = null;
    this.initialValue = null;
    this.setState({ active: false });
  };

  onMouseDownTrack = (e: MouseEvent) => {
    e.stopPropagation();

    if (this.container == null) return;

    const { top, height } = this.container.getBoundingClientRect();
    const { onChange } = this.props;

    const value = Math.max(0, Math.min(1, 1 - (e.clientY - top) / height));
    onChange(value);

    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = { x: e.clientX, y: e.clientY };
    this.initialValue = value;
    this.setState({ active: true });
  }

  updateDimensions = () => {
    invariant(this.container);
    this.setState({
      height: this.container.clientHeight,
    });
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  render() {
    const { value, disabled } = this.props;
    const { active } = this.state;

    let className = 'slider'
    if (active) className += ' slider--active';
    if (disabled) className += ' slider--disabled';

    return (
      <div
        className={className}
        ref={el => this.container = el}
      >
        <div
          className="slider__track"
          onMouseDown={this.onMouseDownTrack}
        >
          <div
            className="slider__handle"
            onMouseDown={this.onMouseDown}
            style={{ top: `${100 * (1 - value)}%` }}
          />
        </div>
      </div>
    );
  }
}
