// @flow
import React, { Component } from 'react';
import type { FileLibrary } from '../../audio';
import type { Section, File } from '../../language';

export default class Files extends Component<{
  ast: Section<File>,
  library: FileLibrary,
}> {
  render() {
    const { ast } = this.props;
    return (
      <div>
        Files...
        {ast.items.map(({ url }) => <p key={url}>{url}</p>)}
      </div>
    );
  }
}
