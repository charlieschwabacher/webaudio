// @flow
import React, { Component } from 'react';
import withDimensions from '../withDimensions';
import live from '../live';

export default withDimensions(live(
  class FFTMeter extends Component<{
    analyser: AnalyserNode,
    width: number,
    height: number,
    color: string,
  }> {
    render() {
      const { width, height, analyser, color } = this.props;
      const binCount = analyser.frequencyBinCount;
      const yScale = 1 / 255;
      const xScale = 1 / (binCount - 1);

      const data = new Uint8Array(binCount);
      analyser.getByteFrequencyData(data);

      let max = 0;
      let d = `M 0 1 L 0 ${1 - data[0] * yScale}`;
      for (let i = 1; i < data.length; i++) {
        const xv = i;
        const yv = data[i]
        if (yv > max) max = yv;
        d += ` L ${xv * xScale} ${1 - yv * yScale}`;
      }
      d += ` L 1 1 L 0 1`;

      return (
        <svg
          width={width}
          height={height}
          viewBox="0 0 1 1"
          preserveAspectRatio="none"
        >
          {
            max > height / 10 &&
            <path
              d={d}
              fill={color}
            />
          }
        </svg>
      );
    }
  }
));
