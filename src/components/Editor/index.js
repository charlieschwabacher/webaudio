// @flow
import React, { Component } from 'react';
import CodeMirror from 'codemirror';
import 'codemirror/lib/codemirror.css';
import './Editor.css';

export default class Editor extends Component<{
  replaceTextRange: (from: number, to: number, text: string) => void,
  initEditor: (editor: CodeMirror) => void,
  destroyEditor: (editor: CodeMirror) => void,
  parseError: ?string,
}> {
  container: ?HTMLDivElement
  editor: CodeMirror

  componentDidMount() {
    this.editor = new CodeMirror(this.container, {
      tabSize: 2,
      viewportMargin: Infinity,
      lineNumbers: true,
    });

    this.editor.on('beforeChange', this.onBeforeChange);

    this.props.initEditor(this.editor);
  }

  componentWillUnmount() {
    this.props.destroyEditor(this.editor);
  }

  onBeforeChange = (
    cmInstance: CodeMirror,
    changeObj: { from: Object, to: Object, text: Array<string>, origin: string },
  ): void => {
    if (changeObj.origin === 'setValue' || changeObj.origin === 'replaceRange') {
      return;
    }

    const from = cmInstance.indexFromPos(changeObj.from);
    const to = cmInstance.indexFromPos(changeObj.to);
    const text = changeObj.text.join('\n');

    this.props.replaceTextRange(from, to, text);
  };

  render() {
    const { parseError } = this.props;
    return (
      <div
        className='editor'
        onKeyDown={e => e.stopPropagation()}
        onKeyUp={e => e.stopPropagation()}
      >
        <div
          className='editor__codemirror-container'
          ref={el => this.container = el}
        />
        {
          parseError &&
          <div
            style={{
              padding: '10px 20px',
              color: '#fff',
              background: '#c00',
            }}
          >
            {parseError}
          </div>
        }
      </div>
    );
  }
}
