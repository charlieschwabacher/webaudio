// @flow
import React, { Component } from 'react';
import vis from 'vis';
import withDimensions from '../withDimensions';
import { SOURCE_TYPES, SEQUENCE_TYPES } from '../../audio';
import type { Root as RootAST } from '../../language';
import './AudioGraph.css';

type VisData = {
  nodes: Array<{
    id: string,
    label: string,
    group: string,
    x?: number,
  }>,
  edges: Array<{
    from: string,
    to: string,
  }>
}

const options = {
  autoResize: true,
  nodes: {
    font: '18px monospace #666',
    shape: 'dot',
    color: '#777',
    size: 20,
    borderWidth: 0,
  },
  groups: {
    seq: { color: '#2a3590' },
    source: { color: '#ff4e49' },
    mod: { color: '#ffa676' },
    default: { color: '#18f3ba' },
    master: { color: '#ccc' },
  },
  edges: {
    smooth: false,
    width: 3,
    color: {
      color: '#bbb',
      hover: '#aaa',
    },
  },
  layout: {
    randomSeed: 0,
    improvedLayout: true,
  },
  physics: {
    enabled: true,
  },
  interaction: {
    dragNodes: true,
    dragView: true,
    zoomView: false,
    selectable: true,
    hover: true,
  },
};


export default withDimensions(
  class AudioGraph extends Component<{
    ast: RootAST,
    width: number,
    height: number,
  }> {
    container: ?HTMLDivElement

    componentDidMount() {
      this.updateGraph();
    }

    componentDidUpdate() {
      this.updateGraph();
    }

    shouldComponentUpdate(nextProps) {
      return (
        nextProps.ast !== this.props.ast ||
        nextProps.width !== this.props.width ||
        nextProps.height !== this.props.height
      );
    }

    updateGraph() {
      const { ast: { modules }, width, height } = this.props;
      const data: VisData = {
        nodes: [
          { id: 'MASTER', label: 'MASTER', group: 'master'  },
        ],
        edges: [],
      };

      // tn = track name, nn = node name
      modules.forEach(module => {
        const { name: mid, nodes, connections } = module;

        data.nodes = data.nodes.concat(
          nodes.items.map(({ name: nid, nodeType}) =>
            ({
              id: `${mid}:${nid}`,
              label: nid,
              group: (
                SEQUENCE_TYPES.has(nodeType)
                ? 'seq'
                : SOURCE_TYPES.has(nodeType)
                ? 'source'
                : 'mod'
              )
            })
          ),
          { id: `${mid}:OUT`, label: `${mid} OUT`, group: 'default' },
        );

        if (connections.items.some(c => c.from === 'IN')) {
          data.nodes.push(
            { id: `${mid}:IN`, label: `${mid} IN`, group: 'track' },
          );
        }

        data.edges = data.edges.concat(
          connections.items.map(({ from, to }) =>
            ({ from: `${mid}:${from}`, to: `${mid}:${to}`})
          ),
          { from: `${mid}:OUT`, to: 'MASTER' },
        );
      })

      new vis.Network(this.container, data, {
        ...options,
        width: `${width}px`,
        height: `${height}px`,
      });
    }

    render() {
      return (
        <div
          ref={el => this.container = el}
          style={{ width: '100%', height: '100%' }}
        />
      );
    }
  }
);
