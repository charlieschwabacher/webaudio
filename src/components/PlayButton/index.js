// @flow
import React, { Component } from 'react';

export default class PlayButton extends Component<{
  playing: boolean,
  color: string,
  onClick?: ?() => void,
  size: number,
}> {
  render() {
    const { size, playing, color, onClick } = this.props;

    return (
      <svg
        onClick={onClick}
        width={size}
        height={size}
        viewBox="0 0 1 1"
        style={{ display: 'block' }}
      >
        {
          playing
            ? <rect
              x={0}
              y={0.1}
              width={0.8}
              height={0.8}
              fill={color}
            />
            : <path
              d="m 0 0 L 1 0.5 L 0 1 L 0 0"
              fill={color}
            />
        }
      </svg>
    );
  }
}
