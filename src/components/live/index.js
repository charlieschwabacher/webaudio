// @flow
import React, { Component } from 'react';

export default function live<P>(
  ComponentClass: Class<Component<P>>,
): Class<Component<P>> {
  return class Live extends Component<P> {
    frameRequest: AnimationFrameID

    componentDidMount() {
      this.frameRequest = requestAnimationFrame(this.doUpdate);
    }

    componentWillUnmount() {
      cancelAnimationFrame(this.frameRequest);
    }

    doUpdate = () => {
      this.forceUpdate();
      this.frameRequest = requestAnimationFrame(this.doUpdate);
    }

    render() {
      return (
        <ComponentClass
          {...this.props}
        />
      );
    }
  }
}
