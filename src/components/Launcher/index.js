// @flow
import React, { Component } from 'react';
import Track from './Track';
import { group, invariant } from '../../utils';
import type { Project } from '../../audio';
import type {
  Module as ModuleAST,
  Sequence as SequenceAST,
} from '../../language';
import type { Session } from '../../store';
import './Launcher.css';

export default class Launcher extends Component<{
  project: Project,
  modules: Array<ModuleAST>,
  sequences: Array<SequenceAST>,
  session: Session,
  selectedSequence: string,
  setModuleLevel: (module: string, level: number) => void,
  selectSequence: (id: string) => void,
  createSequence: (id: string, target: string) => void,
  stageSequence: (module: string, sequence: string) => void,
  stopModule: (module: string) => void,
  toggleSolo: (module: ?string) => void,
  toggleMute: (module: string, mute: boolean) => void,
}> {
  render() {
    const {
      project,
      modules,
      sequences,
      session,
      selectedSequence,
      setModuleLevel,
      selectSequence,
      createSequence,
      stageSequence,
      stopModule,
      toggleSolo,
      toggleMute,
    } = this.props;

    const sequencesByTarget = group(sequences, s => s.target.value);

    return (
      <div className="launcher">
        {
          modules.map(({ name }) => {
            const module = project.modules.get(name);
            invariant(module);
            return (
              <Track
                key={name}
                project={project}
                module={module}
                sequences={sequencesByTarget[name]}
                selectedSequence={selectedSequence}
                solo={session.solo}
                moduleSession={session.modules[name]}
                setModuleLevel={setModuleLevel}
                selectSequence={selectSequence}
                createSequence={createSequence}
                stageSequence={stageSequence}
                stopModule={stopModule}
                toggleSolo={toggleSolo}
                toggleMute={toggleMute}
              />
            );
          })
        }
      </div>
    );
  }
}
