// @flow
import React, { Component } from 'react';
import live from '../live';
import type { Project } from '../../audio';
import type { Sequence as SequenceAST } from '../../language';

export default live(
  class SequenceLoopIndicator extends Component<{
    playStart: number,
    project: Project,
    sequence: SequenceAST,
  }> {
    render() {
      const { playStart, project, sequence } = this.props;

      const tempo = project.ast.project.tempo.value;
      const length = sequence.length.value;
      const elapsed = project.context.currentTime - project.globalStartTime;
      const position = elapsed * tempo / 60;
      const loopPosition = ((position - playStart) % length) / length;

      return (
        <div
          className='launcher__sequence-loop-indicator'
          style={{ width: `${100 * loopPosition}%`}}
        />
      );
    }
  }
);
