// @flow
import React, { Component } from 'react';
import Sequence from './Sequence';
import Slider from '../Slider';
import LevelMeter from '../LevelMeter';
import shallowCompare from 'react-addons-shallow-compare';
import Module from '../../audio/build/Module';
import { range, id } from '../../utils';
import type {
  Sequence as SequenceAST,
} from '../../language';
import type { Project } from '../../audio';

export default class Track extends Component<{
  project: Project,
  module: Module,
  sequences: ?Array<SequenceAST>,
  selectedSequence: string,
  solo: ?string,
  moduleSession: {
    mute: boolean,
    playing: {
      sequence: string, // sequence id
      start: number, // beat the loop started
    },
    next: ?string, // sequence id
  },
  setModuleLevel: (module: string, level: number) => void,
  selectSequence: (id: string) => void,
  createSequence: (id: string, target: string) => void,
  stageSequence: (module: string, sequence: string) => void,
  stopModule: (module: string) => void,
  toggleSolo: (module: ?string) => void,
  toggleMute: (module: string, mute: boolean) => void,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      project,
      module,
      sequences,
      selectedSequence,
      solo,
      moduleSession,
      setModuleLevel,
      selectSequence,
      createSequence,
      stageSequence,
      stopModule,
      toggleSolo,
      toggleMute,
    } = this.props;
    const {
      ast,
      meter,
    } = module;

    const isSolo = solo === ast.name;
    const isMute = moduleSession.mute;

    return (
      <div className="launcher__track">
        <div className="launcher__sequences">
          {
            range(10).map(i => {
              const sequence = sequences && sequences[i];
              return (
                <Sequence
                  key={i}
                  project={project}
                  sequence={sequence}
                  selected={sequence != null && sequence.name === selectedSequence}
                  next={sequence != null && sequence.name === moduleSession.next}
                  playStart={
                    project.playing &&
                    moduleSession.playing &&
                    sequence &&
                    moduleSession.playing.sequence === sequence.name
                    ? moduleSession.playing.start
                    : null
                  }
                  onClick={
                    sequence == null
                    ? () => stopModule(ast.name)
                    : !moduleSession.playing || sequence.name !== moduleSession.playing.sequence
                    ? () => stageSequence(ast.name, sequence.name)
                    : null
                  }
                  onDoubleClick={
                    sequence == null
                    ? () => {
                      const sequenceId = `seq${id()}`;
                      createSequence(sequenceId, ast.name);
                      selectSequence(sequenceId);
                    }
                    : () => selectSequence(sequence.name)
                  }
                />
              );
            })
          }
        </div>
        <div className="launcher__track-audio-controls">
          <div className='launcher__track-controls'>
            <div
              onClick={() => toggleSolo(isSolo ? null : ast.name)}
              className={
                'launcher__track-control' +
                (isSolo ? ' launcher__track-control--solo-active' : '')
              }
            >
              S
            </div>
            <div
              onClick={() => toggleMute(ast.name, !isMute)}
              className={
                'launcher__track-control' +
                (isMute ? ' launcher__track-control--mute-active' : '') +
                (solo ? ' launcher__track-control--disabled' : '')
              }
            >
              Μ
            </div>
          </div>
          <div className="launcher__track-slider">
            <div>
              <Slider
                value={ast.level.value}
                onChange={level => setModuleLevel(ast.name, level)}
              />
            </div>
            <div className='launcher__track-meter'>
              <LevelMeter
                meter={meter}
                color={'#20d279'}
              />
            </div>
          </div>
          <div className="launcher__track-label">
            {ast.name}
          </div>
        </div>
      </div>
    );
  }
}
