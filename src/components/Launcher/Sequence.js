// @flow
import React, { Component } from 'react';
import SequenceLoopIndicator from './SequenceLoopIndicator';
import PlayButton from '../PlayButton';
import shallowCompare from 'react-addons-shallow-compare';
import type {
  Sequence as SequenceAST,
} from '../../language';
import type { Project } from '../../audio';


export default class Sequence extends Component<{
  project: Project,
  sequence: ?SequenceAST,
  selected: boolean,
  next: boolean,
  playStart: ?number,
  onClick: ?() => void,
  onDoubleClick: ?() => void,
}> {
  shouldComponentUpdate(nextProps: Object): boolean {
    return shallowCompare(this, nextProps);
  }

  render() {
    const {
      project,
      sequence,
      selected,
      next,
      playStart,
      onClick,
      onDoubleClick,
    } = this.props;

    let className = 'launcher__sequence';
    if (selected) className += ' launcher__sequence--selected';
    if (next) className += ' launcher__sequence--next';
    if (playStart != null) className += ' launcher__sequence--playing'
    if (sequence == null) className += ' launcher__sequence--placeholder';

    return (
      <div
        className={className}
        onDoubleClick={onDoubleClick}
      >
        {
          playStart != null &&
          sequence != null &&
          <SequenceLoopIndicator
            playStart={playStart}
            sequence={sequence}
            project={project}
          />
        }
        <div className='launcher__sequence-controls'>
          <PlayButton
            size={10}
            playing={sequence == null}
            color='#fff'
            onClick={onClick}
          />
        </div>
      </div>
    );
  }
}
