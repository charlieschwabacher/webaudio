// @flow
import React, { Component } from 'react';
import withDimensions from '../withDimensions';
import './Keyboard.css';

const KEY_PATTERN = [1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1];

export default withDimensions(
  class Keyboard extends Component<{
    width: 0,
    height: 0,
    from: number,
    to: number,
    keys: Set<number>,
  }> {
    render() {
      const { width, height, from, to, keys } = this.props;
      const length = to - from;
      const keyWidth = width / length;

      return (
        <svg
          width={width}
          height={height}
          className='keyboard'
        >
          {
            (new Array(length)).fill().map((_, i) => {
              const key = from + i;
              return (
                <rect
                  key={i}
                  x={i * keyWidth}
                  y={0}
                  width={keyWidth}
                  height={height}
                  className={
                    (keys.has(key) ? 'keyboard__key--active ' : '') +
                    (KEY_PATTERN[key % 12] === 0 ? 'keyboard__key--black ' : '') +
                    'keyboard__key'
                  }
                />
              );
            })
          }
        </svg>
      );
    }
  }
)
