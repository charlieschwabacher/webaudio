// @flow
import { invariant } from '../../utils';
import React, { Component, type ComponentType } from 'react';
import './Dimensions.css';

type Dimensions = { width: number, height: number };

export default function withDimensions<T: Object>(
  ComponentClass: ComponentType<T>,
): ComponentType<$Diff<T, Dimensions>> {
  return class WithDimensions extends Component<
    $Diff<T, Dimensions>,
    Dimensions,
  > {
    container: ?Element

    state = {
      height: 0,
      width: 0,
    };

    updateDimensions = () => {
      const { container } = this;
      invariant(container);

      this.setState({
        width: container.clientWidth,
        height: container.clientHeight,
      });
    }

    componentDidMount() {
      this.updateDimensions();
      window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.updateDimensions);
    }

    render() {
      return (
        <div
          ref={el => this.container = el}
          className='dimensions'
        >
          <ComponentClass
            {...this.props}
            {...this.state}
          />
        </div>
      );
    }
  }
}
