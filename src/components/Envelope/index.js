// @flow
// UI for an envelope control with attack, decay, sustain, and release.
// expects to receive an envelope object { a, d, s, r} as value.

import React, { Component } from 'react';
import { invariant } from '../../utils';
import './Envelope.css';

// ensure a number is between 0 and 1
function constrain(num) {
  return Math.max(0, Math.min(1, num));
}

type ADSR = {
  a: number,
  d: number,
  s: number,
  r: number,
}

type Position = {
  x: number,
  y: number,
}

type DragTarget = 'attack' | 'decay' | 'sustain' | 'release';

export default class Envelope extends Component<{
  value: ADSR,
  dotRadius: number,
  margin: number,
  onChange: ADSR => void,
  height: number,
  width: number,
}, {
  dragTarget: ?DragTarget,
}> {
  initialValue: ?ADSR
  dragStartPosition: ?Position

  static defaultProps = {
    dotRadius: 5,
    margin: 4,
  };

  state = {
    dragTarget: undefined,
  };

  onMouseDown = (e: MouseEvent) => {
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = { x: e.clientX, y: e.clientY };
  };

  onMouseMove = (e: MouseEvent) => {
    const { dragStartPosition, initialValue } = this;
    const { margin, dotRadius, onChange, value, width, height } = this.props;
    const { dragTarget } = this.state;

    invariant(dragStartPosition);
    invariant(initialValue);

    const x = e.clientX - dragStartPosition.x;
    const y = dragStartPosition.y - e.clientY;

    const m = margin + dotRadius;
    const w = (width - 2 * m) / 3;
    const h = height - 2 * m;

    const changes = {};
    if (dragTarget === 'attack') {
      changes.a = constrain(initialValue.a + x / w);
    } else if (dragTarget === 'decay') {
      changes.d = constrain(initialValue.d + x / w);
      changes.s = constrain(initialValue.s + y / h);
    } else if (dragTarget === 'release') {
      changes.r = constrain(initialValue.r + x / w);
    }

    onChange({
      ...value,
      ...changes,
    });
  };

  onMouseUp = (e: MouseEvent) => {
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mouseup', this.onMouseUp);
    this.dragStartPosition = undefined;
    this.initialValue = undefined;
    this.setState({ dragTarget: null });
  };

  onMouseDownAttack = (e: MouseEvent) => {
    const { value } = this.props;
    this.initialValue = value;
    this.setState({ dragTarget: 'attack' });
    this.onMouseDown(e);
  };

  onMouseDownDecay = (e: MouseEvent) => {
    const { value } = this.props;
    this.initialValue = value;
    this.setState({ dragTarget: 'decay' });
    this.onMouseDown(e);
  };

  onMouseDownRelease = (e: MouseEvent) => {
    const { value } = this.props;
    this.initialValue = value;
    this.setState({ dragTarget: 'release' });
    this.onMouseDown(e);
  };

  render() {
    const {
      width,
      height,
      value: { a, d, s, r },
      margin,
      dotRadius,
    } = this.props;
    const { dragTarget } = this.state;
    const m = margin + dotRadius;
    const w = width - 2 * m;
    const h = height - 2 * m;

    if (w <= 0 || h <= 0) {
      return (
        <svg />
      );
    }

    const p1 = {
      x: m,
      y: m + h,
    };

    const p2 = {
      x: m + w / 3 * a,
      y: m,
    };

    const p3 = {
      x: m + p2.x + w / 3 * d,
      y: m + h * (1 - s),
    };

    const p4 = {
      x: m + w * 2 / 3,
      y: m + h * (1 - s),
    };

    const p5 = {
      x: m + w * (2 + r) / 3,
      y: m + h,
    };

    const p = `M ${[p1, p2, p3, p4, p5].map(p => `${p.x} ${p.y}`).join(' L ')}`;

    return (
      <svg width={width} height={height}>

        {/* base */}
        <line
          key="b"
          x1={0}
          y1={m+h}
          x2={2*m+w}
          y2={m+h}
          style={{ fill: 'none', stroke: '#eee' }}
        />

        {/* path */}
        <path
          key="p"
          d={p}
          style={{ fill: 'none', stroke: '#ddd' }}
        />

        {/* attack */}
        <circle
          key="a"
          className={dragTarget === 'attack' ? 'active' : ''}
          cx={p2.x}
          cy={p2.y}
          r={dotRadius}
          onMouseDown={this.onMouseDownAttack}
          style={{ fill: '#ccc' }}
        />

        {/* decay / sustain */}
        <circle
          key="d"
          className={dragTarget === 'decay' ? 'active' : ''}
          cx={p3.x}
          cy={p3.y}
          r={dotRadius}
          onMouseDown={this.onMouseDownDecay}
          style={{ fill: '#ccc' }}
        />

        {/* release */}
        <circle
          key="r"
          className={dragTarget === 'release' ? 'active' : ''}
          cx={p5.x}
          cy={p5.y}
          r={dotRadius}
          onMouseDown={this.onMouseDownRelease}
          style={{ fill: '#ccc' }}
        />
      </svg>
    );
  }
}
