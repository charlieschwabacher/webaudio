export default {
test:
`project
  level 0.6
  tempo 60

sequence BDSEQ
  target BD
  start 0
  length 4
  notes
    50 0 1

module BD
  level 0.6
  voices 3
  nodes
    OSC OscillatorNode type "sawtooth"
    AMP GainNode gain 0
    VENV EnvelopeNode steps [1 0.1 0.7 0.7]
    SEQ SequenceNode
  connections
    OSC->AMP
    AMP->OUT
    VENV->AMP.gain
    SEQ->OSC.detune

`,

test2:
`project
  level 0.51
  tempo 120

sequence KEYSSEQ
  target KEYS
  start 0
  length 8
  notes
    40 0 2
    43 0 2
    47 0 2
    3A 3 2
    42 3 2
    47 3 2
    3A 6 1
    42 6 1
    45 6 1

sequence KEYSSEQ2
  target KEYS
  start 0
  length 4
  notes
    40 0 2
    43 0 2
    47 0 2

sequence BDSEQ
  target BD
  start 0
  length 4
  notes
    40 0 0.25
    50 1 0.25
    40 2 0.25
    43 2.75 0.25
    50 3 0.25
    40 3.5 0.25

module KEYS
  level 0.1
  voices 3
  nodes
    OSC1 OscillatorNode type "square"
    OSC2 OscillatorNode type "square" detune 8
    LPF BiquadFilterNode frequency 150
    HPF BiquadFilterNode type "highpass" frequency 400
    AMP GainNode gain 0
    SEQ SequenceNode
    VENV EnvelopeNode steps [0 0.15 0.5 0.5 0.1 0.5]
    FENV EnvelopeNode steps [900 0.9 0 1]
  connections
    OSC1->AMP
    OSC2->AMP
    AMP->LPF
    LPF->HPF
    HPF->OUT
    SEQ->OSC1.detune
    SEQ->OSC2.detune
    VENV->AMP.gain
    FENV->LPF.frequency

module BD
  level 0.8326986984214899
  voices 1
  nodes
    OSC OscillatorNode
    AMP GainNode gain 0
    VENV EnvelopeNode steps [1 0.25]
    PENV EnvelopeNode steps [400 0.025 0 0]
    SEQ SequenceNode
  connections
    SEQ->OSC.detune
    OSC->AMP
    AMP->OUT
    VENV->AMP.gain
    PENV->OSC.frequency
`,

bassy:
`project
  level 0.6
  tempo 120

sequence BASSSEQ
  target BASS
  start 0
  length 16
  notes
    41 0 0.25
    41 1.5 1
    41 3 0.25
    41 4.5 0.25
    41 6 0.25
    41 7.5 0.25
    41 8 0.25
    41 9.5 1
    41 11 0.25
    41 12.5 0.25
    41 14 0.25
    44 14.5 0.5
    51 15.5 0.25

sequence BDSEQ
  target BD
  start 0
  length 4
  notes
    31 0 0.25
    31 1 0.25
    31 2 0.25
    31 2.75 0.25
    31 3.5 0.25

sequence SDSEQ
  target SD
  start 0
  length 2
  notes
    42 1 0.5

sequence HHSEQ
  target HH
  start 0
  length 4
  notes
    30 0.5 0.25
    30 1.5 0.25
    30 2.5 0.25
    30 3.5 0.25

module BASS
  level 0.4435028248587571
  voices 1
  nodes
    OSC1 OscillatorNode detune 0
    OSC2 OscillatorNode detune 0
    AMP GainNode gain 0
    VENV EnvelopeNode steps [0.4 1]
    PENV1 EnvelopeNode steps [-20 0.9 0 1]
    PENV2 EnvelopeNode steps [30 0.98 0 1]
    SEQ SequenceNode
  connections
    OSC1->AMP
    OSC2->AMP
    AMP->OUT
    VENV->AMP.gain
    PENV1->OSC1.detune
    PENV2->OSC2.detune
    SEQ->OSC1.detune
    SEQ->OSC2.detune

module BD
  level 0.8418079096045198
  voices 1
  nodes
    OSC OscillatorNode
    AMP GainNode gain 0
    VENV EnvelopeNode steps [1 0.2 0 1]
    PENV EnvelopeNode steps [1200 0.05 0 1]
    SEQ SequenceNode
  connections
    OSC->AMP
    AMP->OUT
    VENV->AMP.gain
    PENV->OSC.detune
    SEQ->OSC.detune

module SD
  level 0.9209039548022598
  voices 1
  nodes
    NSE WhiteNoiseNode
    FLT BiquadFilterNode type "highpass" frequency 700
    AMP GainNode gain 0
    ENV EnvelopeNode steps [0.4 0.1 0 0]
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain

module HH
  level 0.940677966101695
  voices 1
  nodes
    NSE WhiteNoiseNode
    FLT BiquadFilterNode type "highpass" frequency 9000
    AMP GainNode gain 0
    ENV EnvelopeNode steps [0.3 0.03 0 0]
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain
`,

now:
`project
  level 0.6
  tempo 100

sequence CHORDSEQ
  target CHORD
  start 0
  length 8
  notes
    50 0 0.5
    57 0 0.5
    53 0 0.5
    50 0.5 0.25
    52 0.75 0.25
    53 1 0.25
    55 1.25 0.25
    57 1.5 0.25
    5A 1.75 0.25
    57 2 0.5
    52 2 0.5
    4A 2 0.5
    50 2.5 0.25
    52 2.75 0.25
    53 3 0.25
    55 3.25 0.25
    57 3.5 0.25
    60 3.75 0.25
    50 4 0.5
    48 4 0.5
    53 4 0.5
    50 4.5 0.25
    52 4.75 0.25
    53 5 0.25
    55 5.25 0.25
    57 5.5 0.25
    5A 5.75 0.25
    52 6 0.5
    55 6 0.5
    4A 6 0.5
    60 6.5 0.25
    5A 6.75 0.25
    57 7 0.25
    55 7.25 0.25
    53 7.5 0.25
    52 7.75 0.25

module CHORD
  level 1
  voices 6
  nodes
    OSC OscillatorNode type "square"
    AMP GainNode gain 0
    DLY DelayNode delayTime 0.125
    LPF BiquadFilterNode frequency 1000
    HPF BiquadFilterNode type "highpass" frequency 400
    FBK GainNode gain 0.57
    SEQ SequenceNode
    AENV EnvelopeNode steps [0.14 1.5]
    FENV EnvelopeNode steps [2900 0.2 0 0]
  connections
    OSC->AMP
    AMP->LPF
    LPF->HPF
    HPF->OUT
    HPF->DLY
    DLY->FBK
    FBK->LPF
    SEQ->OSC.detune
    AENV->AMP.gain
    FENV->LPF.frequency
`,

echoey:
`project
  level 0.6
  tempo 120

sequence BASS
  target BASS
  length 16
  notes
    35 0 0.5
    38 1 0.5
    40 2 0.5
    45 3 0.5
    35 4 0.5
    38 5 0.5
    40 6 0.5
    45 7 0.5
    35 8 0.5
    35 8.5 0.5
    38 9 0.5
    38 9.5 0.5
    40 10 0.5
    40 10.5 0.5
    45 11 0.5
    45 11.5 0.5
    35 12 0.5
    35 12.5 0.5
    38 13 0.5
    38 13.5 0.5
    30 14 0.5
    30 14.5 0.5
    28 15 0.5
    28 15.5 0.5

sequence HAT
  target HAT
  length 1
  notes
    60 0.5 0.5

sequence SNARE
  target SNARE
  length 2
  notes
    60 1 1

sequence KEYS
  target KEYS
  length 16
  notes
    60 0 4
    67 0 4
    55 5 2
    60 5 2
    57 8 4
    63 8 4
    53 13 2
    5A 13 2

module BASS
  level 0.6
  nodes
    OSC1 OscillatorNode detune 0
    OSC2 OscillatorNode detune 1200
    AMP1 GainNode gain 0
    AMP2 GainNode gain 0.5
    ENV EnvelopeNode steps [1 0.1 0.4 0.21]
    SEQ SequenceNode
  connections
    OSC1->AMP1
    OSC2->AMP2
    AMP2->AMP1
    AMP1->OUT
    ENV->AMP1.gain
    SEQ->OSC1.detune
    SEQ->OSC2.detune

module HAT
  level 0.3
  nodes
    NSE WhiteNoiseNode
    AMP GainNode gain 0
    FLT BiquadFilterNode type "highpass" frequency 9000
    ENV EnvelopeNode steps [1 0.05 0 0]
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain

module SNARE
  level 0.7
  nodes
    NSE WhiteNoiseNode
    AMP GainNode gain 0
    FLT BiquadFilterNode type "highpass" frequency 1000
    ENV EnvelopeNode steps [1 0.05 0 0]
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain

module KEYS
  level 0.1
  voices 2
  nodes
    OSC OscillatorNode type "square"
    AMP GainNode gain 0
    LPF BiquadFilterNode frequency 2400
    HPF BiquadFilterNode type "highpass" frequency 700
    DLY DelayNode delayTime 0.125
    FBK GainNode gain 0.83
    ENV EnvelopeNode steps [1 0.1 0.1 0.5]
    LFO1 OscillatorNode frequency 0.7
    AMT1 GainNode gain 10
    LFO2 OscillatorNode frequency 0.3
    AMT2 GainNode gain 1000
    SEQ SequenceNode
  connections
    OSC->AMP
    AMP->LPF
    LPF->HPF
    HPF->OUT
    LPF->DLY
    DLY->FBK
    FBK->LPF
    ENV->AMP.gain
    LFO1->AMT1
    AMT1->OSC.detune
    LFO2->AMT2
    AMT2->LPF.frequency
    SEQ->OSC.detune
`,
glide:
`project
  level 0.6
  tempo 60

sequence BDSEQ
  target BD
  start 0
  length 4
  notes
    30 0 0.25
    40 1 0.25
    30 2 0.25

sequence ufiohojsh
  target BB
  start 0
  length 8
  notes
    50 0 0.25
    60 0.5 0.25
    60 1 0.25
    63 1.5 0.25
    63 2.25 0.25
    65 2.75 0.25
    65 3.25 0.25
    67 4 0.25
    5A 4.25 0.25
    60 4.75 0.25
    60 5.25 0.25
    60 6 0.25
    63 6.75 0.25
    5A 7.5 0.25

module BD
  level 0.7675159235668789
  voices 1
  nodes
    OSC OscillatorNode type "square"
    AMP GainNode gain 0
    VENV EnvelopeNode steps [1 2]
    SEQ SequenceNode glide 0.25
  connections
    OSC->AMP
    AMP->OUT
    VENV->AMP.gain
    SEQ->OSC.detune

module BB
  level 0.5509554140127388
  voices 1
  nodes
    OSC OscillatorNode type "square"
    AMP GainNode gain 0
    VENV EnvelopeNode steps [1 0]
    SEQ SequenceNode glide 0.125
  connections
    OSC->AMP
    AMP->OUT
    VENV->AMP.gain
    SEQ->OSC.detune
`,
sample:
`project
  level 0.51
  tempo 136
  files
    "/amen.wav"
    "/ir.wav"

sequence AMENSEQ
  target AMEN
  start 0
  length 8
  notes
    59 0 1.5
    59 1.5 1.5
    59 3 1
    59 4 4

sequence BASSSEQ
  target BASS
  start 0
  length 8
  notes
    39 0 2.5
    37 2.5 1.5
    34 4 3
    32 7 1

module AMEN
  level 0.5
  voices 1
  nodes
    SMP SamplerNode buffer "/amen.wav"
    RVB ConvolverNode buffer "/ir.wav"
    HPF BiquadFilterNode type "highpass" frequency 1000
    AMT GainNode gain 0.6
    AMP GainNode gain 0
    ENV EnvelopeNode steps [1 0]
    SEQ SequenceNode
  connections
    SMP->AMP
    AMP->OUT
    AMP->HPF
    HPF->RVB
    RVB->AMT
    AMT->OUT
    ENV->AMP.gain
    SEQ->SMP.detune

module BASS
  level 0.5
  voices 1
  nodes
    OSC OscillatorNode
    AMP GainNode gain 0
    ENV EnvelopeNode steps [1 0]
    SEQ SequenceNode
  connections
    OSC->AMP
    AMP->OUT
    SEQ->OSC.detune
    ENV->AMP.gain
`,

dubby:
`project
  tempo 120
  level 1

sequence HATSEQ
  target HAT
  length 1
  notes
    60 1 1

module HAT
  level 0.1
  nodes
    NSE WhiteNoiseNode
    ENV EnvelopeNode steps [1 0.05 0 0]
    AMP GainNode gain 0
    FLT BiquadFilterNode type "highpass" frequency 11000
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain

sequence SNARESEQ
  target SNARE
  length 4
  notes
    30 2 1

module SNARE
  level 0.3
  nodes
    NSE WhiteNoiseNode
    ENV EnvelopeNode steps [1 0.1 0 0]
    AMP GainNode gain 0
    FLT BiquadFilterNode type "highpass" frequency 1000
  connections
    NSE->AMP
    AMP->FLT
    FLT->OUT
    ENV->AMP.gain

sequence REECESEQ
  target REECE
  length 16
  notes
    30 0 4
    35 5 2
    37 8 4
    33 13 2

module REECE
  nodes
    SEQ SequenceNode
    ENV EnvelopeNode steps [1 0.5]
    LFO1 OscillatorNode frequency 0.7
    AMT1 GainNode gain 10
    LFO2 OscillatorNode frequency 0.3
    AMT2 GainNode gain 20
    OSC1 OscillatorNode type "sawtooth"
    OSC2 OscillatorNode type "sawtooth"
    AMP GainNode gain 0
    LPF BiquadFilterNode frequency 100 Q 11
  connections
    OSC1->AMP
    OSC2->AMP
    AMP->LPF
    LPF->OUT
    LFO1->AMT1
    LFO2->AMT2
    AMT1->OSC1.detune
    AMT2->LPF.frequency
    ENV->AMP.gain
    SEQ->OSC1.detune
    SEQ->OSC2.detune

sequence KEYSSEQ
  target KEYS
  length 16
  notes
    60 0 4
    67 0 4
    55 5 2
    60 5 2
    57 8 4
    63 8 4
    53 13 2
    5A 13 2

module KEYS
  level 0.7
  voices 2
  nodes
    SEQ SequenceNode
    ENV EnvelopeNode steps [1 0.4 0.2 0]
    OSC1 OscillatorNode type "square"
    AMP GainNode gain 0
    LPF BiquadFilterNode frequency 2400
    HPF BiquadFilterNode type "highpass" frequency 700
    DLY DelayNode delayTime 0.333
    FBK GainNode gain 0.7
    LFO1 OscillatorNode frequency 0.7
    AMT1 GainNode gain 10
    LFO2 OscillatorNode frequency 4
    AMT2 GainNode gain 0.0025
  connections
    OSC1->AMP
    AMP->LPF
    LPF->HPF
    HPF->OUT
    LPF->DLY
    DLY->FBK
    FBK->LPF
    FBK->HPF
    LFO1->AMT1
    LFO2->AMT2
    SEQ->OSC1.detune
    ENV->AMP.gain
    AMT1->OSC1.detune
    AMT2->DLY.delayTime
`,
//
// breakbeat: `BD
//   config
//     quantum 2
//   sequence
//     28........28....28....28..28....
//   voice
//     nodes
//       OSC OscillatorNode detune 700
//       AMP GainNode
//     modulation
//       ENV AMP gain 1 0.2 0 0
//       ENV OSC detune 1 0.05 0 0
//     connections
//       OSC->AMP->OUT
//
// BASS
//   config
//     quantum 0.5
//   sequence
//     40......37......
//   voice
//     nodes
//       OSC OscillatorNode
//       AMP GainNode gain 0.7
//     modulation
//       ENV AMP gain 1 0.7
//       LFO OSC detune 10 0.6
//     connections
//       OSC->AMP->OUT
//
// HAT
//   config
//     quantum 2
//   sequence
//     ..60
//   voice
//     nodes
//       NSE WhiteNoiseNode
//       AMP GainNode gain 0.23
//       FLT BiquadFilterNode type "highpass" frequency 9000
//     connections
//       NSE->AMP->FLT->OUT
//     modulation
//       ENV AMP gain 1 0.05 0 0
//
// SNARE
//   config
//     quantum 1
//   sequence
//     ..30
//   voice
//     nodes
//       NSE WhiteNoiseNode
//       AMP GainNode gain 0.5
//       FLT BiquadFilterNode type "highpass" frequency 1000
//     connections
//       NSE->AMP->FLT->OUT
//     modulation
//       ENV AMP gain 1 0.1 0 0
//
// KEYS
//   config
//     quantum 1
//     voices 2
//   sequence
//     6065--63--6065--6065--63--6065--
//     6568--67--6368--6368--6568--67--
//   voice
//     nodes
//       OSC1 OscillatorNode type "square"
//       AMP GainNode gain 0.1
//     connections
//       OSC1->AMP->OUT
//     modulation
//       ENV AMP gain 1 0.1 0.1 0.5 0 0.5
//       LFO OSC1 detune 10 0.7
//   track
//     nodes
//       LPF BiquadFilterNode frequency 3000
//       HPF BiquadFilterNode type "highpass" frequency 700
//       DLY DelayNode delayTime 0.25
//       FBK GainNode gain 0.7
//     modulation
//       LFO LPF frequency 1500 0.5
//     connections
//       IN->LPF->HPF->OUT
//       LPF->DLY
//       DLY->FBK->LPF
//       FBK->HPF
// `,
//
// oizo: `BD
//   config
//     quantum 2
//   sequence
//     28....28..282828..28....28......
//   voice
//     nodes
//       OSC OscillatorNode detune 1200
//       AMP GainNode
//     modulation
//       ENV AMP gain 1 0.21 0 0
//       ENV OSC detune 1 0.06 0 0
//     connections
//       OSC->AMP->OUT
//
// BASS
//   config
//     quantum 0.5
//   sequence
//     ..50......47......50......43....
//   voice
//     nodes
//       OSC OscillatorNode type "square"
//       AMP GainNode gain 0.04
//     modulation
//       ENV AMP gain 1 0
//       LFO OSC detune 40 0.12
//     connections
//       OSC->AMP->OUT
//
// FX
//   config
//     quantum 0.5
//   sequence
//     ..............................40
//   voice
//     nodes
//       OSC1 OscillatorNode type "square" detune 1200
//       OSC2 OscillatorNode type "square" detune 1220
//       AMP GainNode gain 0.03
//     modulation
//       ENV AMP gain 1 0
//       ENV OSC1 detune 1 0.5 0 0
//       ENV OSC2 detune 1 0.53 0 0
//     connections
//       OSC1->AMP->OUT
//       OSC2->AMP
//
// KEYS
//   config
//     quantum 0.5
//     voices 3
//   sequence
//     ..............40................
//     ..............43................
//     ..............47................
//   voice
//     nodes
//       OSC1 OscillatorNode type "square"
//       OSC2 OscillatorNode type "square" detune 8
//       LPF BiquadFilterNode frequency 1000
//       HPF BiquadFilterNode type "highpass" frequency 400
//       AMP GainNode gain 0.06
//     modulation
//       ENV AMP gain 0 0.1 1 0.5
//       ENV LPF frequency 1 0.5 0.1 1
//     connections
//       OSC1->AMP->LPF->HPF->OUT
//       OSC2->AMP
//
// HAT
//   config
//     quantum 2
//   sequence
//     60..
//   voice
//     nodes
//       NSE WhiteNoiseNode
//       AMP GainNode gain 0.23
//       FLT BiquadFilterNode type "highpass" frequency 9000
//     connections
//       NSE->AMP->FLT->OUT
//     modulation
//       ENV AMP gain 1 0.05 0 0
//
// SNARE
//   config
//     quantum 1
//   sequence
//     ..30
//   voice
//     nodes
//       NSE WhiteNoiseNode
//       AMP GainNode gain 0.5
//       FLT BiquadFilterNode type "highpass" frequency 1000
//     connections
//       NSE->AMP->FLT->OUT
//     modulation
//       ENV AMP gain 1 0.1 0 0
// `,
//
// tron: `KEYS
//   config
//     quantum 1
//     voices 3
//   sequence
//     40--....
//     43--....
//     47--....
//   voice
//     nodes
//       OSC1 OscillatorNode type "square"
//       OSC2 OscillatorNode type "square" detune 8
//       LPF BiquadFilterNode frequency 2000
//       HPF BiquadFilterNode type "highpass" frequency 700
//       AMP GainNode gain 0.1
//     modulation
//       ENV AMP gain 0 0.4 1 0.6
//       ENV LPF frequency 1 0.5 0.1 1
//       LFO LPF frequency 150 16
//     connections
//       OSC1->AMP->LPF->HPF->OUT
//       OSC2->AMP
// ARP
//   config
//     quantum 6
//   sequence
//     30..32..33..35..37..3a..
//   voice
//     nodes
//       OSC OscillatorNode type "sawtooth"
//       HPF BiquadFilterNode type "highpass" frequency 400 Q 0
//       LPF BiquadFilterNode type "lowpass" frequency 800 8 19
//       AMP GainNode gain 0.1
//     connections
//       OSC->AMP->HPF->LPF->OUT
//     modulation
//       ENV AMP gain 1 0.5 0 1
//       ENV LPF frequency 1 0.2 0 4
//       LFO LPF frequency 150 8
// BD
//   config
//     quantum 0.5
//   sequence
//     27......
//   voice
//     nodes
//       OSC OscillatorNode detune 500
//       AMP GainNode gain 0.6
//     connections
//       OSC->AMP->OUT
//     modulation
//       ENV AMP gain 1 0.5 0 1
//       ENV OSC detune 1 1
// SD
//   config
//     quantum 0.5
//   sequence
//     ..30
//   voice
//     nodes
//       NSE WhiteNoiseNode
//       FLT BiquadFilterNode type "highpass" frequency 1000 Q 10
//       AMP GainNode gain 0.5
//     connections
//       NSE->AMP->FLT->OUT
//     modulation
//       ENV AMP gain 1 0.1 0 0
// `,
};
