// @flow

const activeKeys: Set<string> = new Set();

window.addEventListener('keydown', e => activeKeys.add(e.key));
window.addEventListener('keyup', e => activeKeys.delete(e.key));

export default activeKeys;
