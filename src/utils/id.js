// @flow

let lastId = 0;

export default () => `${lastId++}`;
