// @flow

export default function keyMapObj<T>(
  values: Array<T>,
  keyFn: (item: T) => string
): {[key: string]: T} {
  return values.reduce((memo, v) => {
    memo[keyFn(v)] = v;
    return memo;
  }, {});
}
