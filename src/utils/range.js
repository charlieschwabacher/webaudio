// @flow

export default function range(n1: number, n2?: number): Array<number> {
  if (n2 === undefined) {
    n2 = n1;
    n1 = 0;
  }
  return (new Array(Math.max(0, n2 - n1))).fill().map((_, i) => n1 + i);
}
