// @flow

export default function group<T>(
  values: Array<T>,
  keyFn: (item: T) => string
): {[key: string]: Array<T>} {
  return values.reduce((memo, v) => {
    const k = keyFn(v);
    memo[k] = memo[k] || [];
    memo[k].push(v);
    return memo;
  }, {});
}
