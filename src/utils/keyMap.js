// @flow

export default function keyMap<A, B>(
  values: Array<A>,
  keyFn: (item: A) => B
): Map<B, A> {
  return values.reduce((memo, v) => {
    memo.set(keyFn(v), v);
    return memo;
  }, new Map());
}
