// @flow

export default function invariant<T>(condition: ?T, message?: ?string): T {
  if (!condition) throw new Error(message);
  return condition;
}
