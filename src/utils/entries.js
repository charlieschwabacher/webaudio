// @flow

export default function values<T>(obj: {[string]: T}): Array<[string, T]> {
  return Object.keys(obj).map(k => [k, obj[k]]);
}
