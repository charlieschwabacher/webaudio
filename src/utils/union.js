// @flow

export default function union<T>(a: Set<T>, b: Set<T>): Set<T> {
  const result = new Set();
  a.forEach(v => result.add(v));
  b.forEach(v => result.add(v));
  return result;
}
