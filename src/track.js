export default `
config
  tempo 100
  steps 4
  voices 2
sequence
  50....52....53..50--..52--..53--
  57----5a--..60--57--..5a----57..
voice
  nodes
    OSC1 OscillatorNode type "sine"
    AMP1 GainNode
  connections
    OSC1->AMP1->OUT
  modulation
    ENV AMP1 gain 1 0.01
track
  nodes
    AMPT GainNode
  connections
    IN->AMPT->OUT
  modulation
    LFO AMPT gain 0.1 0.3
`.trim();
