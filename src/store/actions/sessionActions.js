// @flow

export const PLAY = 'PLAY';
export const STOP = 'STOP';
export const STAGE_SEQUENCE = 'STAGE_SEQUENCE';
export const PROMOTE_SEQUENCE = 'PROMOTE_SEQUENCE';
export const STOP_MODULE = 'STOP_MODULE';
export const TOGGLE_MUTE = 'TOGGLE_MUTE';
export const TOGGLE_SOLO = 'TOGGLE_SOLO';


export type PlayAction = {
  type: 'PLAY',
  payload: {},
};

export function play(): PlayAction {
  return { type: PLAY, payload: {} };
}


export type StopAction = {
  type: 'STOP',
  payload: {},
};

export function stop(): StopAction {
  return { type: STOP, payload: {} };
}


export type StageSequenceAction = {
  type: 'STAGE_SEQUENCE',
  payload: {
    module: string,
    sequence: string,
  },
};

export function stageSequence(
  module: string,
  sequence: string,
): StageSequenceAction {
  return { type: STAGE_SEQUENCE, payload: { module, sequence } };
}


export type PromoteSequenceAction = {
  type: 'PROMOTE_SEQUENCE',
  payload: {
    module: string,
    beat: number,
  },
};

export function promoteSequence(
  module: string,
  beat: number,
): PromoteSequenceAction {
  return { type: PROMOTE_SEQUENCE, payload: { module, beat } };
}


export type StopModuleAction = {
  type: 'STOP_MODULE',
  payload: {
    module: string,
  },
};

export function stopModule(
  module: string,
): StopModuleAction {
  return { type: STOP_MODULE, payload: { module } };
}


export type ToggleMuteAction = {
  type: 'TOGGLE_MUTE',
  payload: {
    module: string,
    mute: boolean,
  },
};

export function toggleMute(
  module: string,
  mute: boolean,
): ToggleMuteAction {
  return { type: TOGGLE_MUTE, payload: { module, mute } };
}


export type ToggleSoloAction = {
  type: 'TOGGLE_SOLO',
  payload: {
    module: ?string,
  },
};

export function toggleSolo(
  module: ?string,
): ToggleSoloAction {
  return { type: TOGGLE_SOLO, payload: { module } };
}
