// @flow

import type CodeMirror from 'codemirror';

export const INIT_EDITOR = 'INIT_EDITOR';
export const DESTROY_EDITOR = 'DESTROY_EDITOR';
export const REPLACE_TEXT_RANGE = 'REPLACE_TEXT_RANGE';
export const RESET_WITH_TEXT = 'RESET_WITH_TEXT';
export const UPDATE_NOTES = 'UPDATE_NOTES';
export const REMOVE_NOTES = 'REMOVE_NOTES';
export const FLATTEN_NOTES = 'FLATTEN_NOTES';
export const UPDATE_LOOP = 'UPDATE_LOOP';
export const SET_MODULE_LEVEL = 'SET_MODULE_LEVEL';
export const SET_MASTER_LEVEL = 'SET_MASTER_LEVEL';
export const ADD_NODE = 'ADD_NODE';
export const REMOVE_NODE = 'REMOVE_NODE';
export const UPDATE_NODE = 'UPDATE_NODE';
export const CONNECT_NODES = 'CONNECT_NODES';
export const DISCONNECT_NODES = 'DISCONNECT_NODES';
export const CREATE_SEQUENCE = 'CREATE_SEQUENCE';
export const ADD_FILE = 'ADD_FILE';
export const REMOVE_FILE = 'REMOVE_FILE';


export type InitEditorAction = {
  type: 'INIT_EDITOR',
  payload: {
    editor: CodeMirror,
  },
};

export function initEditor(editor: CodeMirror): InitEditorAction {
  return { type: INIT_EDITOR, payload: { editor } };
}


export type DestoryEditorAction = {
  type: 'DESTROY_EDITOR',
  payload: {
    editor: CodeMirror,
  },
};

export function destroyEditor(editor: CodeMirror): DestoryEditorAction {
  return { type: DESTROY_EDITOR, payload: { editor } };
}


export type ReplaceTextRangeAction = {
  type: 'REPLACE_TEXT_RANGE',
  payload: {
    from: number,
    to: number,
    text: string,
  },
};

export function replaceTextRange(
  from: number,
  to: number,
  text: string,
): ReplaceTextRangeAction {
  return { type: REPLACE_TEXT_RANGE, payload: { from, to, text } };
}


export type ResetWithTextAction = {
  type: 'RESET_WITH_TEXT',
  payload: {
    text: string,
  },
};

export function resetWithText(text: string): ResetWithTextAction {
  return { type: RESET_WITH_TEXT, payload: { text } };
}


export type RemoveNotesAction = {
  type: 'REMOVE_NOTES',
  payload: {
    sequence: string,
    ids: Array<string>,
  },
};

export function removeNotes(sequence: string, ids: Array<string>): RemoveNotesAction {
  return { type: REMOVE_NOTES, payload: { sequence, ids } };
}

export type UpdateNotesAction = {
  type: 'UPDATE_NOTES',
  payload: {
    sequence: string,
    notes: { [id: string]: { key: number, start: number, length: number } },
  },
};

export function updateNotes(
  sequence: string,
  notes: { [id: string]: { key: number, start: number, length: number } },
): UpdateNotesAction {
  return { type: UPDATE_NOTES, payload: { sequence, notes } };
}


export type FlattenNotesAction = {
  type: 'FLATTEN_NOTES',
  payload: {
    sequence: string,
  },
};

export function flattenNotes(sequence: string): FlattenNotesAction {
  return { type: FLATTEN_NOTES, payload: { sequence } };
}


export type UpdateLoopAction = {
  type: 'UPDATE_LOOP',
  payload: {
    sequence: string,
    start: number,
    length: number,
  },
};

export function updateLoop(
  sequence: string,
  start: number,
  length: number,
): UpdateLoopAction {
  return { type: UPDATE_LOOP, payload: { sequence, start, length } };
}


export type SetModuleLevelAction = {
  type: 'SET_MODULE_LEVEL',
  payload: {
    module: string,
    level: number,
  },
};

export function setModuleLevel(
  module: string,
  level: number,
): SetModuleLevelAction {
  return { type: SET_MODULE_LEVEL, payload: { module, level } };
}


export type SetMasterLevelAction = {
  type: 'SET_MASTER_LEVEL',
  payload: {
    level: number,
  },
};

export function setMasterLevel(level: number): SetMasterLevelAction {
  return { type: SET_MASTER_LEVEL, payload: { level } };
}


export type AddNodeAction = {
  type: 'ADD_NODE',
  payload: {
    module: string,
    type: string,
  },
};

export function addNode(module: string, type: string): AddNodeAction {
  return { type: ADD_NODE, payload: { module, type } };
}


export type RemoveNodeAction = {
  type: 'REMOVE_NODE',
  payload: {
    module: string,
    node: string,
  },
};

export function removeNode(module: string, node: string): RemoveNodeAction {
  return { type: REMOVE_NODE, payload: { module, node } };
}


export type UpdateNodeAction = {
  type: 'UPDATE_NODE',
  payload: {
    module: string,
    node: string,
    attributes: Object,
  },
};

export function updateNode(
  module: string,
  node: string,
  attributes: Object,
): UpdateNodeAction {
  return { type: UPDATE_NODE, payload: { module, node, attributes } };
}

export type ConnectNodesAction = {
  type: 'CONNECT_NODES',
  payload: {
    module: string,
    from: string,
    to: string,
  },
};

export function connectNodes(
  module: string,
  from: string,
  to: string,
): ConnectNodesAction {
  return { type: CONNECT_NODES, payload: { module, from, to } };
}


export type DisconnectNodesAction = {
  type: 'DISCONNECT_NODES',
  payload: {
    module: string,
    from: string,
    to: string,
  },
};

export function disconnectNodes(
  module: string,
  from: string,
  to: string,
): DisconnectNodesAction {
  return { type: DISCONNECT_NODES, payload: { module, from, to } };
}


export type CreateSequenceAction = {
  type: 'CREATE_SEQUENCE',
  payload: {
    name: string,
    target: string,
  },
};

export function createSequence(
  name: string,
  target: string,
): CreateSequenceAction {
  return { type: CREATE_SEQUENCE, payload: { name, target } };
}


export type AddFileAction = {
  type: 'ADD_FILE',
  payload: {
    id: string,
    url: string,
  },
};

export function addFile(id: string, url: string): AddFileAction {
  return { type: ADD_FILE, payload: { id, url } };
}


export type RemoveFileAction = {
  type: 'REMOVE_FILE',
  payload: {
    id: string,
  },
};

export function removeFile(id: string): RemoveFileAction {
  return { type: REMOVE_FILE, payload: { id } };
}
