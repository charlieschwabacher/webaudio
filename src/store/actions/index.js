// @flow

import type {
  InitEditorAction,
  DestoryEditorAction,
  ReplaceTextRangeAction,
  ResetWithTextAction,
  UpdateNotesAction,
  RemoveNotesAction,
  FlattenNotesAction,
  UpdateLoopAction,
  SetModuleLevelAction,
  SetMasterLevelAction,
  CreateSequenceAction,
  AddFileAction,
  RemoveFileAction,
} from './projectActions';
import type {
  PlayAction,
  StopAction,
  StageSequenceAction,
  PromoteSequenceAction,
  StopModuleAction,
  ToggleMuteAction,
  ToggleSoloAction,
} from './sessionActions';

export type Action = (
  InitEditorAction |
  DestoryEditorAction |
  ReplaceTextRangeAction |
  ResetWithTextAction |
  UpdateNotesAction |
  RemoveNotesAction |
  FlattenNotesAction |
  UpdateLoopAction |
  SetModuleLevelAction |
  SetMasterLevelAction |
  CreateSequenceAction |
  AddFileAction |
  RemoveFileAction |
  PlayAction |
  StopAction |
  StageSequenceAction |
  PromoteSequenceAction |
  StopModuleAction |
  ToggleMuteAction |
  ToggleSoloAction
);

export * from './projectActions';
export * from './sessionActions';
