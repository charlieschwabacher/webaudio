// @flow
import { createStore, combineReducers } from 'redux';
import * as reducers from './reducers';
export * from './actions';

export type { Session } from './reducers/project/session';

export default createStore(
  combineReducers(reducers),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
