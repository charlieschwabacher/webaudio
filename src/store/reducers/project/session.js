// @flow
import {
  PLAY,
  STOP,
  STAGE_SEQUENCE,
  PROMOTE_SEQUENCE,
  STOP_MODULE,
  TOGGLE_MUTE,
  TOGGLE_SOLO,
  CREATE_SEQUENCE,
} from '../../actions';
import type {
  Action,
  StageSequenceAction,
  PromoteSequenceAction,
  StopModuleAction,
  ToggleSoloAction,
  ToggleMuteAction,
  CreateSequenceAction,
} from '../../actions';
import { keyMap, group } from '../../../utils';
import type {
  Root as RootAST,
  Module as ModuleAST,
  Sequence as SequenceAST,
} from '../../../language';

export type Session = {
  playing: boolean,
  solo: ?string, // module id
  modules: {
    [id: string]: {
      mute: boolean,
      playing: {
        sequence: string, // sequence id
        start: number, // beat the loop started
      },
      next: ?string, // sequence id
    }
  }
}

// add session objects for any modules that do not have them
export function updateSessionModules(
  session: Session,
  modules: Array<ModuleAST>,
): Session {
  const updatedModules = { ...session.modules };

  modules.forEach(module => {
    if (updatedModules[module.name] == null) {
      updatedModules[module.name] = {
        mute: false,
        playing: null,
        next: null,
      };
    }
  });

  return {
    ...session,
    modules: updatedModules,
  };
}

// remove references to any squences that do not exist from session state
export function updateSessionSequences(
  session: Session,
  sequences: Array<SequenceAST>,
): Session {
  const sequencesByName = keyMap(sequences, s => s.name);
  const updatedModules = { ...session.modules };

  Object.keys(session.modules).forEach(moduleName => {
    const module = session.modules[moduleName];
    if (module.playing && !sequencesByName.has(module.playing.sequence)) {
      updatedModules[moduleName].playing = null;
    }
    if (module.next && !sequencesByName.has(module.next)) {
      updatedModules[moduleName].next = null;
    }
  });

  return {
    ...session,
    modules: updatedModules,
  };
}

// play the first sequence for each module
export function resetPlayback(session: Session, ast: RootAST): Session {
  const updatedModules = { ...session.modules };
  const sequencesByTarget = group(
    ast.sequences,
    s => s.target.value,
  );

  for (const name in session.modules) {
    if (sequencesByTarget[name]) {
      updatedModules[name] = {
        ...updatedModules[name],
        playing: {
          sequence: sequencesByTarget[name][0].name,
          start: 0,
        },
      };
    }
  }

  return {
    ...session,
    modules: updatedModules,
    playing: false,
  };
}

function play(state: Session): Session {
  return {
    ...state,
    playing: true,
  };
}

function stop(state: Session): Session {
  return {
    ...state,
    playing: false,
    modules: Object.keys(state.modules).reduce((memo, name) => {
      const module = state.modules[name];
      memo[name] = {
        ...module,
        playing: (
          module.playing
          ? {
            ...module.playing,
            start: 0,
          }
          : null
        ),
        next: null,
      };
      return memo;
    }, {}),
  };
}

function stageSequence(
  state: Session,
  action: StageSequenceAction,
): Session {
  const module =  state.modules[action.payload.module];

  // if sequence is already playing, it can't be staged
  if (module.playing && module.playing.sequence === action.payload.sequence) {
    return state;
  }

  return {
    ...state,
    modules: {
      ...state.modules,
      [action.payload.module]: {
        ...module,
        next: action.payload.sequence,
      },
    },
  };
}

function promoteSequence(
  state: Session,
  action: PromoteSequenceAction,
): Session {
  const next = state.modules[action.payload.module].next;
  return {
    ...state,
    modules: {
      ...state.modules,
      [action.payload.module]: {
        ...state.modules[action.payload.module],
        playing: {
          sequence: next,
          start: action.payload.beat,
        },
        next: null,
      },
    },
  };
}

function stopModule(
  state: Session,
  action: StopModuleAction,
): Session {
  return {
    ...state,
    modules: {
      ...state.modules,
      [action.payload.module]: {
        ...state.modules[action.payload.module],
        playing: null,
        next: null,
      }
    },
  };
}

function toggleMute(
  state: Session,
  action: ToggleMuteAction,
): Session {
  return {
    ...state,
    modules: {
      ...state.modules,
      [action.payload.module]: {
        ...state.modules[action.payload.module],
        mute: action.payload.mute,
      },
    }
  }
}

function toggleSolo(state: Session, action: ToggleSoloAction): Session {
  return {
    ...state,
    solo: action.payload.module,
  };
}

function createSequence(
  state: Session,
  action: CreateSequenceAction,
): Session {
  const module =  state.modules[action.payload.target];
  return {
    ...state,
    modules: {
      ...state.modules,
      [action.payload.target]: {
        ...module,
        next: action.payload.name,
      },
    },
  };
}

export default function session(state: Session, action: Action): Session {
  switch (action.type) {
    case PLAY:
      return play(state);
    case STOP:
      return stop(state);
    case STAGE_SEQUENCE:
      return stageSequence(state, action);
    case PROMOTE_SEQUENCE:
      return promoteSequence(state, action);
    case STOP_MODULE:
      return stopModule(state, action);
    case TOGGLE_MUTE:
      return toggleMute(state, action);
    case TOGGLE_SOLO:
      return toggleSolo(state, action);
    case CREATE_SEQUENCE:
      return createSequence(state, action);
    default:
      return state;
  }
}
