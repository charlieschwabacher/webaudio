// @flow
import type { Root, ASTNode } from '../../../language';
import type { Session } from './session';
import type { Matcher } from 'ohm-js';
import type CodeMirror, { TextMarker } from 'codemirror';

export type ProjectState = {
  matcher: Matcher,
  editor: ?CodeMirror,
  markers: Array<TextMarker>,
  session: Session,
  parseError: ?string,
  ast: Root,
};

export type Change = {
  ast: Root,
  added: Array<ASTNode>,
  changed: Array<ASTNode>,
  removed: Array<ASTNode>,
};
