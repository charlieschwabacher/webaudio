// @flow
import {
  UPDATE_NOTES,
  REMOVE_NOTES,
  FLATTEN_NOTES,
  UPDATE_LOOP,
  SET_MODULE_LEVEL,
  SET_MASTER_LEVEL,
  CREATE_SEQUENCE,
} from '../../actions';
import { values } from '../../../utils';
import {
  Root,
  Project,
  Sequence,
  Section,
  Module,
  Config,
  Note,
} from '../../../language';
import type {
  UpdateNotesAction,
  RemoveNotesAction,
  FlattenNotesAction,
  UpdateLoopAction,
  SetModuleLevelAction,
  SetMasterLevelAction,
  CreateSequenceAction,
} from '../../actions';
import type { Change } from './types';

function updateNotes(state: Root, action: UpdateNotesAction): Change {
  const { sequence, notes } = action.payload;
  const sequenceIndex = state.sequences.findIndex(s => s.name === sequence);
  const sequenceAST = state.sequences[sequenceIndex];

  let anyChanged = false;
  for (let id in notes) {
    const updated = notes[id];
    const existing = sequenceAST.notes.itemsById[id];
    if (
      !existing ||
      updated.key !== existing.key ||
      updated.start !== existing.start ||
      updated.length !== existing.length
    ) {
      anyChanged = true;
      break;
    }
  }

  if (!anyChanged) {
    return {
      ast: state,
      added: [],
      changed: [],
      removed: [],
    };
  }

  const added = [];
  const changed = [];

  const newNotes = values(
    Object.keys(notes).reduce((memo, id) => {
      const existing = memo[id];

      let next;
      if (existing) {
        next = new Note({ ...existing, ...notes[id] });
        changed.push(next);
      } else {
        next = new Note({
          ...notes[id],
          _start: NaN,
          _offset: sequenceAST.notes._length,
          _length: 0,
        });
        added.push(next);
      }

      memo[id] = next;
      return memo;
    }, sequenceAST.notes.itemsById),
  );

  return {
    ast: new Root({
      ...state,
      sequences: [
        ...state.sequences.slice(0, sequenceIndex),
        new Sequence({
          ...sequenceAST,
          notes: new Section({
            ...sequenceAST.notes,
            items: newNotes,
          }),
        }),
        ...state.sequences.slice(sequenceIndex + 1),
      ],
    }),
    added,
    changed,
    removed: [],
  };
}

function removeNotes(state: Root, action: RemoveNotesAction): Change {
  const { sequence, ids } = action.payload;
  const sequenceIndex = state.sequences.findIndex(s => s.name === sequence);
  const sequenceAST = state.sequences[sequenceIndex];
  const notes = sequenceAST.notes;
  const removeIds = new Set(ids);

  const items = [];
  const removed = [];
  notes.items.forEach(note => {
    if (removeIds.has(note._id)) {
      removed.push(note);
    } else {
      items.push(note);
    }
  });

  const nextNotes = new Section({
    ...notes,
    items,
  });
  removed.forEach(node => {
    node._parent = nextNotes;
  });

  return {
    ast: new Root({
      ...state,
      sequences: [
        ...state.sequences.slice(0, sequenceIndex),
        new Sequence({
          ...sequenceAST,
          notes: nextNotes,
        }),
        ...state.sequences.slice(sequenceIndex + 1),
      ],
    }),
    added: [],
    changed: [],
    removed,
  };
}

function flattenNotes(state: Root, action: FlattenNotesAction): Change  {
  const { sequence } = action.payload;
  const sequenceIndex = state.sequences.findIndex(s => s.name === sequence);
  const sequenceAST = state.sequences[sequenceIndex];
  const notes = sequenceAST.notes;

  const flatNotes = {};
  const removed = [];
  notes.items.forEach(note => {
    const startAndKey = `${note.start}:${note.key}`;
    if (flatNotes[startAndKey]) {
      removed.push(flatNotes[startAndKey]);
    }
    flatNotes[startAndKey] = note;
  });

  if (removed.length === 0) {
    return { ast: state, added: [], changed: [], removed };
  }

  return {
    ast: new Root({
      ...state,
      sequences: [
        ...state.sequences.slice(0, sequenceIndex),
        new Sequence({
          ...sequenceAST,
          notes: new Section({
            ...notes,
            items: values(flatNotes),
          }),
        }),
        ...state.sequences.slice(sequenceIndex + 1),
      ],
    }),
    added: [],
    changed: [],
    removed,
  };
}

function updateLoop(state: Root, action: UpdateLoopAction): Change {
  const { sequence, start, length } = action.payload;
  const sequenceIndex = state.sequences.findIndex(s => s.name === sequence);
  const sequenceAST = state.sequences[sequenceIndex];

  const changed = [];

  let nextStart;
  if (start !== sequenceAST.start.value) {
    nextStart = new Config({
      ...sequenceAST.start,
      value: start,
    });
    changed.push(nextStart);
  } else {
    nextStart = sequenceAST.start;
  }

  let nextLength;
  if (length !== sequenceAST.length.value) {
    nextLength = new Config({
      ...sequenceAST.length,
      value: length,
    });
    changed.push(nextLength);
  } else {
    nextLength = sequenceAST.length;
  }

  return {
    ast: new Root({
      ...state,
      sequences: [
        ...state.sequences.slice(0, sequenceIndex),
        new Sequence({
          ...sequenceAST,
          start: nextStart,
          length: nextLength,
        }),
        ...state.sequences.slice(sequenceIndex + 1),
      ],
    }),
    added: [],
    changed,
    removed: [],
  };
}

function setModuleLevel(state: Root, action: SetModuleLevelAction): Change {
  const { module, level } = action.payload;
  const moduleIndex = state.modules.findIndex(m => m.name === module);
  const moduleAST = state.modules[moduleIndex];

  const nextLevel = new Config({
    ...moduleAST.level,
    value: level,
  });

  return {
    ast: new Root({
      ...state,
      modules: [
        ...state.modules.slice(0, moduleIndex),
        new Module({
          ...moduleAST,
          level: nextLevel,
        }),
        ...state.modules.slice(moduleIndex + 1),
      ],
    }),
    added: [],
    changed: [ nextLevel ],
    removed: [],
  };
}

function setMasterLevel(state: Root, action: SetMasterLevelAction): Change {
  const { level } = action.payload;

  const nextLevel = new Config({
    ...state.project.level,
    value: level,
  })

  return {
    ast: new Root({
      ...state,
      project: new Project({
        ...state.project,
        level: nextLevel,
      })
    }),
    added: [],
    changed: [ nextLevel ],
    removed: [],
  };
}

function createSequence(state: Root, action: CreateSequenceAction): Change {
  const { name, target } = action.payload;

  // TODO: offsets
  const newSequence = new Sequence({
    name,
    target: new Config({
      name: 'target',
      kind: 'string',
      value: target,
      _start: NaN,
      _offset: 10 + name.length,
      _length: 10 + target.length,
    }),
    start: new Config({
      name: 'start',
      kind: 'number',
      value: 0,
      _start: NaN,
      _offset: 20 + name.length + target.length,
      _length: 10,
    }),
    length: new Config({
      name: 'length',
      kind: 'number',
      value: 4,
      _start: NaN,
      _offset: 30 + name.length + target.length,
      _length: 11,
    }),
    notes: new Section({
      name: 'notes',
      kind: 'note',
      items: [],
      _start: NaN,
      _offset: 41 + name.length + target.length,
      _length: 8,
    }),
    modulation: [],
    _start: NaN,
    _offset: state._length,
    _length: 0,
  });

  return {
    ast: new Root({
      ...state,
      sequences: [
        ...state.sequences,
        newSequence,
      ],
    }),
    added: [ newSequence ],
    changed: [],
    removed: [],
  };
}

export default function ast(state: Root, action: Object): Change {
  switch (action.type) {
    case UPDATE_NOTES:
      return updateNotes(state, action);
    case REMOVE_NOTES:
      return removeNotes(state, action);
    case FLATTEN_NOTES:
      return flattenNotes(state, action);
    case UPDATE_LOOP:
      return updateLoop(state, action);
    case SET_MODULE_LEVEL:
      return setModuleLevel(state, action);
    case SET_MASTER_LEVEL:
      return setMasterLevel(state, action);
    case CREATE_SEQUENCE:
      return createSequence(state, action);
    default:
      return { ast: state, added: [], changed: [], removed: [] };
  }
}
