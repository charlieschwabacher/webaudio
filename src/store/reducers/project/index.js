// @flow
import { semantics } from '../../../language';
import { invariant } from '../../../utils';
import {
  INIT_EDITOR,
  DESTROY_EDITOR,
  RESET_WITH_TEXT,
  REPLACE_TEXT_RANGE,
} from '../../actions';
import reduceAST from './ast';
import {
  default as reduceSession,
  updateSessionModules,
  updateSessionSequences,
  resetPlayback,
} from './session';
import initialProject from './initialProject';
import type { ProjectState } from './types';
import type { Action } from '../../actions';

export default function project(
  state: ProjectState = initialProject,
  action: Action,
): ProjectState {
  let {
    matcher,
    editor,
    markers,
    session,
    ast,
    parseError,
  } = state;

  if (action.type === INIT_EDITOR) {
    editor = action.payload.editor;
    editor.setValue(matcher.getInput());
  } else if (action.type === DESTROY_EDITOR) {
    editor = null;
  }

  // parse text and catch any parse error for set text actions, otherwise
  // delegate to the ast reducer
  if (action.type === RESET_WITH_TEXT || action.type === REPLACE_TEXT_RANGE) {
    // efficiently update the ohm matcher
    if (action.type === RESET_WITH_TEXT) {
      const { text } = action.payload;
      matcher.setInput(text);
      if (editor != null) {
        editor.setValue(text);
      }
    }
    if (action.type === REPLACE_TEXT_RANGE) {
      const { from, to, text } = action.payload;
      matcher.replaceInputRange(from, to, text);
    }

    const match = matcher.match();
    if (match.succeeded()) {
      parseError = null;
      ast = semantics(match).ast;
    } else {
      parseError = match.message;
    }

    // clear marked changes in editor
    markers.forEach(marker => marker.clear());
    markers = [];
  } else {
    const {
      added,
      changed,
      removed,
      ast: nextAST,
    } = reduceAST(state.ast, action);

    // if there are changes
    if (nextAST !== state.ast) {
      ast = nextAST;

      // clear markers
      markers.forEach(marker => marker.clear());
      markers = [];

      // splice in changes to text editor and matcher
      const doUpdate = () => {
        added.forEach(node => {
          const { start } = node.textLocation();
          const text = node.print();
          matcher.replaceInputRange(start, start, text);
          if (editor != null) {
            editor.replaceRange(
              text,
              editor.posFromIndex(start),
              editor.posFromIndex(start),
              'replaceRange',
            );
          }
          node.updateText();
        });
        changed.forEach(node => {
          const { start, end } = node.textLocation();
          const text = node.print();
          matcher.replaceInputRange(start, end, text);
          if (editor != null) {
            editor.replaceRange(
              text,
              editor.posFromIndex(start),
              editor.posFromIndex(end),
              'replaceRange',
            );
          }
          node.updateText();
        });
        removed.forEach(node => {
          const { start, end } = node.textLocation();
          matcher.replaceInputRange(start, end, '');
          if (editor != null) {
            editor.replaceRange(
              '',
              editor.posFromIndex(start),
              editor.posFromIndex(end),
              'replaceRange',
            );
          }
          node.removeText();
        });
      };

      if (editor == null) {
        doUpdate();
      } else {
        editor.operation(doUpdate);
        editor.operation(() => {
          // mark changes
          [ ...added, ...changed ].forEach(node => {
            invariant(editor);
            const { start, end } = node.textLocation();
            const adjustedStart = start + node.print().search(/[^ ]/);
            markers.push(
              editor.markText(
                editor.posFromIndex(adjustedStart),
                editor.posFromIndex(end),
                { className: 'editor__codemirror-active-marker' },
              ),
            );
          });
        });
      }

      parseError = null;
    }
  }

  // if modules have changed, ensure session info is created
  if (ast.modules !== state.ast.modules) {
    session = updateSessionModules(session, ast.modules);
  }

  // if sequences have changed, remove any bad references from session object
  if (ast.sequences !== state.ast.sequences) {
    session = updateSessionSequences(session, ast.sequences);
  }

  // if we reset the project, play the first sequence for each module
  if (action.type === RESET_WITH_TEXT) {
    session = resetPlayback(session, ast);
  }

  // delegate to session reducer
  session = reduceSession(session, action);

  const nextState = {
    matcher,
    editor,
    markers,
    session,
    ast,
    parseError,
  };

  // TODO: this is for debugging, remove it
  window.state = nextState;

  return nextState;
};
