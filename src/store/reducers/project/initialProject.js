// @flow
import presetTracks from '../../../presetTracks';
import { grammar, semantics } from '../../../language';
import {
  updateSessionModules,
  resetPlayback,
} from './session';

const text = presetTracks.test.replace(/\t/g, '')
const matcher = grammar.matcher();
matcher.setInput(text);
const ast = semantics(matcher.match()).ast;

const session = resetPlayback(
  updateSessionModules(
    {
      playing: false,
      solo: null,
      modules: {},
    },
    ast.modules,
  ),
  ast,
);

export default {
  matcher,
  ast,
  session,
  editor: null,
  markers: [],
  parseError: null,
};
