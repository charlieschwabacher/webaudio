// @flow

// these nodes will have 'start' called after creation
export const SOURCE_TYPES: Set<string> = new Set([
  'AudioBufferSourceNode',
  'ConstantSourceNode',
  'EnvelopeNode',
  'OscillatorNode',
  'SequenceNode',
  'WhiteNoiseNode',
]);

// these nodes will have noteOn and noteOff called
export const SEQUENCE_TYPES: Set<string> = new Set([
  'EnvelopeNode',
  'SequenceNode',
  'SamplerNode',
]);

export const DEFAULT_NODE_OPTIONS: {[string]: Object} = {
  // OscillatorNode: {
  //   frequency: 440
  // },
};

export const MIN_TIME = 0.002;
