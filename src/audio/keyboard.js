// @flow
// TODO: this file isn't used and it's imports have been refactored away - the
// functionality is good though, so it should be update and restored at some
// point

// import { GLOBAL_CONTEXT, MIN_TIME } from './build';
// import type { Track } from './build';
//
// const keyOffsets = {
//   'a': -1,
//   'z': 0,
//   's': 1,
//   'x': 2,
//   'c': 3,
//   'f': 4,
//   'v': 5,
//   'g': 6,
//   'b': 7,
//   'n': 8,
//   'j': 9,
//   'm': 10,
//   'k': 11,
//   ',': 12,
//   'l': 13,
//   '.': 14,
//   '/': 15,
//   '\'': 16,
// }
//
// let track: ?Track;
// let timeout;
//
// export default function keyboard(t: Track) {
//   if (track) {
//     track.output.gain.setTargetAtTime(0, GLOBAL_CONTEXT.currentTime, MIN_TIME);
//   }
//   clearTimeout(timeout);
//   timeout = setTimeout(() => {
//     track && track.output.disconnect();
//     track = t;
//   });
// }
//
// window.onkeydown = e => {
//   if (track == null || e.repeat) return;
//
//   const offset = keyOffsets[e.key];
//   if (offset == null) return;
//   const key = 69 + offset;
//
//   track.play(key, GLOBAL_CONTEXT.currentTime);
// }
//
// window.onkeyup = e => {
//   if (track == null) return;
//
//   const offset = keyOffsets[e.key];
//   if (offset == null) return;
//   const key = 69 + offset;
//
//   track.stop(key, GLOBAL_CONTEXT.currentTime);
// }
