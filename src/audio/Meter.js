// @flow

const CLIP_LEVEL = 0.999;
const AVERAGING = 0.98;
const CLIP_LAG = 0.25;

export default class Meter {
  context: AudioContext
  processor: ScriptProcessorNode
  clipping: boolean
  lastClip: number
  level: number

  lastClip = 0;
  level = 0;

  constructor(context: AudioContext) {
    this.context = context;
    this.processor = context.createScriptProcessor(512, 1, 1);
    this.processor.connect(context.destination);

    this.processor.onaudioprocess = e => {
      const buf = e.inputBuffer.getChannelData(0);
      const bufLength = buf.length;

      let max = 0;
      for (let i = 0; i < bufLength; i++) {
        max = Math.max(Math.abs(buf[i]), max);
      }
      if (max >= CLIP_LEVEL) {
        this.lastClip = context.currentTime;
      }
      this.level = Math.max(max, this.level * AVERAGING);
    };
  }

  connectFrom(node: AudioNode) {
    node.connect(this.processor);
  }

  disconnect() {
    this.processor.disconnect();
  }

  isClipping() {
    return this.lastClip + CLIP_LAG > this.context.currentTime;
  }
}
