// @flow

export type Message = {
  type: 'ON' | 'OFF',
  key: number,
  beat: number,
};
