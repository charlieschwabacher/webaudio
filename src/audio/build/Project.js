// @flow

import update from './update';
import { MIN_TIME } from '../constants';
import FileLibrary from './FileLibrary';
import Module from './Module';
import Sequence from './Sequence';
import { invariant } from '../../utils';
import { promoteSequence } from '../../store/actions';
import type { Root as RootAST } from '../../language';
import type { Session } from '../../store';
import type { Action } from '../../store/actions';


// time ahead in seconds to schedule changes to audioparams
const LOOKAHEAD = 0.1;

// time in ms between sequence steps
const STEP = LOOKAHEAD / 4 * 1000;

export default class Project {
  ast: RootAST
  session: Session
  dispatch: (action: Action) => void
  context: AudioContext
  master: GainNode
  analyser: AnalyserNode
  library: FileLibrary
  modules: Map<string, Module>
  sequences: Map<string, Sequence>

  playing: boolean
  timeout: TimeoutID

  // the time that playback was started
  // (relative to the same origin as currentTime of the audio contxt)
  globalStartTime: number

  // time processed by sequences relative to globalStartTime, this should be
  // greater than the currentTime of the audio context
  playedTo: number

  constructor(
    ast: RootAST,
    session: Session,
    dispatch: (action: Action) => void,
  ) {
    const context = new AudioContext();
    const master = new GainNode(context, { gain: ast.project.level.value });
    const analyser = new AnalyserNode(context, { fftSize: 128 });
    const library = new FileLibrary(context, ast.project.files);

    master.connect(context.destination);
    master.connect(analyser);

    this.dispatch = dispatch;
    this.context = context;
    this.master = master;
    this.analyser = analyser;
    this.library = library;
    this.modules = new Map();
    this.sequences = new Map();
    this.playing = false;

    this.update(ast, session);
  }

  update(ast: RootAST, session: Session) {
    const { library, modules, sequences } = this;

    // update files
    if (library.ast !== ast.project.files) {
      library.update(ast.project.files);
    }

    // update modules
    this.modules = update(
      modules,
      ast.modules,
      ast => {
        const m = new Module(this.context, this.library, ast, session);
        m.output.connect(this.master);
        return m;
      },
      (module, ast) => {
        if (module.ast !== ast || module.session !== session) {
          module.update(ast, session);
        }
      },
      module => module.disconnect(),
    );

    // update sequences
    this.sequences = update(
      sequences,
      ast.sequences,
      ast => new Sequence(this, ast),
      (sequence, ast) => {
        if (sequence.ast !== ast) {
          sequence.update(ast);
        }
      },
      () => {},
    );

    // update level
    this.master.gain.linearRampToValueAtTime(
      ast.project.level.value,
      this.context.currentTime + MIN_TIME,
    );

    // play or pause
    if (session.playing && !this.playing) {
      this.start();
    } else if (!session.playing && this.playing) {
      this.stop();
    }

    this.ast = ast;
    this.session = session;
  }

  start = () => {
    this.playing = true;
    this.globalStartTime = this.context.currentTime;
    this.playedTo = 0;
    this.step();
  }

  stop = () => {
    this.playing = false;
    this.timeout = setTimeout(() => {
      this.sequences.forEach(sequence => {
        sequence.stop(this.context.currentTime);
      });
    }, LOOKAHEAD);
  }

  suspend = () => {
    this.context.suspend();
  }

  resume = () => {
    this.context.resume().then(() => this.step());
  }

  step = () => {
    if (!this.playing || this.context.state !== 'running') return;

    const globalTime = this.context.currentTime;
    const currentTime = globalTime - this.globalStartTime;
    const playFromTime = this.playedTo;
    const playToTime = currentTime + LOOKAHEAD;
    const bps = this.ast.project.tempo.value / 60; // beats per second

    this.modules.forEach((module, moduleId) => {
      const { playing, next } = this.session.modules[moduleId];

      let promotingNext = false;
      let playNextFromTime;
      let nextSequence;
      if (next) {
        nextSequence = this.sequences.get(next);
        invariant(nextSequence);

        const length = nextSequence.ast.length.value / bps;
        if (
          Math.floor(playToTime / length) > Math.floor(playFromTime / length)
        ) {
          promotingNext = true;
          playNextFromTime = Math.floor(playToTime / length) * length;
        }
      }

      if (playing) {
        const sequence = this.sequences.get(playing.sequence);
        invariant(sequence);

        let playCurrentToTime = playToTime;
        if (promotingNext) {
          invariant(playNextFromTime);
          playCurrentToTime = playNextFromTime;
        }

        sequence.step(
          globalTime,
          currentTime,
          playFromTime,
          playCurrentToTime,
          bps,
        );
      }

      if (promotingNext) {
        invariant(playNextFromTime && nextSequence);
        nextSequence.step(
          globalTime,
          currentTime,
          playNextFromTime,
          playToTime,
          bps,
        );

        this.dispatch(promoteSequence(moduleId, playNextFromTime * bps));
      }
    });

    this.playedTo = playToTime;
    setTimeout(this.step, STEP);
  }
}
