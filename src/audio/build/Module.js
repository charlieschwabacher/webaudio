// @flow

// A track represents an instrument encompassing one or more voices and a
// subgraph processing the mixed output of all voices, resulting in a single
// ouput node.  Modules receive note on and note off calls from a sequence and
// are responsible for distributing them between voices.

import Voice from './Voice';
import FileLibrary from './FileLibrary';
import { buildVoices } from './build';
import { MIN_TIME } from '../constants';
import Meter from '../Meter';
import type { Module as ModuleAST } from '../../language';
import type { Session } from '../../store';

export default class Module {
  context: AudioContext
  library: FileLibrary
  ast: ModuleAST
  session: Session
  input: GainNode
  output: GainNode
  meter: Meter
  voices: Array<Voice>
  voicesByKey: Map<number, Voice>
  keysByVoice: Map<Voice, number>

  constructor(
    context: AudioContext,
    library: FileLibrary,
    ast: ModuleAST,
    session: Session,
  ) {
    // create track audio graph
    const input = new GainNode(context);
    const output = new GainNode(context, { gain: ast.level.value });
    const meter = new Meter(context);

    // connect meter
    meter.connectFrom(output);

    // build voices
    const numVoices = ast.voices.value;
    const voices = buildVoices(context, library, input, output, ast, numVoices);

    this.context = context;
    this.library = library;
    this.ast = ast;
    this.session = session;
    this.input = input;
    this.output = output;
    this.meter = meter;
    this.voices = voices;
    this.voicesByKey = new Map();
    this.keysByVoice = new Map();
  }

  update(ast: ModuleAST, session: Session) {
    if (ast.voices !== this.ast.voices) {
      const delta = ast.voices.value - this.ast.voices.value;
      if (delta > 0) {
        this.voices.push(...buildVoices(
          this.context,
          this.library,
          this.input,
          this.output,
          ast,
          delta,
        ));
      } else {
        const nextVoices = this.voices.slice(0, delta);
        const removedVoices = this.voices.slice(delta);
        removedVoices.forEach(voice => voice.disconnect);
        this.voices = nextVoices;
      }
    }

    if (
      ast.nodes !== this.ast.nodes ||
      ast.connections !== this.ast.connections
    ) {
      this.voices.forEach(voice => voice.update(ast));
    }

    const solo = session.solo === ast.name;
    const anySolo = session.solo != null;
    const mute = session.modules[ast.name].mute;
    const level = (
      solo || (!mute && !anySolo)
      ? ast.level.value
      : 0
    );

    this.output.gain.linearRampToValueAtTime(
      level,
      this.context.currentTime + MIN_TIME,
    );

    this.ast = ast;
  }

  disconnect() {
    this.input.disconnect();
    this.output.disconnect();
  }

  noteOn(key: number, time: number) {
    // find voice w/ rules:
    // - use the first voice that is not on
    // - if all voices are on, use the first voice
    const index = Math.max(this.voices.findIndex(v => !v.on), 0);
    const voice = this.voices[index];

    // demote to end of voices array, add to voicesByKey map
    this.voices.push(voice);
    this.voices.splice(index, 1);

    // track use
    const { keysByVoice, voicesByKey } = this;
    const lastKey = keysByVoice.get(voice);
    if (lastKey != null && voicesByKey.get(lastKey) === voice) {
      voicesByKey.delete(lastKey);
    }
    voicesByKey.set(key, voice);
    keysByVoice.set(voice, key);

    // play key
    voice.noteOn(key, time);
  }

  noteOff(key: number, time: number) {
    const voice = this.voicesByKey.get(key);
    if (voice != null) {
      voice.noteOff(time);
      this.voicesByKey.delete(key);
    }
  }

  stop(time: number) {
    this.voices.forEach(voice => voice.noteOff(time));
    this.voicesByKey = new Map();
  }
}
