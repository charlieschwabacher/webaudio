// @flow

import type { Section, File } from '../../language';

export default class FileLibrary {
  context: AudioContext
  ast: Section<File>
  files: Map<string, Promise<AudioBuffer>>

  constructor(context: AudioContext, ast: Section<File>) {
    this.context = context;
    this.files = new Map();
    this.update(ast);
  }

  update(ast: Section<File>): void {
    const urls = ast.items.map(({ url }) => url);
    const next = new Set(urls);
    urls.forEach(url => {
      if (!this.files.has(url)) {
        this.load(url);
      }
    });
    this.files.forEach((_, url) => {
      if (!next.has(url)) {
        this.release(url);
      }
    });
    this.ast = ast;
  }

  load(url: string): void {
    this.files.set(
      url,
      fetch(url)
      .then(response => response.arrayBuffer())
      .then(arrayBuffer => this.context.decodeAudioData(arrayBuffer)),
    );
  }

  release(url: string): void {
    this.files.delete(url);
  }

  get(url: string): Promise<AudioBuffer> {
    return this.files.get(url) || Promise.reject();
  }
}
