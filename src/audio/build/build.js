// @flow

import {
  DEFAULT_NODE_OPTIONS,
  SOURCE_TYPES,
} from '../constants';
import { invariant } from '../../utils';
import FileLibrary from './FileLibrary';
import Voice from './Voice';
import type { Message } from './types';
import AUDIO_NODES from '../nodes';
import {
  Section as SectionAST,
  Connection as ConnectionAST,
  Module as ModuleAST,
  ANode as ANodeAST,
  Sequence as SequenceAST,
} from '../../language';

export function buildVoices(
  context: AudioContext,
  library: FileLibrary,
  input: GainNode,
  output: GainNode,
  ast: ModuleAST,
  count: number,
): Array<Voice> {
  return (new Array(count)).fill().map(() => {
    const voice = new Voice(context, library, ast);
    input.connect(voice.input);
    voice.output.connect(output);
    return voice;
  });
}

export function buildNodes(
  context: AudioContext,
  library: FileLibrary,
  astNodes: SectionAST<ANodeAST>,
): Map<string, AudioNode> {
  const nodes = new Map();

  astNodes.items.forEach(ast => {
    const node = buildNode(context, library, ast);
    nodes.set(ast.name, node);
  });

  return nodes;
}

export function buildNode(
  context: AudioContext,
  library: FileLibrary,
  ast: ANodeAST,
): AudioNode {
  const { nodeType, options } = ast;
  const NodeClass = AUDIO_NODES[nodeType];
  const defaultOptions = DEFAULT_NODE_OPTIONS[nodeType] || {};
  const specifiedOptions = options.reduce((memo, o) => {
    memo[o.key] = o.value;
    return memo;
  }, {});
  const completeOptions = { ...defaultOptions, ...specifiedOptions };
  invariant(NodeClass != null, `urecognized node type '${nodeType}'`);

  // the buffer option expects an AudioBuffer - we have a string from the ast,
  // but can look up the AudioBuffer through the FileLibrary
  const buffer = completeOptions.buffer;
  delete completeOptions.buffer;

  const node = new NodeClass(context, completeOptions);

  if (buffer) {
    library.get(buffer).then(buffer => {
      node.buffer = buffer;
    });
  }

  if (SOURCE_TYPES.has(nodeType)) {
    node.start();
  }

  return node;
}

export function connectNodes(
  nodes: Map<string, AudioNode>,
  connections: SectionAST<ConnectionAST>,
  input: AudioNode,
  output: AudioNode,
): void {
  connections.items.forEach(connection => {
    const {from, to, param} = connection;

    const fromNode = from === 'IN' ? input : nodes.get(from);
    const toNode = to === 'OUT' ? output : nodes.get(to);
    invariant(fromNode != null, `unknown node ${from}`);
    invariant(toNode != null, `unknown node ${to}`);

    let target = toNode;
    if (param != null) {

      // BUG: flow doesn't handle [] access to AudioNode
      target = (toNode: Object)[param];

      invariant(
        target instanceof AudioParam,
        `cannot find audio param '${to}'`,
      );
    }

    fromNode.connect(target);
  });
}

// we split notes into 'ON', 'OFF' messages, and then sort them by time and so
// that when both 'ON' and 'OFF' messages occur at exactly the same time, the
// 'OFF' message always comes first.

// this ignores notes outside the defined loop, and adjusts start so that it is
// relative to the start of the loop

export function buildMessages(
  sequence: SequenceAST,
): Array<Message> {
  const messages = sequence.notes.items.reduce(
    (memo, { start, length, key }) => {
      if (
        start >= sequence.start.value &&
        start < sequence.start.value + sequence.length.value
      ) {
        memo.push(
          {
            kind: 'Message',
            type: 'ON',
            beat: start - sequence.start.value,
            key,
          },
          {
            kind: 'Message',
            type: 'OFF',
            beat: start + length - sequence.start.value,
            key,
          },
        );
      }
      return memo;
    },
    [],
  );

  messages.sort((a, b) => {
    const ab = a.beat % sequence.length.value;
    const bb = b.beat % sequence.length.value;

    if (ab < bb) {
      return -1;
    } else if (ab > bb) {
      return 1;
    } else if (a.type === 'OFF') {
      return -1;
    } else if (b.type === 'OFF') {
      return 1;
    }
    return 0;
  });

  return messages;
}
