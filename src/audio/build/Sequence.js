// @flow

// A sequence manages playback

import { buildMessages } from './build';
import { invariant } from '../../utils';
import type Project from './Project';
import type Module from './Module';
import type { Message } from './types';
import type { Sequence as SequenceAST } from '../../language';

export default class Sequence {
  project: Project
  ast: SequenceAST
  target: string
  messages: Array<Message>

  // handlers recieving note change information
  handlers: Set<(notes: Set<number>) => void>

  // notes that are currently 'on'
  activeNotes: Set<number>

  constructor(project: Project, ast: SequenceAST) {
    this.project = project;
    this.ast = ast;
    this.target = this.ast.target.value;
    this.handlers = new Set();
    this.activeNotes = new Set();
    this.messages = buildMessages(ast);
  }

  update(ast: SequenceAST) {
    this.ast = ast;
    this.target = this.ast.target.value;
    this.messages = buildMessages(ast);
  }

  onNoteChange(handler: (notes: Set<number>) => void) {
    this.handlers.add(handler);
  }

  offNoteChange(handler: (notes: Set<number>) => void) {
    this.handlers.delete(handler);
  }

  stop(time: number) {
    this._target().stop(time);
    this.activeNotes = new Set();
    this.handlers.forEach(handler => {
      handler(this.activeNotes);
    });
  }

  _target(): Module {
    const module = this.project.modules.get(this.target);
    invariant(module != null, `module '${this.target}' not found`);
    return module;
  }

  step = (
    globalTime: number,
    currentTime: number,
    playFromTime: number,
    playToTime: number,
    bps: number,
  ): void => {
    const {
      messages,
      ast,
    } = this;

    const loopLength = ast.length.value / bps;
    const loopStartTime = globalTime - playFromTime % loopLength;
    const playFromBeats = (playFromTime % loopLength) * bps;
    const playToBeats = (playToTime % loopLength) * bps;
    const target = this._target();

    let notesChanged = false;

    const send = (type, key, time) => {
      notesChanged = true;
      // console.log(type, key, {wrap: playToBeats <= playFromBeats, tempo, time, length, bps, currentTime, globalTime, playFromTime, playToTime, length, loopLength, loopStartTime, playFromBeats, playToBeats});
      if (type === 'ON') {
        this.activeNotes.add(key);
        target.noteOn(key, time);
      } else {
        this.activeNotes.delete(key);
        target.noteOff(key, time);
      }
    };

    messages.forEach(
      playToBeats > playFromBeats
        // there is no wrap
        ? ({ beat, type, key }) => {
          if (beat >= playFromBeats && beat < playToBeats) {
            send(type, key, loopStartTime + beat / bps);
          }
        }
        // there may be wrap
        : ({ beat, type, key }) => {
          if (beat >= playFromBeats) {
            send(type, key, loopStartTime + beat / bps);
          } else if (beat < playToBeats) {
            send(type, key, loopStartTime + loopLength + beat / bps);
          }
        }
    );

    if (notesChanged) {
      this.handlers.forEach(handler => {
        handler(this.activeNotes);
      })
    }
  }
}
