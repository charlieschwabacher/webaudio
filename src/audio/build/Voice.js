// @flow

// A Voice represents a w/ soure and effect nodes, and modulation.  It is
// responsible for changing the pitch of sources and triggering modulation in
// response to note on and off messages.

import { MIN_TIME } from '../constants';
import {
  buildNode,
  buildNodes,
  connectNodes,
} from './build';
import FileLibrary from './FileLibrary';
import { keyMap, invariant } from '../../utils';
import type { Module as ModuleAST } from '../../language';

// Necessary because flow doesn't handle array access to AudioNode properties
type AudioNode = Object;

export default class Voice {
  ast: ModuleAST
  context: AudioContext
  library: FileLibrary
  input: AudioNode // GainNode
  output: AudioNode // GainNode
  nodes: Map<string, AudioNode>
  on: boolean
  id: number

  constructor(context: AudioContext, library: FileLibrary, ast: ModuleAST) {
    const input = new GainNode(context, { gain: 0 });
    const output = new GainNode(context, { gain: 0 });
    const nodes = buildNodes(context, library, ast.nodes);
    connectNodes(nodes, ast.connections, input, output);

    this.ast = ast;
    this.context = context;
    this.library = library;
    this.input = input;
    this.output = output;
    this.nodes = nodes;
    this.on = false;
  }

  update(ast: ModuleAST) {
    const { context, library, nodes, input, output } = this;

    // map new and existing nodes and connections by id
    const existingNodesById = keyMap(this.ast.nodes.items, n => n.name);
    const newNodesById = keyMap(ast.nodes.items, n => n.name);
    const existingConnectionsBySignature = keyMap(
      this.ast.connections.items,
      c => `${c.from}->${c.to}`,
    );
    const newConnectionsBySignature = keyMap(
      ast.connections.items,
      c => `${c.from}->${c.to}`,
    );

    // handle removed nodes
    nodes.forEach((node, id) => {
      if (!newNodesById.has(id)) {
        node.disconnect();
        nodes.delete(id);
      }
    });

    // handle new or updated nodes
    newNodesById.forEach((node, id) => {
      const existing = existingNodesById.get(id);
      if (node !== existing) {
        // create new node
        if (existing == null) {
          nodes.set(id, buildNode(context, library, node));
        // update attrs of existing node
        } else {
          const audioNode = nodes.get(id);
          invariant(audioNode);

          node.options.forEach(option => {
            const { key, value } = option;

            // update audiparam
            if (audioNode[key] instanceof AudioParam) {
              if (audioNode[key].value !== value) {
                audioNode[key].setValueAtTime(value, context.currentTime);
              }
            // update audio buffer
            } else if (key === 'buffer') {
              invariant(value instanceof 'string');
              library.get(value).then(buffer => {
                if (audioNode.buffer !== buffer) {
                  audioNode.buffer = buffer
                }
              });
            // update scalar value
            } else {
              if (audioNode[key] !== value) {
                audioNode[key] = value;
              }
            }
          });
        }
      }
    });

    // handle removed connections
    existingConnectionsBySignature.forEach(({ from, to, param}, sig) => {
      if (!newConnectionsBySignature.has(sig)) {
        const fromNode = from === 'IN' ? input : nodes.get(from);
        const toNode = to === 'OUT' ? output : nodes.get(to);
        if (fromNode && toNode) {
          const destination = param != null ? toNode[param] : toNode;
          fromNode.disconnect(destination);
        }
      }
    });

    // add new connections
    newConnectionsBySignature.forEach(({ from, to, param }, sig) => {
      if (!existingConnectionsBySignature.has(sig)) {
        const fromNode = from === 'IN' ? input : nodes.get(from);
        const toNode = to === 'OUT' ? output : nodes.get(to);
        invariant(fromNode && toNode, `invalid connection '${sig}'`);

        const destination = param != null ? toNode[param] : toNode;
        fromNode.connect(destination);
      }
    });

    // save updates
    this.ast = ast;
  }

  disconnect() {
    this.input.disconnect();
    this.output.disconnect();
  }

  noteOn(key: number, time: number) {
    this.output.gain.linearRampToValueAtTime(1, time + MIN_TIME);

    // mark voice on
    this.on = true;

    // forward note on to nodes
    this.nodes.forEach(node => {
      if (node.noteOn) { node.noteOn(key, time); }
    });
  }

  noteOff(time: number) {
    // mark voice off
    this.on = false;

    // forward note off to nodes
    this.nodes.forEach(node => {
      if (node.noteOff) { node.noteOff(time); }
    })
  }
}
