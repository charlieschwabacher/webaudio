// @flow

export default function update<
  A: { ast: Object },
  B: { name: string }
>(
  children: Map<string, A>,
  astChildren: Array<B>,
  build: (ast: B) => A,
  update: (child: A, ast: B) => void,
  destroy: (child: A) => void,
): Map<string, A> {
  const result = new Map();

  astChildren.forEach(ast => {
    const { name } = ast;
    const existing = children.get(name);

    if (existing == null) {
      result.set(name, build(ast));
    } else {
      result.set(name, existing);
      update(existing, ast);
    }
  });

  children.forEach((child, name) => {
    if (!result.has(name)) {
      destroy(child);
    }
  });

  return result;
}
