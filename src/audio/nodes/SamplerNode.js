// @flow

export default class SamplerNode extends window.GainNode {
  buffer: ?AudioBuffer
  source: ?AudioBufferSourceNode
  detuneProxy: window.ConstantSourceNode
  detune: AudioParam

  constructor(
    context: AudioContext,
    options: { buffer?: ?AudioBuffer, detune?: ?number },
  ) {
    super(context, { gain: 1 });

    // use constant source node to proxy audio param value to dynamic nodes
    const detuneProxy = new window.ConstantSourceNode(context, { offset: options.detune || 0 });
    detuneProxy.start();

    this.buffer = options.buffer;
    this.detuneProxy = detuneProxy;
    this.detune = this.detuneProxy.offset;
  }

  noteOn(key: number, time: number) {
    const { buffer, source, context, detuneProxy } = this;

    if (source != null) {
      source.disconnect();
    }

    const nextSource = new AudioBufferSourceNode(context, { buffer });
    nextSource.connect(this);
    detuneProxy.connect(nextSource.detune);
    nextSource.start(time);

    this.source = nextSource;
  }

  noteOff(time: number) { }
}
