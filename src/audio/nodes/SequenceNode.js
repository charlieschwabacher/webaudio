// @flow

import { MIN_TIME } from '../constants';

export default class SequenceNode extends window.ConstantSourceNode {
  glide: number

  constructor(context: AudioContext, options: { glide?: number }) {
    super(context, { offset: 0 });
    this.glide = Math.max(MIN_TIME, options.glide || 0);
  }

  noteOn(key: number, time: number) {
    // console.log('SEQUENCE NODE NOTE ON', time, key);

    const detuneValue = 100 * (key - 69);
    this.offset.linearRampToValueAtTime(detuneValue, time + this.glide);
  }

  noteOff(time: number) { }
}
