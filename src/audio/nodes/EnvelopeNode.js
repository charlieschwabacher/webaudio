// @flow

import { MIN_TIME } from '../constants';
const DEFAULT_STEPS = [1.0, 0];

export default class SequenceNode extends window.ConstantSourceNode {
  steps: Array<number>

  constructor(context: AudioContext, options: { steps?: Array<number> }) {
    super(context, { offset: 0 });
    this.steps = options.steps || DEFAULT_STEPS;
  }

  noteOn(key: number, time: number) {
    const initialValue = this.steps[0];
    const steps = this.steps.slice(1, -1);

    this.offset.cancelAndHoldAtTime(time);

    let sumTime = time + MIN_TIME;
    this.offset.linearRampToValueAtTime(initialValue, sumTime);

    for (let i = 0, len = steps.length - 1; i < len; i += 2) {
      const stepTime = steps[i];
      const value = steps[i + 1];
      sumTime += stepTime;
      this.offset.linearRampToValueAtTime(value, sumTime);
    }
  }

  noteOff(time: number) {
    this.offset.cancelAndHoldAtTime(time);

    const release = this.steps.slice(-1)[0];
    const releaseTime = time + Math.max(release, MIN_TIME);
    this.offset.linearRampToValueAtTime(0, releaseTime);
  }
}
