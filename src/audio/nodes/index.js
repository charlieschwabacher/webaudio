// @flow
// These files subclassing audio nodes need to be ignored by webpack / babel
// because their compilation leads to invalid calls to their native AudioNode
// constructors.  I made this happen for now by editing react-scripts webpack
// config under node modules, but will need to eject from create-react-app for
// a better long term solution.

import EnvelopeNode from './EnvelopeNode';
import SamplerNode from './SamplerNode';
import SequenceNode from './SequenceNode';
import WhiteNoiseNode from './WhiteNoiseNode';

export default {
  // $FlowFixMe - flow doesn't know about ConstantSourceNode
  ConstantSourceNode, // eslint-disable-line

  AudioBufferSourceNode,
  BiquadFilterNode,
  ConvolverNode,
  DelayNode,
  DynamicsCompressorNode,
  EnvelopeNode,
  GainNode,
  OscillatorNode,
  SamplerNode,
  SequenceNode,
  WaveShaperNode,
  WhiteNoiseNode,
}
