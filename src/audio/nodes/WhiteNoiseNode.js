// @flow

export default class WhiteNoiseNode extends AudioBufferSourceNode {
  constructor(context: AudioContext) {
    const sampleRate = context.sampleRate;
    const bufferSize = 2 * sampleRate;
    const buffer = context.createBuffer(1, bufferSize, sampleRate);
    const output = buffer.getChannelData(0);
    for (var i = 0; i < bufferSize; i++) {
      output[i] = Math.random() * 2 - 1;
    }

    super(context, { buffer, loop: true });
  }
}
