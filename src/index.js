// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Project } from './audio';
import App from './App';
import store from './store';
import { invariant } from './utils';
import './index.css';


// set up Project

const { project: { ast, session } } = store.getState();
const project = new Project(ast, session, store.dispatch);
window.project = project;

store.subscribe(() => {
  const { project: { ast, session } } = store.getState();
  project.update(ast, session);
});

document.addEventListener('visibilitychange', () => {
  if (document.hidden) {
    project.suspend();
  } else {
    project.resume();
  }
});

// render UI
const root = document.getElementById('root');
invariant(root, 'Could not find root element to render react app');
ReactDOM.render(
  <Provider store={store}>
    <App project={project} />
  </Provider>,
  root,
);
