import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from './store';
import { Project } from './audio';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const { project: { ast, session } } = store.getState();
  const project = new Project(ast, session, store.dispatch);

  ReactDOM.render(
    <App
      project={project}
      store={store}
    />,
    div,
  );
});
