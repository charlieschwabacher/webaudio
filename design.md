Project takes an ast, creates an audio context and graph, handles control of
playback, and runs the main sequencing loop.  When the AST changes, project
diffs and feeds changes down through the graph.

```
Project
  modules: Map<string, Module>
  sequences: Map<string, Sequence>
  analyser: AnalyserNode

  constructor(ast: ProjectAST): void
  update(ast: ProjectAST): void

  start(): void
  stop(): void
```

Module recieves a module AST, creates its voices, and delegates note on and off
messages to the appropriate voice.
```
Module
  input: GainNode
  output: GainNode
  voices: Array<Voice>

  constructor(context: AudioContext, ast: ModuleAST)
  update(ast: ModuleAST): void

  noteOn(note: number, time: number): void
  noteOff(note: number, time: number): void

  stop(time: number): void
```

Voice receives a module AST, and handles playback of one note at a time
```
Voice
  input: GainNode
  output: GainNode
  nodes: Map<string, AudioNode>
  targets: Map<string, AudioNode>

  constructor(context: AudioContext, ast: ModuleAST)
  update(ast: ModuleAST): void

  noteOn(note: number, time: number): void
  noteOff(time: number): void
```

Sequence handles looped playback of a group of notes by sending note on and off
messages to target nodes of a track.
```
Sequence
  constructor(project: Project, ast: ProjectAST): void
  update(ast: ProjectAST): void

  start(): void
  stop(): void
```
