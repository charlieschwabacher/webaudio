// general requirements

global.fetch = () => Promise.resolve({
  arrayBuffer() { return Promise.resolve(new ArrayBuffer()); }
});

global.requestAnimationFrame = () => {};


// codemirror requirements

global.document.body.createTextRange = () => ({
  getBoundingClientRect: () => ({ }),
  getClientRects: () => ({ }),
});


// web audio classes

global.AudioContext = class AudioContext {
  createBuffer() { return new AudioBuffer(); }
  createScriptProcessor() { return new ScriptProcessorNode(); }
  decodeAudioData() { return new AudioBuffer(); }
};

global.AudioNode = class AudioNode {
  connect() {}
  disconnect() {}
};

class SourceNode extends AudioNode {
  start() {}
}

global.AudioParam = class AudioParam {
  linearRampToValueAtTime() {}
};

global.AudioBuffer = class AudioBuffer {
  getChannelData() {}
}


// source nodes

global.AudioBufferSourceNode = class AudioBufferSourceNode extends SourceNode {
  playbackRate = new AudioParam();
  detune = new AudioParam();
};

global.ConstantSourceNode = class ConstantSourceNode extends SourceNode {
  offset = new AudioParam();
};

global.OscillatorNode = class OscillatorNode extends SourceNode {
  frequency = new AudioParam();
  detune = new AudioParam();
};


// non source nodes

global.AnalyserNode = class AnalyserNode extends AudioNode {
  getByteFrequencyData() { return []; }
};

global.BiquadFilterNode = class BiquadFilterNode extends AudioNode {
  frequency = new AudioParam();
  Q = new AudioParam();
};

global.ConvolverNode = class ConvolverNode extends AudioNode {

};

global.DelayNode = class DelayNode extends AudioNode {
  delayTime = new AudioParam();
};

global.DynamicsCompressorNode = class DynamicsCompressorNode extends AudioNode {

}

global.GainNode = class GainNode extends AudioNode {
  gain = new AudioParam();
};

global.ScriptProcessorNode = class DynamicsCompressorNode extends AudioNode {

};

global.WaveShaperNode = class WaveShaperNode extends AudioNode {

};
